//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_AQ_JAGER_H
#define OPENDARTS_FLASH_EOS_AQ_JAGER_H
//--------------------------------------------------------------------------

#include "dartsflash/eos/ideal.hpp"
#include "dartsflash/eos/aq/aq.hpp"

namespace jager {
	extern double R, T_0, P_0, m_s0;
    extern std::unordered_map<std::string, double> gi0, hi0, gi_0, hi_0;
    
    extern std::unordered_map<std::string, double> omega, cp1, cp2;
    extern std::unordered_map<std::string, std::vector<double>> vp;
    extern std::vector<double> eps;

    extern std::unordered_map<std::string, std::unordered_map<std::string, std::vector<double>>> Bca, Cca, Dca, B;

    extern double e, eps0;

	class Integral
	{
	protected:
    	double pp, TT;
		std::string component;
    
	public:
		Integral() { }
		Integral(std::string component_) { component = component_; }

		virtual double f(double X) { (void) X; return 0.; }

	protected:
		double simpson(double x0, double x1, int steps);
	};

	class IG : public Integral
	{
	private:
		double gi0, hi0;
		std::vector<double> cpi;
	
	public:
		IG(std::string component_);

		// Jager/Ballard integrals
		double G() { return this->gi0 / (M_R * jager::T_0); }  // gi0/RT0: ideal gas Gibbs energy at p0 = 1 bar, T0 = 298.15 K
		double H(double T);  // Integral of H(T)/RT^2 dT from T_0 to T
		double dHdT(double T);  // Derivative of integral w.r.t. temperature
	};

	class H : public Integral
	{
	public:
		H(std::string component_) : Integral(component_) {}
		
		double f(double T) override;
		double F(double T);

		double dFdT(double T);
	};

	class V : public Integral
	{
	public:
		V(std::string component_) : Integral(component_) {}
		
		double f(double p) override;
		double f(double p, double T);
		double F(double p, double T);

		double dFdP(double p, double T);
		double dFdT(double p, double T);
	};

	class PX : public Integral
	{
	public:
		PX() : Integral() {}
		
		double f(double p) override;
		double f(double p, double T);
		double F(double p, double T);
	};
}

class Jager2003 : public AQBase
{
private:
	// parameters for H2O fugacity
	std::vector<double> gi, hi, vi, gi0; // gibbs energy, enthalpy, volume, gibbs energy of ideal gas
	double I, eps, depsdP, depsdT, A_DH, dA_DHdP, dA_DHdT; // ionic contribution coefficients
	std::vector<double> dIdxj, B0, B1, dB0dP, dB0dT, B_ca, C_ca, D_ca, dBcadT, dCcadT, dDcadT;
	std::vector<double> lna, dlnadxj; //, dlnadP, dlnadT;

public:
	Jager2003(CompData& comp_data);

	AQBase* getCopy() override { return new Jager2003(*this); }

	void init_PT(double p_, double T_, AQEoS::CompType comp_type) override;
	void solve_PT(std::vector<double>& x_, bool second_order, AQEoS::CompType comp_type) override;

	double lnphii(int i) override;
	double dlnphii_dP(int i) override;
	double dlnphii_dT(int i) override;
	double dlnphii_dxj(int i, int j) override;

	double G_PT_pure() override;

private:
	// Activity of water component
	void lnaw(bool second_order = false);
	double dlnaw_dP();
	double dlnaw_dT();

	// Activity of molecular species
	void lnam(bool second_order = false);
	double dlnam_dP(int i);
	double dlnam_dT(int i);

	// Activity of ionic species
	void lnai(bool second_order = false);
	double dlnai_dP(int i);
	double dlnai_dT(int i);

public:
	int dP_test(double p_, double T_, std::vector<double>& n_, double tol);
	int dT_test(double p_, double T_, std::vector<double>& n_, double tol);
	// int dX_test();
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_AQ_JAGER_H
//--------------------------------------------------------------------------
