//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_AQ_ZIABAKHSH_H
#define OPENDARTS_FLASH_EOS_AQ_ZIABAKHSH_H
//--------------------------------------------------------------------------

#include "dartsflash/eos/aq/aq.hpp"

namespace ziabakhsh {
	extern std::vector<double> Psw;
	extern std::unordered_map<std::string, std::vector<double>> labda, ksi;
	extern std::unordered_map<std::string, double> eta, tau, beta, Gamma;
	extern double R;
}

class Ziabakhsh2012 : public AQBase
{
private:
	double m_c, m_ac;
	double V_H2O{ 18.1 }, Mw{ 18.0152 };
	double K0_H2O, lnKw;
	double rho0_H2O, drho0_H2OdT, drho0_H2OdP, f0_H2O, df0_H2OdT, df0_H2OdP;
	std::vector<double> lnk_H, labda, ksi, dmcdxj, dmacdxj;

public:
	Ziabakhsh2012(CompData& comp_data);

	AQBase* getCopy() override { return new Ziabakhsh2012(*this); }
	
	void init_PT(double p_, double T_, AQEoS::CompType component) override;
	void solve_PT(std::vector<double>& x_, bool second_order, AQEoS::CompType comp_type) override;

	double lnphii(int i) override;
	double dlnphii_dP(int i) override;
	double dlnphii_dT(int i) override;
	double dlnphii_dxj(int i, int j) override;

	double G_PT_pure() override;

private:
	double lnji(int i);
	double dlnji_dxj(int i, int j);
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_AQ_ZIABAKHSH_H
//--------------------------------------------------------------------------
