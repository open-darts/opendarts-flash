//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_IDEAL_H
#define OPENDARTS_FLASH_EOS_IDEAL_H
//--------------------------------------------------------------------------

#include <unordered_map>
#include <vector>
#include <string>
#include "dartsflash/global/global.hpp"
#include "dartsflash/eos/eos.hpp"

class IdealGas : public EoS
{
private:
	double N;

public:
	IdealGas(CompData& comp_data);
	EoS* getCopy() override { return new IdealGas(*this); }

	// PV = nRT
	double P(double v, double T_, double N_) { return N_ * this->units.R * T_ / v; }
	double V(double p_, double T_, double N_) { return N_ * this->units.R * T_ / p_; }

	// Solve EoS
	void init_PT(double p_, double T_, bool calc_gpure=true) override;
	void solve_PT(std::vector<double>::iterator n_it, bool second_order=true) override;

	double lnphii(int i) override { (void) i; return 0.; }
	double dlnphii_dP(int i) override { (void) i; return 0.; }
	double dlnphii_dT(int i) override { (void) i; return 0.; }
	double dlnphii_dnj(int i, int j) override { (void) i; (void) j; return 0.; }

	std::vector<double> G_PT_pure() override { return std::vector<double>(nc, 0.); }
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_IDEAL_H
//--------------------------------------------------------------------------
