//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_VDWP_MUNCK_H
#define OPENDARTS_FLASH_EOS_VDWP_MUNCK_H
//--------------------------------------------------------------------------

#include "dartsflash/eos/vdwp/vdwp.hpp"

namespace munck {
	extern double R, T_0;
	
	extern std::unordered_map<std::string, std::unordered_map<std::string, std::vector<double>>> A_km, B_km;
	extern std::unordered_map<std::string, std::unordered_map<std::string, double>> dmu0, dH0, dV0, dCp;

	class Integral
	{
	protected:
    	double pp, TT;
		std::string phase, ref_phase;
    
	public:
		Integral(std::string phase_, std::string ref_phase_) { phase = phase_; ref_phase = ref_phase_; }
	};

	class HB : public Integral
	{
	public:
		HB(std::string phase_, std::string ref_phase_) : Integral(phase_, ref_phase_) { }

		double f(double T);
		double F(double T);

		double dFdT(double T);
	};

	class VB : public Integral
	{
	public:
		VB(std::string phase_, std::string ref_phase_) : Integral(phase_, ref_phase_) { }

		double f(double p, double T);
		double F(double p, double T);

		double dFdP(double p, double T);
		double dFdT(double p, double T);
	};
	
} // namespace munck

class Munck : public VdWP
{
private:
	std::string ref_phase;
	double dmu, dH, dV;

public:
	Munck(CompData& comp_data, std::string hydrate_type);
	EoS* getCopy() override { return new Munck(*this); }

	void init_PT(double p_, double T_, bool calc_gpure=true) override;
	void solve_PT(std::vector<double>::iterator n_it, bool second_order=true) override;

	double V(double p_, double T_, std::vector<double>& n) override;
	double fw(std::vector<double>& fi) override;

private:
	// Fugacity of water
	virtual double dfw_dP(std::vector<double>& dfidP) override;
	virtual double dfw_dT(std::vector<double>& dfidT) override;
	virtual std::vector<double> dfw_dxj(std::vector<double>& dfidxj) override;

	// Langmuir constant
	std::vector<double> calc_Ckm() override;
	std::vector<double> dCkm_dP() override;
	std::vector<double> dCkm_dT() override;
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_VDWP_MUNCK_H
//--------------------------------------------------------------------------
