//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_VDWP_VDWP_H
#define OPENDARTS_FLASH_EOS_VDWP_VDWP_H
//--------------------------------------------------------------------------

#include <unordered_map>
#include <vector>
#include <string>

#include "dartsflash/eos/eos.hpp"
#include "dartsflash/global/global.hpp"

namespace vdwp {
	extern double R;
	extern std::unordered_map<std::string, int> n_cages;
	extern std::unordered_map<std::string, std::vector<double>> Nm, vm;
    extern std::unordered_map<std::string, double> nH2O, xwH_full;
	extern std::unordered_map<std::string, std::vector<int>> zm;

} // namespace vdwp

class VdWP : public EoS
{
protected:
	std::string phase;
	int water_index;
	std::vector<double> theta_km, C_km, dCkmdP, dCkmdT;
	std::vector<double> f, x;
	std::vector<double>::iterator n_iterator;

	// Structure-specific parameters
	std::vector<int> zm; // #waters in cage
	std::vector<double> Nm, vm; // #guests per unit cell, #cages per H2O per unit cell
	int n_cages; // #cages in hydrate structure
	double nH2O; // #H2O molecules in hydrate structure

	// Michelsen (1990) parameters
	double eta, N0;
	std::vector<double> alpha, Nk;

public:
	VdWP(CompData& comp_data, std::string hydrate_type);

	double lnphii(int i) override;
	double dlnphii_dnj(int i, int k) override;
	double dlnphii_dxj(int i, int j) override;

	std::vector<double> dlnphi_dP() override;
	std::vector<double> dlnphi_dT() override;
	std::vector<double> dlnphi_dxj();
	// std::vector<double> dlnphi_dn() override;
	
	std::vector<double> xH();
	virtual double V(double p_, double T_, std::vector<double>& n) = 0;
	virtual double fw(std::vector<double>& fi) = 0;
	double fw(double p_, double T_, std::vector<double>& fi);

	virtual std::vector<double> G_PT_pure() override { return std::vector<double>(nc, NAN); }
	
protected:
	// Fugacity of water (specific for each model)
	virtual double dfw_dP(std::vector<double>& dfidP) = 0;
	virtual double dfw_dT(std::vector<double>& dfidT) = 0;
	virtual std::vector<double> dfw_dxj(std::vector<double>& dfidxj) = 0;

	// Change in chemical potential upon cage filling
	double calc_dmuH();
	double ddmuH_dP(std::vector<double>& dfidP);
	double ddmuH_dT(std::vector<double>& dfidT);
	std::vector<double> ddmuH_dxj(std::vector<double>& dfidxj);

	// Fugacity of guest molecules (Michelsen 1990)
	std::vector<double> fi();
	std::vector<double> dfi_dP();
	std::vector<double> dfi_dT();
	std::vector<double> dfi_dxj();

	// Langmuir constants C_km (specific for each model)
	virtual std::vector<double> calc_Ckm() = 0;
	virtual std::vector<double> dCkm_dP() = 0;
	virtual std::vector<double> dCkm_dT() = 0;

	// Cage occupancy theta_km (generic VdWP)
	std::vector<double> calc_theta();
	std::vector<double> dtheta_dP(std::vector<double>& dfidP);
	std::vector<double> dtheta_dT(std::vector<double>& dfidT);
	std::vector<double> dtheta_dxj(std::vector<double>& dfidxj);

	// Eta and N parameters for guest fugacities (generic VdWP)
	double calc_eta();
	double deta_dP();
	double deta_dT();
	std::vector<double> deta_dxj();

	double dN0dxk(int k);
	double dNjdxk(int j, int k);

public:
	// Test derivatives and integrals
	virtual int dP_test(double p_, double T_, std::vector<double>& x_, double tol);
	virtual int dT_test(double p_, double T_, std::vector<double>& x_, double tol);
	
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_VDWP_VDWP_H
//--------------------------------------------------------------------------
