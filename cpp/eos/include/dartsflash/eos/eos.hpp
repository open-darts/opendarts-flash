//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_EOS_H
#define OPENDARTS_FLASH_EOS_EOS_H
//--------------------------------------------------------------------------

#include <vector>
#include <string>
#include <map>
#include <Eigen/Dense>
#include "dartsflash/global/components.hpp"
#include "dartsflash/global/units.hpp"

class EoS
{
public:
	enum RootFlag : int { NONE = -1, STABLE = 0, MIN, MAX };
	enum RootSelect : int { REJECT = 0, ACCEPT, PREFER };

protected:
	int nc, ni = 0, ns;
	double p{ -1. }, T{ -1. };
	std::map<int, std::vector<double>> eos_range;
	std::vector<double> dlnphidn, dlnphidT, dlnphidP, gpure;
	RootFlag root{ STABLE }, root_type{ STABLE };
	RootSelect is_preferred_root = RootSelect::ACCEPT;
	bool is_stable_root = true;
	bool multiple_minima = true;
	CompData compdata;
	Units units;

public:
	EoS(CompData& comp_data);
	virtual ~EoS() = default;

	EoS(const EoS&) = default;
	virtual EoS* getCopy() = 0;

	// Getters and setters for EoS range, roots, comp_data, etc.
	void set_eos_range(int i, const std::vector<double>& range);
	bool eos_in_range(std::vector<double>::iterator n_it);
	
	CompData &get_comp_data() { return this->compdata; }
	void set_root_flag(EoS::RootFlag root_flag) { this->root = root_flag; }
	virtual RootFlag is_root_type() { return EoS::RootFlag::STABLE; }
	RootFlag is_root_type(std::vector<double>::iterator n_it) { this->solve_PT(n_it, false); return this->is_root_type(); }
	RootFlag is_root_type(double p_, double T_, std::vector<double>& n_) { this->solve_PT(p_, T_, n_, 0, false); return this->is_root_type(); }
	
	bool is_stable() { return this->is_stable_root; }
	virtual RootSelect select_root(std::vector<double>::iterator n_it) { (void) n_it; return RootSelect::ACCEPT; }
	bool has_multiple_minima() { return this->multiple_minima; }

	// Solve EoS
	virtual void init_PT(double p_, double T_, bool calc_gpure=true) = 0;
	virtual void solve_PT(std::vector<double>::iterator n_it, bool second_order=true) = 0;
	virtual void solve_PT(double p_, double T_, std::vector<double>& n_, int start_idx=0, bool second_order=true);

	virtual double lnphii(int i) = 0;
	virtual double dlnphii_dP(int i) { return dlnphidP[i]; }
	virtual double dlnphii_dT(int i) { return dlnphidT[i]; }
	virtual double dlnphii_dnj(int i, int j) { return dlnphidn[i*nc + j]; }
	virtual double dlnphii_dxj(int i, int j) { return dlnphidn[i*nc + j]; }

	std::vector<double> lnphi();
	virtual std::vector<double> dlnphi_dP();
	virtual std::vector<double> dlnphi_dT();
	virtual std::vector<double> dlnphi_dn();
	std::vector<double> fugacity(double p_, double T_, std::vector<double>& x_);
	
	// Method to find if GE/TPD surface at composition is convex
	Eigen::MatrixXd calc_hessian(std::vector<double>::iterator n_it);
	bool is_convex(std::vector<double>::iterator n_it);
	bool is_convex(double p_, double T_, std::vector<double>& n_, int start_idx=0);
	double calc_condition_number(double p_, double T_, std::vector<double>& n_, int start_idx=0);

	// Ideal Gibbs energy, enthalpy and heat capacities
	double cpi(double T_, int i);  // Cpi/R: heat capacity at constant pressure of component i
	double cvi(double T_, int i) { return this->cpi(T_, i) - 1.; } // Cvi/R: heat capacity at constant volume of component i
	double hi(double T_, int i);  // Hi/R: Ideal gas enthalpy
	double si_PT(double p_, double T_, int i);  // Si/R: ideal gas entropy at constant pressure
	double si_VT(double V_, double T_, int i);  // Si/R: ideal gas entropy at constant volume

	// "Total" properties (reference conditions arbitrary)
	double G_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	double H_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	double S_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	// double A_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	// double U_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);

	// Residual properties
	virtual double Gr_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	virtual double Hr_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	virtual double Sr_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	// virtual double Ar_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	// virtual double Ur_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);

	// Pure component properties
	std::vector<double>& get_gpure() { return this->gpure; }
	virtual std::vector<double> G_PT_pure() = 0;
	// virtual std::vector<double> H_PT_pure(double p_, double T_);
	// virtual std::vector<double> S_PT_pure(double p_, double T_);
	// virtual std::vector<double> A_PT_pure(double p_, double T_);
	// virtual std::vector<double> U_PT_pure(double p_, double T_);

	// Mixing properties and local minima
	double property_of_mixing(std::vector<double>& x_, std::vector<double>& mixture_prop, std::vector<double>& pure_prop);
	std::vector<double> mix_min(std::vector<double>& x_, std::vector<double>& gpure_, double& gmix);

protected:
	// Residual properties
	std::vector<double> Gri_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	std::vector<double> Hri_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	std::vector<double> Sri_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	// std::vector<double> Ari_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	// std::vector<double> Uri_PT(double p_, double T_, std::vector<double>& x_, int start_idx=0);

	// Numerical derivatives
	std::vector<double> dlnphi_dP_num(double p_, double T_, std::vector<double>& n_, double dp);
	std::vector<double> dlnphi_dT_num(double p_, double T_, std::vector<double>& n_, double dT);
	std::vector<double> dlnphi_dn_num(double p_, double T_, std::vector<double>& n_, double dn);

	// Translation of derivatives w.r.t. mole fractions to mole numbers
	double dxj_to_dnk(std::vector<double>& dlnphiidxj, std::vector<double>::iterator n_it, int k);
	std::vector<double> dxj_to_dnk(std::vector<double>& dlnphiidxj, std::vector<double>::iterator n_it);
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_EOS_H
//--------------------------------------------------------------------------
