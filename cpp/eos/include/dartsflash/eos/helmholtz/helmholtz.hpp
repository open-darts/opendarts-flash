//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_HELMHOLTZ_HELMHOLTZ_H
#define OPENDARTS_FLASH_EOS_HELMHOLTZ_HELMHOLTZ_H
//--------------------------------------------------------------------------

#include <complex>
#include "dartsflash/eos/eos.hpp"
#include "dartsflash/global/global.hpp"

class HelmholtzEoS : public EoS
{
protected:
	double z, v, N;
	std::unordered_map<int, std::pair<double, EoS::RootFlag>> preferred_roots = {};
	std::vector<std::complex<double>> Z_roots;
	std::vector<double> n;

public:
	HelmholtzEoS(CompData& comp_data);
	
protected:
	// Volume, pressure function, compressibility factor and derivatives
	virtual double V() = 0;
	virtual double dZ_dP() = 0;
	virtual double d2Z_dP2() = 0;
	double P();
	double dP_dV();
	double dP_dT();
	double dP_dni(int i);
	double dV_dni(int i);
	double dV_dT();
	double dT_dni(int i);
	double d2P_dV2();

	// Reduced Helmholtz function and derivatives
	virtual double F() = 0;
	virtual double dF_dV() = 0;
	virtual double dF_dT() = 0;
	virtual double dF_dni(int i) = 0;
    virtual double d2F_dnidnj(int i, int j) = 0;
    virtual double d2F_dTdni(int i) = 0;
    virtual double d2F_dVdni(int i) = 0;
    virtual double d2F_dTdV() = 0;
    virtual double d2F_dV2() = 0;
    virtual double d2F_dT2() = 0;
	virtual double d3F_dV3() = 0;

public:
	// Calculate compressibility factor Z
	virtual std::vector<std::complex<double>> Z() = 0;
	void set_preferred_roots(int i, double x, EoS::RootFlag root_flag) { this->preferred_roots[i] = std::pair<double, EoS::RootFlag>{x, root_flag}; }
	virtual EoS::RootSelect select_root(std::vector<double>::iterator n_it) override;
	virtual EoS::RootFlag is_root_type() override { return (this->root_type > EoS::RootFlag::NONE) ? this->root_type : this->identify_root(); }
	virtual EoS::RootFlag identify_root() = 0;

	// Overloaded function for calculation of P(V, T, n) and V(p, T, n)
	double P(double V_, double T_, std::vector<double>& n_);
	double V(double p_, double T_, std::vector<double>& n_);
	double Zp(double p_, double T_, std::vector<double>& n_);
	double Zv(double V_, double T_, std::vector<double>& n_);
	double rho(double p_, double T_, std::vector<double>& n_);

	// Critical point calculations
	struct CriticalPoint
	{
		double Pc, Tc, Vc, Zc;

		CriticalPoint(double pc, double tc, double vc, double zc) : Pc(pc), Tc(tc), Vc(vc), Zc(zc) {}
		void print_point() { print("Pc, Tc, Vc, Zc", std::vector<double>{Pc, Tc, Vc, Zc}); }
	};
	virtual CriticalPoint critical_point(std::vector<double>& n_) = 0;
	virtual bool is_critical(double p_, double T_, std::vector<double>& n_) = 0;

	// Evaluation of EoS at (P, T, n) or (T, V, n)
	void init_PT(double p_, double T_, bool calc_gpure=true) override = 0;
	void solve_PT(std::vector<double>::iterator n_it, bool second_order=true) override;
	void solve_PT(double p_, double T_, std::vector<double>& n_, int start_idx=0, bool second_order=true) override { return EoS::solve_PT(p_, T_, n_, start_idx, second_order); }
	virtual void init_VT(double V_, double T_) = 0;
	void solve_VT(std::vector<double>::iterator n_it, bool second_order=true);
	void solve_VT(double V_, bool second_order=true);
	void solve_VT(double V_, double T_, std::vector<double>& n_, int start_idx=0, bool second_order=true);

	// Zero'th, first and second order parameters for Helmholtz formulation
	virtual void zeroth_order(std::vector<double>::iterator n_it) = 0;
	virtual void zeroth_order(std::vector<double>::iterator n_it, double V_) = 0;
	virtual void zeroth_order(double V_) = 0;
	virtual void first_order(std::vector<double>::iterator n_it) = 0;
	virtual void second_order(std::vector<double>::iterator n_it) = 0;
	
	// Fugacity coefficient and derivatives
	double lnphii(int i) override;
	double dlnphii_dP(int i) override;
	double dlnphii_dT(int i) override;
	double dlnphii_dnj(int i, int j) override;

	// virtual std::vector<double> G_PT_pure() override;

	// Residual bulk properties
	double Ar_VT(double V_, double T_, std::vector<double>& n_, int start_idx=0);
	double Sr_VT(double V_, double T_, std::vector<double>& n_, int start_idx=0);
	double Ur_VT(double V_, double T_, std::vector<double>& n_, int start_idx=0);
	double Hr_VT(double V_, double T_, std::vector<double>& n_, int start_idx=0);
	double Gr_VT(double V_, double T_, std::vector<double>& n_, int start_idx=0);
	virtual double Gr_PT(double p_, double T_, std::vector<double>& n_, int start_idx=0) override;
	virtual double Hr_PT(double p_, double T_, std::vector<double>& n_, int start_idx=0) override;
	virtual double Sr_PT(double p_, double T_, std::vector<double>& n_, int start_idx=0) override;
	double Ar_PT(double p_, double T_, std::vector<double>& n_, int start_idx=0);  // override;
	double Ur_PT(double p_, double T_, std::vector<double>& n_, int start_idx=0);  // override;

	double Cvr(double p_, double T_, std::vector<double>& n_, int start_idx=0);
	double Cv(double p_, double T_, std::vector<double>& n_, int start_idx=0);
	double Cpr(double p_, double T_, std::vector<double>& n_, int start_idx=0);
	double Cp(double p_, double T_, std::vector<double>& n_, int start_idx=0);
	double vs(double p_, double T_, std::vector<double>& n_, int start_idx=0);
	double JT(double p_, double T_, std::vector<double>& n_, int start_idx=0);

	// Consistency tests
	int derivatives_test(double p_, double T_, std::vector<double>& n_, double tol);
	int lnphi_test(double p_, double T_, std::vector<double>& n_, double tol);
	int pressure_test(double p_, double T_, std::vector<double>& n_, double tol);
	int temperature_test(double p_, double T_, std::vector<double>& n_, double tol);
	int composition_test(double p_, double T_, std::vector<double>& n_, double tol);
	int pvt_test(double p_, double T_, std::vector<double>& n_, double tol);
	int properties_test(double p_, double T_, std::vector<double>& n_, double tol);
	
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_HELMHOLTZ_HELMHOLTZ_H
//--------------------------------------------------------------------------
