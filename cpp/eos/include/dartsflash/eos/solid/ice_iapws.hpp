//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_SOLID_ICEIAPWS_H
#define OPENDARTS_FLASH_EOS_SOLID_ICEIAPWS_H
//--------------------------------------------------------------------------

#include <unordered_map>
#include <string>
#include <vector>

#include "dartsflash/eos/eos.hpp"
#include "dartsflash/global/global.hpp"

class IceIAPWS : public EoS
{
protected:
	double lnfS;

public:
	IceIAPWS(CompData& comp_data, std::string phase);

	EoS* getCopy() override { return new IceIAPWS(*this); }

	// Overloaded function for calculation of P(T, V, n) and V(p, T, n)
	double P(double T_, double V_, std::vector<double>& n);
	double V(double p_, double T_, std::vector<double>& n);
	double dV_dP(double p_, double T_, std::vector<double>& n);
	double dV_dT(double p_, double T_, std::vector<double>& n);
	double dV_dni(double p_, double T_, std::vector<double>& n, int i);

	void init_PT(double p_, double T_, bool calc_gpure=true) override;
	void solve_PT(std::vector<double>::iterator n_it, bool second_order=true) override;

	double lnphii(int i) override;
	std::vector<double> dlnphi_dP() override;
	std::vector<double> dlnphi_dT() override;
	std::vector<double> dlnphi_dn() override;

	std::vector<double> G_PT_pure() override;

	int pvt_test(double p_, double T_, std::vector<double>& n, double tol);

};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_SOLID_ICEIAPWS_H
//--------------------------------------------------------------------------
