#include <iostream>
#include <vector>
#include <complex>
#include <cmath>
#include <numeric>

#include "dartsflash/global/global.hpp"
#include "dartsflash/global/components.hpp"
#include "dartsflash/eos/ideal.hpp"
#include "dartsflash/maths/maths.hpp"

IdealGas::IdealGas(CompData& comp_data) : EoS(comp_data) { }

void IdealGas::init_PT(double p_, double T_, bool calc_gpure)
{
    this->p = p_; this->T = T_;
    (void) calc_gpure;
    this->gpure = std::vector<double>(ns, 0.);
    return;
}

void IdealGas::solve_PT(std::vector<double>::iterator n_it, bool second_order)
{
    this->N = std::accumulate(n_it, n_it + ns, 0.);
    (void) second_order;
    return;
}
