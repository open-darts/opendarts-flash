#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

#include "dartsflash/global/global.hpp"
#include "dartsflash/global/components.hpp"
#include "dartsflash/maths/maths.hpp"
#include "dartsflash/eos/solid/solid.hpp"

namespace solid_par {
    double T_0{ 298.15 }; // reference temperature
    double P_0{ 1. }; // reference pressure [bar]

    // gibbs energy of ideal gas at p0, T0
    std::unordered_map<std::string, double> gi0 = {
        {"H2O", -228700}, {"CO2", -394600}, {"N2", 0}, {"H2S", -33100}, 
        {"C1", -50830}, {"C2", -32900}, {"C3", -23500}, {"iC4", -20900}, {"nC4", -17200}, {"iC5", -15229}, {"nC5", -8370}, {"nC6", -290}, {"nC7", 8120},
        {"NaCl", -384138.00}, {"CaCl2", -748100.00}, {"KCl", -409140.00} 
    };
    // molar enthalpy of ideal gas at p0, T0
    std::unordered_map<std::string, double> hi0 = {
        {"H2O", -242000}, {"CO2", -393800}, {"N2", 0}, {"H2S", -20200}, 
        {"C1", -74900}, {"C2", -84720}, {"C3", -103900}, {"iC4", -134600}, {"nC4", -126200}, {"iC5", -165976}, {"nC5", -146500}, {"nC6", -167300}, {"nC7", -187900} ,
        {"NaCl", -411153.00}, {"CaCl2", -795800.00}, {"KCl", -436747.00}
    };

    std::unordered_map<std::string, std::string> pure_comp = {{"Ice", "H2O"}, {"NaCl", "NaCl"}, {"CaCl2", "CaCl2"}, {"KCl", "KCl"}};

    std::unordered_map<std::string, double> gp0 = {{"Ice", -236539.24}, {"NaCl", -384138.00}, {"CaCl2", -748100.00}, {"KCl", -409140.00}};
    std::unordered_map<std::string, double> hp0 = {{"Ice", -292714.43}, {"NaCl", -411153.00}, {"CaCl2", -795800.00}, {"KCl", -436747.00}};
    std::unordered_map<std::string, double> v0 = {{"Ice", 19.7254}, {"NaCl", 26.9880}, {"CaCl2", 51.5270}, {"KCl", 37.5760}};
    std::unordered_map<std::string, double> kappa = {{"Ice", 1.3357E-5}, {"NaCl", 2.0000E-6}, {"CaCl2", 2.0000E-6}, {"KCl", 2.0000E-6}};

    std::unordered_map<std::string, std::vector<double>> cp = {
        {"Ice", {0.735409713, 1.4180551e-2, -1.72746e-5, 63.5104e-9}},
        {"NaCl", {5.526, 0.1963e-2, 0., 0.}},
        {"CaCl2", {8.646, 0.153e-2, 0., 0.}},
        {"KCl", {6.17, 0., 0., 0.}}
    };
    std::unordered_map<std::string, std::vector<double>> alpha = {
        {"Ice", {1.522300E-4, 1.660000E-8, 0.}},
        {"NaCl", {2.000000E-5, 0., 0.}},
        {"CaCl2", {2.000000E-5, 0., 0.}},
        {"KCl", {2.000000E-5, 0., 0.}}
    };

    IG::IG(std::string component_) : Integral(component_)
    {
        this->gi0 = solid_par::gi0[component_];
        this->hi0 = solid_par::hi0[component_];
        this->cpi = comp_data::cpi[component_];
    }
    double IG::H(double T)
    {
        // Integral of H(T)/RT^2 dT from T_0 to T
        return (-(this->hi0 / M_R
                - this->cpi[0] * solid_par::T_0 
                - 1. / 2 * this->cpi[1] * std::pow(solid_par::T_0, 2) 
                - 1. / 3 * this->cpi[2] * std::pow(solid_par::T_0, 3)
                - 1. / 4 * this->cpi[3] * std::pow(solid_par::T_0, 4)) * (1./T - 1./solid_par::T_0)
                + (this->cpi[0] * (std::log(T) - std::log(solid_par::T_0))
                + 1. / 2 * this->cpi[1] * (T - solid_par::T_0)
                + 1. / 6 * this->cpi[2] * (std::pow(T, 2) - std::pow(solid_par::T_0, 2))
                + 1. / 12 * this->cpi[3] * (std::pow(T, 3) - std::pow(solid_par::T_0, 3))));
    }
    double IG::dHdT(double T)
    {
        // Derivative of integral w.r.t. temperature
        return (this->hi0 / M_R + 
                this->cpi[0] * (T-solid_par::T_0) 
                + 1. / 2 * this->cpi[1] * (std::pow(T, 2)-std::pow(solid_par::T_0, 2)) 
                + 1. / 3 * this->cpi[2] * (std::pow(T, 3)-std::pow(solid_par::T_0, 3))
                + 1. / 4 * this->cpi[3] * (std::pow(T, 4)-std::pow(solid_par::T_0, 4))) / std::pow(T, 2);
    }

    double H::f(double T) 
    {
        // H/R: molar enthalpy of pure phase [eq. 3.38], J/mol
        return (hi0[phase] / M_R
                + cp[phase][0] * (T-solid_par::T_0) 
                + 1. / 2 * cp[phase][1] * (std::pow(T, 2)-std::pow(solid_par::T_0, 2)) 
                + 1. / 3 * cp[phase][2] * (std::pow(T, 3)-std::pow(solid_par::T_0, 3)) 
                + 1. / 4 * cp[phase][3] * (std::pow(T, 4)-std::pow(solid_par::T_0, 4)));
    }
    double H::F(double T) 
    {
        // Integral of H(T)/RT^2 dT from T_0 to T
        return -(hi0[phase] / M_R
                - cp[phase][0] * solid_par::T_0 
                - 1. / 2 * cp[phase][1] * std::pow(solid_par::T_0, 2) 
                - 1. / 3 * cp[phase][2] * std::pow(solid_par::T_0, 3)
                - 1. / 4 * cp[phase][3] * std::pow(solid_par::T_0, 4)) * (1./T - 1./solid_par::T_0)
                + cp[phase][0] * (std::log(T) - std::log(solid_par::T_0))
                + 1. / 2 * cp[phase][1] * (T - solid_par::T_0)
                + 1. / 6 * cp[phase][2] * (std::pow(T, 2) - std::pow(solid_par::T_0, 2))
                + 1. / 12 * cp[phase][3] * (std::pow(T, 3) - std::pow(solid_par::T_0, 3));
    }
    double H::dfdT(double T)
    {
        // d(H/R)/dT
        return cp[phase][0]
                + cp[phase][1] * T
                + cp[phase][2] * std::pow(T, 2) 
                + cp[phase][3] * std::pow(T, 3);
    }
    double H::dFdT(double T) 
    {
        // Derivative of integral w.r.t. T
        return this->f(T) / std::pow(T, 2);
    }
    int H::test_derivatives(double T, double tol)
    {
        int error_output = 0;
        double df = this->dfdT(T);
        double dF = this->dFdT(T);

        double dT = 1e-5;
        double f_ = this->f(T-dT);
        double f1 = this->f(T+dT);
        double df_num = (f1-f_)/(2*dT);
        double F_ = this->F(T-dT);
        double F1 = this->F(T+dT);
        double dF_num = (F1-F_)/(2*dT);

        if (std::fabs(df_num - df) > tol) { print("Solid H df/dT", std::vector<double>{df_num, df}); error_output++; }
        if (std::fabs(dF_num - dF) > tol) { print("Solid H dF/dT", std::vector<double>{dF_num, dF}); error_output++; }
        
        return error_output;
    }

    double V::f(double p, double T) 
    {
        // molar volume of pure phase [eq. 3.39], m3/mol
        double a = alpha[phase][0] * (T-T_0) 
                + alpha[phase][1] * std::pow((T-T_0), 2) 
                + alpha[phase][2] * std::pow((T-T_0), 3);
        double b = - kappa[phase] * (p-P_0);
        return v0[phase]*1e-6 * std::exp(a+b);
    }
    double V::F(double p, double T) 
    {
        // Integral of V(T,P)/RT dP from P_0 to P
        // int e^cx dx = 1/c e^cx
        // int v0 exp(a + b*p)/RT dp = v0 exp(a)/RT * int exp(b*p) dp = v0 exp(a)/bRT * exp(b*p)
        double a = alpha[phase][0] * (T-T_0) 
                + alpha[phase][1] * std::pow((T-T_0), 2) 
                + alpha[phase][2] * std::pow((T-T_0), 3);
        double b = -kappa[phase];
        return v0[phase]*1e-6/M_R * std::exp(a-b*P_0)/T * (std::exp(b*p) / b - std::exp(b*P_0)/b);
    }
    double V::dfdP(double p, double T)
    {
        // dV/dP: derivative of molar volume of pure phase w.r.t. pressure [eq. 3.39], m3/mol
        double a = alpha[phase][0] * (T-T_0) 
                + alpha[phase][1] * std::pow((T-T_0), 2) 
                + alpha[phase][2] * std::pow((T-T_0), 3);
        double b = -kappa[phase] * (p-P_0);
        double dbdp = -kappa[phase];
        return v0[phase]*1e-6 * std::exp(a+b) * dbdp;
    }
    double V::dfdT(double p, double T)
    {
        // dV/dT: derivative of molar volume of pure phase w.r.t. temperature [eq. 3.39], m3/mol
        double a = alpha[phase][0] * (T-T_0) 
                + alpha[phase][1] * std::pow((T-T_0), 2) 
                + alpha[phase][2] * std::pow((T-T_0), 3);
        double dadT = alpha[phase][0] + 
                + 2. * alpha[phase][1] * (T-T_0)
                + 3. * alpha[phase][2] * std::pow((T-T_0), 2);
        double b = -kappa[phase] * (p-P_0);
        return v0[phase]*1e-6 * std::exp(a+b) * dadT;
    }
    double V::dFdP(double p, double T) 
    {
        // Derivative of integral w.r.t. pressure
        return this->f(p, T) / (M_R*T);
    }
    double V::dFdT(double p, double T) 
    {
        // Derivative of integral w.r.t. temperature
        double a = alpha[phase][0] * (T-T_0) 
                + alpha[phase][1] * std::pow(T-T_0, 2) 
                + alpha[phase][2] * std::pow(T-T_0, 3);
        double b = -kappa[phase];

        double da_dT = alpha[phase][0] 
                    + 2*alpha[phase][1] * (T-T_0) 
                    + 3*alpha[phase][2] * std::pow(T-T_0, 2);
        
        double d_dT = (T * da_dT * std::exp(a-b*P_0) - std::exp(a-b*P_0)) / std::pow(T, 2);
        
        return v0[phase]*1e-6/M_R * d_dT * (std::exp(b*p) / b - std::exp(b*P_0) / b);
    }
    int V::test_derivatives(double p, double T, double tol)
    {
        int error_output = 0;
        double f_, f1, df;
        double F_, F1, dF;
        double df_num, dF_num;

        // Test derivatives w.r.t. pressure
        double dp = 1e-5;
        df = this->dfdP(p, T);
        dF = this->dFdP(p, T);
        f_ = this->f(p-dp, T);
        f1 = this->f(p+dp, T);
        df_num = (f1-f_)/(2*dp);
        F_ = this->F(p-dp, T);
        F1 = this->F(p+dp, T);
        dF_num = (F1-F_)/(2*dp);
        if (std::fabs(df_num - df) > tol) { print("Solid V df/dP", std::vector<double>{df_num, df}); error_output++; }
        if (std::fabs(dF_num - dF) > tol) { print("Solid V dF/dP", std::vector<double>{dF_num, dF}); error_output++; }

        // Test derivatives w.r.t. pressure
        double dT = 1e-5;
        df = this->dfdT(p, T);
        dF = this->dFdT(p, T);
        f_ = this->f(p, T-dT);
        f1 = this->f(p, T+dT);
        df_num = (f1-f_)/(2*dT);
        F_ = this->F(p, T-dT);
        F1 = this->F(p, T+dT);
        dF_num = (F1-F_)/(2*dT);
        if (std::fabs(df_num - df) > tol) { print("Solid V df/dT", std::vector<double>{df_num, df}); error_output++; }
        if (std::fabs(dF_num - dF) > tol) { print("Solid V dF/dT", std::vector<double>{dF_num, dF}); error_output++; }

        return error_output;
    }
}

PureSolid::PureSolid(CompData& comp_data, std::string phase_) : EoS(comp_data)
{
    this->phase = phase_;
    std::string pure_comp = solid_par::pure_comp[phase];
    this->pure_comp_idx = std::distance(compdata.components.begin(), std::find(compdata.components.begin(), compdata.components.end(), pure_comp));
}

void PureSolid::init_PT(double p_, double T_, bool calc_gpure) {
    if (p_ != p || T_ != T)
    {
        this->p = p_; this->T = T_;

        // ideal gas Gibbs energy
        solid_par::IG ig = solid_par::IG(solid_par::pure_comp[phase]);
        double gio = ig.G();  // gi0/RT0
        double hio = ig.H(T);  // eq. 3.3
        double gi0 = gio - hio;  // eq. 3.2

        // Gibbs energy of ions in aqueous phase
        solid_par::H h = solid_par::H(phase);
        solid_par::V v = solid_par::V(phase);
        double gs0 = solid_par::gp0[phase] / (M_R * solid_par::T_0);
        double hs = h.F(T);  // integral of H(T)/RT^2 from T0 to T
        double vs = v.F(p, T);  // integral of V(T,P)/RT from P0 to P
        double gs = gs0 - hs + vs;
        
        // Calculate fugacity coefficient
        lnfS = gs - gi0;

        // Calculate gpure
        (void) calc_gpure;
        this->gpure = this->G_PT_pure();
    }
}

void PureSolid::solve_PT(std::vector<double>::iterator n_it, bool second_order) {
    (void) n_it;
    (void) second_order;
    return;
}

double PureSolid::P(double T_, double V_, std::vector<double>& n)
{
    // Find pressure at given (T, V, n)
    this->p = 1.;
    double v = V_;

    // Newton loop to find root
    while (true)
    {
        double res = this->V(p, T_, n) - v;
        double dres_dp = this->dV_dP(p, T_, n);
        p -= res/dres_dp;

        if (std::fabs(res) < 1e-14)
        {
            break;
        }
    }
    return p;
}

double PureSolid::V(double p_, double T_, std::vector<double>& n)
{
    // Calculate volume at (P, T, n)
    (void) n;
    solid_par::V v = solid_par::V(phase);
    return v.f(p_, T_);
}

double PureSolid::dV_dP(double p_, double T_, std::vector<double>& n)
{
    // Calculate pressure derivative of volume at (P, T, n)
    (void) n;
    solid_par::V v = solid_par::V(phase);
    return v.dfdP(p_, T_);
}

double PureSolid::dV_dT(double p_, double T_, std::vector<double>& n)
{
    // Calculate temperature derivative of volume at (P, T, n)
    (void) n;
    solid_par::V v = solid_par::V(phase);
    return v.dfdT(p_, T_);
}

double PureSolid::dV_dni(double p_, double T_, std::vector<double>& n, int i)
{
    // Calculate temperature derivative of volume at (P, T, n)
    (void) p_;
    (void) T_;
    (void) n;
    (void) i;
    return 0.;
}

double PureSolid::lnphii(int i) 
{
    return (i == pure_comp_idx) ? lnfS - std::log(p) : NAN;
}

std::vector<double> PureSolid::G_PT_pure()
{
    std::vector<double> Gpure(nc, NAN);
    Gpure[this->pure_comp_idx] = lnfS - std::log(p);
    return Gpure;
}

std::vector<double> PureSolid::dlnphi_dP()
{
    solid_par::V v = solid_par::V(phase);
    double dVs_dP = v.dFdP(p, T);
    
    dlnphidP = std::vector<double>(nc, 0.);
    dlnphidP[pure_comp_idx] = dVs_dP - 1./p;
    return dlnphidP;
}

std::vector<double> PureSolid::dlnphi_dT() {
    solid_par::H h = solid_par::H(phase);
    solid_par::V v = solid_par::V(phase);
    double dHs_dT = h.dFdT(T);
    double dVs_dT = v.dFdT(p, T);

    solid_par::IG ig = solid_par::IG(solid_par::pure_comp[phase]);
    double dHi_dT = ig.dHdT(T);

    dlnphidT = std::vector<double>(nc, 0.);
    dlnphidT[pure_comp_idx] = -dHs_dT + dVs_dT + dHi_dT;
    return dlnphidT;
}

std::vector<double> PureSolid::dlnphi_dn() {
    return std::vector<double>(nc, 0.);
}

int PureSolid::pvt_test(double p_, double T_, std::vector<double>& n, double tol)
{
    // Consistency of PVT: Calculate volume at (P, T, n) and find P at (T, V, n)
    int error_output = 0;

    // Calculate volume at P, T, n
    double v = this->V(p_, T_, n);

    // Evaluate P(T,V,n)
    double pp = this->P(T_, v, n);
    if (std::fabs(pp - p_) > tol)
    {
        print("P(T, V, n) != p", std::vector<double>{pp, p_, std::fabs(pp-p_)});
        error_output++;
    }
    
    return error_output;
}