#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <numeric>
#include <unordered_map>

#include "dartsflash/eos/vdwp/munck.hpp"

namespace munck {
    double T_0 = 273.15;
    double R = 8.3145;

    // Langmuir constants (Munck [1988])
    std::unordered_map<std::string, std::unordered_map<std::string, std::vector<double>>> A_km = {
        {"sI", {{"C1", {0.7228e-3, 23.35e-3}}, {"C2", {0., 3.039e-3}}, {"C3", {0., 0.}}, {"iC4", {0., 0.}}, {"nC4", {0., 0.}}, {"N2", {1.617e-3, 6.078e-3}}, {"CO2", {0.2474e-3, 42.46e-3}}, {"H2S", {0.025e-3, 16.34e-3}}}},
        {"sII", {{"C1", {0.2207e-3, 100.e-3}}, {"C2", {0., 240.e-3}}, {"C3", {0., 5.455e-3}}, {"iC4", {0., 189.3e-3}}, {"nC4", {0., 30.51e-3}}, {"N2", {0.1742e-3, 18.e-3}}, {"CO2", {0.0845e-3, 851.e-3}}, {"H2S", {0.0298e-3, 87.2e-3}}}},
    };
    std::unordered_map<std::string, std::unordered_map<std::string, std::vector<double>>> B_km = {
        {"sI", {{"C1", {3187., 2653.}}, {"C2", {0., 3861.}}, {"C3", {0., 0.}}, {"iC4", {0., 0.}}, {"nC4", {0., 0.}}, {"N2", {2905., 2431.}}, {"CO2", {3410., 2813.}}, {"H2S", {4568., 3737.}}}},
        {"sII", {{"C1", {3453., 1916.}}, {"C2", {0., 2967.}}, {"C3", {0., 4638.}}, {"iC4", {0., 3800.}}, {"nC4", {0., 3699.}}, {"N2", {3082., 1728.}}, {"CO2", {3615., 2025.}}, {"H2S", {4878., 2633.}}}},
    };

    // Physical constants in liquid/ice reference states
    std::unordered_map<std::string, std::unordered_map<std::string, double>> dmu0 = { // Δmu_0 [J/mol]
        {"sI", {{"W", 1264.}, {"I", 0.}}}, {"sII", {{"W", 883.}, {"I", 0.}}},
    };
    std::unordered_map<std::string, std::unordered_map<std::string, double>> dH0 = { // ΔH_0 [J/mol]
        {"sI", {{"W", -4858.}, {"I", 1151.}}}, {"sII", {{"W", -5201.}, {"I", 808.}}},
    };
    std::unordered_map<std::string, std::unordered_map<std::string, double>> dV0 = { // ΔV_0 [cm3/mol]
        {"sI", {{"W", 4.6}, {"I", 3.0}}}, {"sII", {{"W", 5.0}, {"I", 3.4}}},
    };
    std::unordered_map<std::string, std::unordered_map<std::string, double>> dCp = { // ΔCp_0 [J/mol.K]
        {"sI", {{"W", 39.16}, {"I", 0.}}}, {"sII", {{"W", 39.16}, {"I", 0.}}},
    };

    double HB::f(double T) {
        double h_beta = dH0[phase][ref_phase] + dCp[phase][ref_phase] * (T - T_0);
        return h_beta / (R * std::pow(T, 2)); // molar enthalpy of empty hydrate lattice
    }

    double HB::F(double T) {
        double R_inv = 1./R;
        double H_beta = - dH0[phase][ref_phase] * R_inv * (1./T-1./T_0) 
                        + dCp[phase][ref_phase] * R_inv * (std::log(T)-std::log(T_0) + T_0/T - 1.);
        return H_beta;
    }

    double HB::dFdT(double T) {
        return this->f(T);
    }

    double VB::f(double p, double T) {
        (void) p;
        return dV0[phase][ref_phase] / (R * 0.5*(T + 273.15)); // molar enthalpy of empty hydrate lattice
    }

    double VB::F(double p, double T) {
		return dV0[phase][ref_phase] * p / (R * 0.5*(T + 273.15)); // molar enthalpy of empty hydrate lattice, p_0 = 0
    }

    double VB::dFdP(double p, double T) {
        return this->f(p, T);
    }

    double VB::dFdT(double p, double T) {
        return -dV0[phase][ref_phase] * p / (R * 0.5*std::pow(T + 273.15, 2)); // molar enthalpy of empty hydrate lattice
    }
    
} // namespace munck

Munck::Munck(CompData& comp_data, std::string hydrate_type) : VdWP(comp_data, hydrate_type) { }

void Munck::init_PT(double p_, double T_, bool calc_gpure)
{
    // Fugacity of water in hydrate phase following modified VdW-P (Ballard, 2002)
    // Initializes all composition-independent parameters:
    // - Ideal gas Gibbs energy of water [eq. 3.1-3.4]
    // - Gibbs energy of water in empty hydrate lattice [eq. 3.47]
    // Each time calculating fugacity of water in hydrate, evaluate:
    // - Contribution of cage occupancy to total energy of hydrate [eq. 3.44]
    // - Activity of water in the hydrate phase [eq. 4.38]
    // - Chemical potential of water in hydrate [eq. 4.35]
    if (p_ != p || T_ != T)
    {
        this->p = p_;
        this->T = T_;

        // Calculate Langmuir constant of each guest k in each cage m
        C_km = this->calc_Ckm();

        // Calculate difference in chemical potential between empty hydrate lattice and reference phase (W or I)
        ref_phase = (T > 273.15) ? "W" : "I";  // water or ice

        // Chemical potential at reference conditions
        dmu = munck::dmu0[phase][ref_phase]/(munck::R * munck::T_0);

        // Enthalpy contribution
        munck::HB h = munck::HB(phase, ref_phase);
        dH = h.F(T);
        
        // Volume contribution
        munck::VB v = munck::VB(phase, ref_phase);
        dV = v.F(p, T);

        (void) calc_gpure;
    }
}

void Munck::solve_PT(std::vector<double>::iterator n_it, bool second_order)
{
    // Calculate mixture parameters with composition n
    (void) second_order;
    this->n_iterator = n_it;
    double nT_inv = 1./std::accumulate(n_it, n_it + this->nc, 0.);
    std::transform(n_it, n_it + this->nc, this->x.begin(),
                   [&nT_inv](double element) { return element *= nT_inv; });

    // Calculate fugacities
    f = this->fi();
    f[water_index] = this->fw(f);

    return;
}

double Munck::V(double p_, double T_, std::vector<double>& n)
{
    // Molar volume of hydrate
    (void) p_;
    (void) n;
    ref_phase = (T_ > 273.15) ? "W" : "I";  // water or ice
    return munck::dV0[phase][ref_phase];
}

double Munck::fw(std::vector<double>& fi){
    // Fugacity of water in hydrate following Munck (1988)

    // Contribution of cage occupancy to total energy of hydrate
    this->f = fi;
    double dmu_H = this->calc_dmuH();

    return std::exp(dmu - dH + dV + dmu_H);
}

double Munck::dfw_dP(std::vector<double>& dfidP) 
{
    // dfw/dP = exp(dmu/RT) * d/dP (dmu/RT)
    munck::VB v = munck::VB(phase, ref_phase);
    double ddV = v.dFdP(p, T);
    double ddmuH = this->ddmuH_dP(dfidP);
    return f[water_index] * (ddV + ddmuH);
}

double Munck::dfw_dT(std::vector<double>& dfidT)
{
    // dfw/dT = exp(dmu/RT) * d/dP (dmu/RT)
    munck::HB h = munck::HB(phase, ref_phase);
    munck::VB v = munck::VB(phase, ref_phase);

    double ddH = h.dFdT(T);
    double ddV = v.dFdT(p, T);
    double ddmuH = this->ddmuH_dT(dfidT);
    return f[water_index] * (-ddH + ddV + ddmuH);
}

std::vector<double> Munck::dfw_dxj(std::vector<double>& dfidxj)
{
    // dfw/dxk = exp(dmu/RT) * d/dxk (dmu/RT)
    std::vector<double> dfwdxk(nc);
    std::vector<double> ddmuHdxk = this->ddmuH_dxj(dfidxj);
    for (int k = 0; k < nc; k++)
    {
        dfwdxk[k] = f[water_index] * ddmuHdxk[k];
    }
    return dfwdxk;
}

std::vector<double> Munck::calc_Ckm() {
    // Calculate Langmuir constant of each guest k in each cage m
    std::vector<double> Ckm(n_cages * nc);
    
    double invT = 1./T;
    double atm_to_bar = 1./1.01325;
    for (int k = 0; k < nc; k++)
    {
        if (k != water_index)
        {
            std::vector<double> Aki = munck::A_km[phase][this->compdata.components[k]];
            std::vector<double> Bki = munck::B_km[phase][this->compdata.components[k]];
            for (int m = 0; m < n_cages; m++)
            {
                Ckm[nc*m + k] = Aki[m] * atm_to_bar * invT * std::exp(Bki[m] * invT);
            }
        }
    }
    return Ckm;
}

std::vector<double> Munck::dCkm_dP()
{
    return std::vector<double>(n_cages*nc, 0.);
}

std::vector<double> Munck::dCkm_dT()
{
    // Calculate derivative of Langmuir constants w.r.t. T
    std::vector<double> dCkmdT_(n_cages * nc);
    
    double invT = 1./T;
    double atm_to_bar = 1./1.01325;
    for (int k = 0; k < nc; k++)
    {
        if (k != water_index)
        {
            std::vector<double> Aki = munck::A_km[phase][this->compdata.components[k]];
            std::vector<double> Bki = munck::B_km[phase][this->compdata.components[k]];
            for (int m = 0; m < n_cages; m++)
            {
                dCkmdT_[nc*m + k] = Aki[m] * invT * atm_to_bar * std::exp(Bki[m] * invT) * (-invT - Bki[m]*std::pow(invT, 2));
            }
        }
    }
    return dCkmdT_;
}
