#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <numeric>
#include <unordered_map>

#include "dartsflash/eos/vdwp/vdwp.hpp"

namespace vdwp {
    double R = 8.314472;
    
    std::unordered_map<std::string, int> n_cages = {{"sI", 2}, {"sII", 2}, {"sH", 3}};
    std::unordered_map<std::string, std::vector<double>> Nm = {{"sI", {2, 6}}, {"sII", {16, 8}}, {"sH", {3, 2, 1}}}; // number per unit cell
    std::unordered_map<std::string, double> nH2O = {{"sI", 46.}, {"sII", 136.}, {"sH", 34.}}; // total number of H2O molecules in hydrate structure (sI, sII, sH)
    std::unordered_map<std::string, std::vector<int>> zm = {{"sI", {20, 24}}, {"sII", {20, 28}}, {"sH", {20, 20, 36}}}; // #waters in cage sI
    
    std::unordered_map<std::string, std::vector<double>> vm = { // number of cages per H2O molecule per unit cell
        {"sI", {Nm["sI"][0]/nH2O["sI"], Nm["sI"][1]/nH2O["sI"]}}, 
        {"sII", {Nm["sII"][0]/nH2O["sII"], Nm["sII"][1]/nH2O["sII"]}},
        {"sH", {Nm["sH"][0]/nH2O["sH"], Nm["sH"][1]/nH2O["sH"], Nm["sH"][2]/nH2O["sH"]}},
    };
    std::unordered_map<std::string, double> xwH_full = {
        {"sI", 1. - (Nm["sI"][0] + Nm["sI"][1]) / (nH2O["sI"] + Nm["sI"][0] + Nm["sI"][1])},
        {"sII", 1. - (Nm["sII"][0] + Nm["sII"][1]) / (nH2O["sII"] + Nm["sII"][0] + Nm["sII"][1])},
        {"sH", 1. - (Nm["sH"][0] + Nm["sH"][1] + Nm["sH"][2]) / (nH2O["sH"] + Nm["sH"][0] + Nm["sH"][1] + Nm["sH"][2])},
    };

} // namespace vdwp

VdWP::VdWP(CompData& comp_data, std::string hydrate_type) : EoS(comp_data) {
	this->phase = hydrate_type;
	water_index = comp_data.water_index;
    this->multiple_minima = false;

    zm = vdwp::zm[phase];
    Nm = vdwp::Nm[phase];
    vm = vdwp::vm[phase];
    n_cages = vdwp::n_cages[phase];
    nH2O = vdwp::nH2O[phase];

    // Define range of applicability (lower bound corresponds to xwH with full cage occupancy, upper bound is hypothetical empty hydrate lattice)
    this->eos_range[water_index] = {vdwp::xwH_full[phase], 1.};

    x.resize(nc);
    Nk.resize(nc);
    alpha.resize(nc);
}

double VdWP::fw(double p_, double T_, std::vector<double>& fi)
{
    // Calculate fugacity of water
    this->init_PT(p_, T_);
    return this->fw(fi);
}

std::vector<double> VdWP::fi() {
    // Calculate guest fugacity from p, T, n
    // Cole (1990) and Michelsen (1990)
    f = std::vector<double>(nc);

    // Introduce alpha[k] = Ck1/Ck2 and N[k]
    alpha = std::vector<double>(nc);
    Nk = std::vector<double>(nc);  // number of guest molecules per water molecule in unit cell
    for (int k = 0; k < nc; k++)
    {
        if (k == water_index)
        {
            Nk[k] = 0.;
            alpha[k] = 0.;
        }
        else
        {
            Nk[k] = x[k] / x[water_index];
            alpha[k] = C_km[k] / C_km[k + nc];
        }
    }
    double sumNk = std::accumulate(Nk.begin(), Nk.end(), 0.);
    N0 = vm[0] + vm[1] - sumNk;

    // Calculate eta
    eta = this->calc_eta();

    // Calculate guest fugacities
    for (int k = 0; k < nc; k++)
    {
        if (k != water_index)
        {
            f[k] = Nk[k] * 1. / (C_km[nc+k] * N0) * 1. / (eta + alpha[k] * (1-eta));
        }
    }

    return f;
}

std::vector<double> VdWP::dfi_dP() {
    // Derivative of guest fugacities w.r.t. P

    // Calculate derivatives of Langmuir constants C_km w.r.t. P
    dCkmdP = this->dCkm_dP();

    // Calculate derivatives of eta w.r.t. P
    double detadP = this->deta_dP();

    // Calculate derivative of guest fugacities
    std::vector<double> dfidP(nc);
    for (int i = 0; i < nc; i++)
    {
        if (i != water_index)
        {
            double denom = 1./(eta+alpha[i]*(1.-eta));

            // da[i]/dP = d/dP (Ci1/Ci2) = dCi1/dP / Ci2 - Ci1 * dCi2/dP / Ci2^2
            double dalphai_dP = dCkmdP[i] / C_km[nc+i] - C_km[i] * dCkmdP[nc+i] / std::pow(C_km[nc+i], 2);

            // df[i]/dP = Ni/N0 * (-1/Ci2^2 * dCi2/dP * 1/(n + a[i](1-n)) 
            //                     -1/Ci2 * 1/(n+a[i](1-n))^2 * (dn/dP*(1-a[i])+da[i]/dP*(1-n))
            dfidP[i] =  Nk[i]/N0 * (- 1./std::pow(C_km[nc+i], 2) * dCkmdP[nc+i] * denom 
                        - 1./C_km[nc+i] * std::pow(denom, 2) * (detadP * (1.-alpha[i]) + dalphai_dP * (1.-eta)));
        }
    }
    return dfidP;
}

std::vector<double> VdWP::dfi_dT() {
    // Derivative of guest fugacities w.r.t. T

    // Calculate derivatives of Langmuir constants C_km w.r.t. T
    dCkmdT = this->dCkm_dT();

    // Calculate derivatives of eta w.r.t. T
    double detadT = this->deta_dT();

    // Calculate derivative of guest fugacities
    std::vector<double> dfidT(nc);
    for (int i = 0; i < nc; i++)
    {
        if (i != water_index)
        {
            double denom = 1./(eta+alpha[i]*(1.-eta));

            // da[i]/dT = d/dT (Ci1/Ci2) = dCi1/dT / Ci2 - Ci1 * dCi2/dT / Ci2^2
            double dalphai_dT = dCkmdT[i] / C_km[nc+i] - C_km[i] * dCkmdT[nc+i] / std::pow(C_km[i+nc], 2);

            // df[i]/dT = Ni/N0 * (-1/Ci2^2 * dCi2/dT * 1/(n + a[i](1-n)) 
            //                     -1/Ci2 * 1/(n+a[i](1-n))^2 * (dn/dT*(1-a[i])+da[i]/dT*(1-n))
            dfidT[i] =  Nk[i]/N0 * (- 1./std::pow(C_km[nc+i], 2) * dCkmdT[nc+i] * denom
                        - 1./C_km[nc+i] * std::pow(denom, 2) * (detadT * (1.-alpha[i]) + dalphai_dT * (1.-eta)));
        }
    }
    return dfidT;
}

std::vector<double> VdWP::dfi_dxj() {
    // Derivative of guest fugacities w.r.t. x_j

    // Calculate derivatives of eta w.r.t. x_j
    std::vector<double> detadxj = this->deta_dxj();

    // Calculate derivative of guest fugacities
    std::vector<double> dfidxj(nc*nc);
    for (int i = 0; i < nc; i++)
    {
        if (i != water_index)
        {
            double denom = 1./(eta+alpha[i]*(1.-eta));
            for (int j = 0; j < nc; j++)
            {
                dfidxj[i*nc + j] = (this->dNjdxk(i, j)/N0 - this->dN0dxk(j)*Nk[i]/std::pow(N0, 2)) * 1./C_km[nc+i] * denom
                                    - Nk[i]/N0 * 1./C_km[nc+i] * std::pow(denom, 2) * detadxj[j]*(1.-alpha[i]);
            }
        }
    }
    return dfidxj;
}

std::vector<double> VdWP::calc_theta() {
    // Fractional occupancy of cage m by component i
    std::vector<double> theta(n_cages*nc, 0.);
    for (int m = 0; m < n_cages; m++) 
    {
        double sum_cf{ 0. };
        for (int j = 0; j < nc; j++) 
        {
            sum_cf += C_km[nc*m + j] * f[j];
        }
        double denom = 1./(1.+sum_cf);
        for (int i = 0; i < nc; i++)
        {
            theta[nc*m + i] = C_km[nc*m + i] * f[i] * denom;
        }
    }
    return theta;
}

std::vector<double> VdWP::dtheta_dP(std::vector<double>& dfidP) {
    // Derivative of theta_im w.r.t. P
    std::vector<double> dtheta_km(n_cages*nc);

    for (int m = 0; m < n_cages; m++) 
    {
        double sum_cf{ 0. };
        double sum_dcf{ 0. };
        for (int j = 0; j < nc; j++) 
        {
            sum_cf += C_km[nc*m + j] * f[j];
            sum_dcf += dCkmdP[nc*m + j] * f[j] + C_km[nc*m + j] * dfidP[j];
        }
        double denom = 1./(1. + sum_cf);
        for (int i = 0; i < nc; i++)
        {
            dtheta_km[nc*m + i] = (dCkmdP[nc*m + i] * f[i] + C_km[nc*m + i] * dfidP[i]) * denom
                                    - C_km[nc*m + i] * f[i] * std::pow(denom, 2) * sum_dcf;
        }
    }
    return dtheta_km;
}

std::vector<double> VdWP::dtheta_dT(std::vector<double>& dfidT) {
    // Derivative of theta_im w.r.t. T
    std::vector<double> dtheta_km(n_cages*nc);

    for (int m = 0; m < n_cages; m++) 
    {
        double sum_cf{ 0. };
        double sum_dcf{ 0. };
        for (int j = 0; j < nc; j++) 
        {
            sum_cf += C_km[nc*m + j] * f[j];
            sum_dcf += dCkmdT[nc*m + j] * f[j] + C_km[nc*m + j] * dfidT[j];
        }
        double denom = 1./(1. + sum_cf);
        for (int i = 0; i < nc; i++)
        {
            dtheta_km[nc*m + i] = (dCkmdT[nc*m + i] * f[i] + C_km[nc*m + i] * dfidT[i]) * denom
                                    - C_km[nc*m + i] * f[i] * std::pow(denom, 2) * sum_dcf;
        }
    }
    return dtheta_km;
}

std::vector<double> VdWP::dtheta_dxj(std::vector<double>& dfidxj) {
    // Derivative of theta_im w.r.t. x_j
    std::vector<double> dthetadxj(n_cages*nc*nc);
    for (int m = 0; m < n_cages; m++) 
    {
        double sum_cf{ 0. };
        std::vector<double> sum_dcf(nc, 0.);
        for (int j = 0; j < nc; j++) 
        {
            sum_cf += C_km[m*nc + j] * f[j];
            for (int l = 0; l < nc; l++)
            {
                sum_dcf[j] += dfidxj[l*nc + j] * C_km[m*nc + l];
            }
        }
        double denom = 1./(1.+sum_cf);
        for (int i = 0; i < nc; i++)
        {
            for (int k = 0; k < nc; k++)
            {
                dthetadxj[m*nc*nc + i*nc + k] = C_km[m*nc + i] * dfidxj[i*nc+k] * denom 
                                                - C_km[m*nc + i] * f[i] * std::pow(denom, 2) * sum_dcf[k];
            }
        }
    }
    return dthetadxj;
}

double VdWP::calc_eta() {
    // Calculate eta in Newton loop
    // F = sum_j Nj eta / (eta + alpha[j]*(1-eta)) + eta * N0 - v2 = 0
    // dF/deta = sum_j Nj alpha_j / (eta + alpha[j]*(1-eta))^2 + N0 
    double eta_ = 0.;
    while (true)
    {
        // Calculate F and dF
        double F = eta_ * N0 - vm[1];
        double dF = N0;
        for (int k = 0; k < nc; k++)
        {
            if (alpha[k] > 0.)
            {
                double denom = 1./(eta_ + alpha[k] * (1. - eta_));
                F += Nk[k] * eta_ * denom;
                dF += Nk[k] * alpha[k] * std::pow(denom, 2);
            }
            else
            {
                F += Nk[k];
            }
        }

        if (std::fabs(F) < 1e-13)
        {
            break;
        }
        else
        {
            eta_ -= F/dF;
        }
    }
    return eta_;
}

double VdWP::deta_dP() {
    // Differentiate F = 0 implicitly w.r.t. P to obtain dn/dP (n = eta, a = alpha)
    // F = sum_j Nj n / (n + a[j]*(1-n)) + n * N0 - v2 = 0

    // sum_j d/dP Nj n / (n + a[j]*(1-n)) + dn/dP * N0 = 0
    // sum_j Nj * d/dP (n / (n + a[j]*(1-n))) + dn/dP * N0 = 0
    // sum_j Nj * (dn/dP / (n + a[j]*(1-n)) - n * (dn/dP + da[j]/dP * (1-n) - a[j] * dn/dP)/(n+a[j]*(1-n))^2) + dn/dP * N0 = 0
    // sum_j (Nj * (1/(n + a[j]*(1-n)) - (n-a[j])/(n+a[j]*(1-n))^2) + N0) * dn/dP = sum_j n * (1-n) * da[j]/dP/(n+a[j]*(1-n))^2

    // denom = sum_j (Nj * (1/(n + a[j]*(1-n)) - (n-a[j])/(n+a[j]*(1-n))^2)) + N0
    double denom = N0;
    // num = sum_j Nj * n * (1-n) * da[j]/dP / (n+a[j]*(1-n))^2
    double num = 0.;

    for (int j = 0; j < nc; j++)
    {
        if (j != water_index)
        {
            double denominator = 1. / (eta+alpha[j]*(1.-eta));
            // denom += Nj * (1 / (n+a[j]*(1-n)) - n*(1-a[j]) / (n + a[j]*(1-n))^2)
            denom += Nk[j] * (denominator - eta*(1.-alpha[j]) * std::pow(denominator, 2));

            // num += n * (1-n) * da[j]/dP / (n+a[j]*(1-n))^2
            // da[i]/dP = d/dP (Ci1/Ci2) = dCi1/dP / Ci2 - Ci1 * dCi2/dP / Ci2^2
            double dalphaj_dP = dCkmdP[j] / C_km[nc+j] - C_km[j] * dCkmdP[nc+j] / std::pow(C_km[nc+j], 2);
            
            num += Nk[j] * eta * (1.-eta) * dalphaj_dP * std::pow(denominator, 2);
        }
    }

    double detadP = num/denom;
    return detadP;
}

double VdWP::deta_dT() {
    // Differentiate F = 0 implicitly w.r.t. T to obtain dn/dT (n = eta, a = alpha)
    // F = sum_j Nj n / (n + a[j]*(1-n)) + n * N0 - v2 = 0

    // sum_j d/dT Nj n / (n + a[j]*(1-n)) + dn/dT * N0 = 0
    // sum_j Nj * d/dT (n / (n + a[j]*(1-n))) + dn/dT * N0 = 0
    // sum_j Nj * (dn/dT / (n + a[j]*(1-n)) - n * (dn/dT + da[j]/dT * (1-n) - a[j] * dn/dT)/(n+a[j]*(1-n))^2) + dn/dT * N0 = 0
    // sum_j (Nj * (1/(n + a[j]*(1-n)) - (n-a[j])/(n+a[j]*(1-n))^2) + N0) * dn/dT = n * (1-n) * da[j]/dT/(n+a[j]*(1-n))^2

    // denom = sum_j (Nj * (1/(n + a[j]*(1-n)) - (n-a[j])/(n+a[j]*(1-n))^2)) + N0
    double denom = N0;
    // num = sum_j Nj * n * (1-n) * da[j]/dT / (n+a[j]*(1-n))^2
    double num = 0.;

    for (int j = 0; j < nc; j++)
    {
        if (j != water_index)
        {
            double denominator = 1. / (eta+alpha[j]*(1.-eta));
            // denom += Nj * (1 / (n+a[j]*(1-n)) - n*(1-a[j]) / (n + a[j]*(1-n))^2)
            denom += Nk[j] * (denominator - eta*(1.-alpha[j]) * std::pow(denominator, 2));

            // num += Nj * n * (1-n) * da[j]/dT / (n+a[j]*(1-n))^2
            // da[i]/dT = d/dT (Ci1/Ci2) = dCi1/dT / Ci2 - Ci1 * dCi2/dT / Ci2^2
            double dalphaj_dT = dCkmdT[j] / C_km[nc+j] - C_km[j] * dCkmdT[nc+j] / std::pow(C_km[nc+j], 2);
            
            num += Nk[j] * eta * (1.-eta) * dalphaj_dT * std::pow(denominator, 2);
        }
    }

    double detadT = num/denom;
    return detadT;
}

std::vector<double> VdWP::deta_dxj() {
    // Differentiate F = 0 implicitly w.r.t. xk to obtain dn/dxk (n = eta, a = alpha)
    // F = sum_j Nj n / (n + a[j]*(1-n)) + n * N0 - v2 = 0
    std::vector<double> detadxj(nc);
    
    // sum_j d/dxk (Nj n / (n + a[j]*(1-n)) + dn/dxk N0 + eta * dN0/dxk = 0
    // sum_j (dNj/dxk n / (n + a[j]*(1-n)) + Nj dn/dxk / (n + a[j]*(1-n)) - n * (1-a[j]) * dn/dxk / (n + a[j]*(1-n))^2) + dn/dxk N0 + n dN0/dxk = 0
    // dn/dxk (sum_j (Nj / (n + a[j]*(1-n)) - n*(1-a[j])/(n + a[j]*(1-n))^2) + N0) = -n dN0/dxk - sum_j dNj/dxk n / (n+a[j]*(1-n))
    // dn/dxk = num / denom
    
    // Calculate denominator
    // denom = sum_j [Nj * (1 / (n+a[j]*(1-n)) - n*(1-a[j]) / (n + a[j]*(1-n))^2)] + N0
    double denom = N0;
    for (int j = 0; j < nc; j++)
    {
        if (j != water_index)
        {
            double denominator = 1. / (eta+alpha[j]*(1.-eta));
            // += Nj * (1 / (n+a[j]*(1-n)) - n*(1-a[j]) / (n + a[j]*(1-n))^2)
            denom += Nk[j] * (denominator - eta*(1.-alpha[j]) * std::pow(denominator, 2));
        }
    }

    // Calculate numerator
    // num = -n dN0/dxk - sum_j [dNj/dxk * n / (n + a[j]*(1-n))]
    for (int k = 0; k < nc; k++)
    {
        double num = -eta * this->dN0dxk(k);
        for (int j = 0; j < nc; j++)
        {
            if (j != water_index)
            {
                num -= this->dNjdxk(j, k) * eta / (eta + alpha[j]*(1.-eta));
            }
        }
        detadxj[k] = num / denom;
    }
    return detadxj;
}

double VdWP::dN0dxk(int k) 
{
    // N0 = v0 + v1 - Σj Nj = v0 + v1 - Σj xj/xw
    if (k == water_index)
    {
        // dN0/dxw = Σj xj/xw^2 = (1-xw)/xw^2
        return (1.-x[water_index]) / std::pow(x[water_index], 2);
    }
    else
    {
        // dN0/dxk = -1/xw
        return -1. / x[water_index];
    }
}

double VdWP::dNjdxk(int j, int k)
{
    // Nj = xj/xw
    if (j == water_index)
    {
        // Nw = 0
        return 0.;
    }
    else if (j == k)
    {
        // dNj/dxj = 1/xw
        return 1./x[water_index];
    }
    else if (k == water_index)
    {
        // dNj/dxw = -xj/xw^2
        return -x[j] / std::pow(x[water_index], 2);
    }
    else
    {
        // dNj/dxk = 0 if j != k
        return 0.;
    }
}

double VdWP::calc_dmuH() {  
    // Fractional occupancy of cage m by component i
    theta_km = this->calc_theta();

    // Contribution of cage occupancy to chemical potential
    double dmu_H{ 0. };
    for (int m = 0; m < n_cages; m++) 
    {
        double sumtheta{ 0. };
        for (int j = 0; j < nc; j++) 
        {
            sumtheta += theta_km[nc*m + j];
        }
        dmu_H += vm[m] * std::log(1.-sumtheta);
    }
    return dmu_H;
}

double VdWP::ddmuH_dP(std::vector<double>& dfidP) {
    // Derivative of Δmu_wH w.r.t. P

    // Calculate derivatives of theta_km w.r.t. P
    std::vector<double> dthetadP = this->dtheta_dP(dfidP);

    double ddmuHdP{ 0. };
    for (int m = 0; m < n_cages; m++)
    {
        double sum_theta = std::accumulate(theta_km.begin()+m*nc, theta_km.begin()+(m+1)*nc, 0.);
        double sum_dtheta{ 0. };
        for (int j = 0; j < nc; j++) 
        {
            sum_dtheta += dthetadP[nc*m + j];
        }
        ddmuHdP -= vm[m] / (1.-sum_theta) * sum_dtheta;
    }
    return ddmuHdP;
}

double VdWP::ddmuH_dT(std::vector<double>& dfidT) {
    // Derivative of Δmu_wH w.r.t. T
    
    // Calculate derivatives of theta_km w.r.t. T
    std::vector<double> dthetadT = this->dtheta_dT(dfidT);

    double ddmuHdT{ 0. };
    for (int m = 0; m < n_cages; m++)
    {
        double sum_theta = std::accumulate(theta_km.begin()+m*nc, theta_km.begin()+(m+1)*nc, 0.);
        double sum_dtheta{ 0. };
        for (int j = 0; j < nc; j++) 
        {
            sum_dtheta += dthetadT[nc*m + j];
        }
        ddmuHdT -= vm[m] / (1.-sum_theta) * sum_dtheta;
    }
    return ddmuHdT;
}

std::vector<double> VdWP::ddmuH_dxj(std::vector<double>& dfidxj) {
    // Derivative of Δmu_wH w.r.t. x_j

    // Calculate derivatives of theta_km w.r.t. x_j
    std::vector<double> dthetadxj = this->dtheta_dxj(dfidxj);

    std::vector<double> ddmuHdxj(nc, 0.);
    for (int m = 0; m < n_cages; m++)
    {
        double sum_theta = std::accumulate(theta_km.begin()+m*nc, theta_km.begin()+(m+1)*nc, 0.);
        for (int k = 0; k < nc; k++)
        {
            double sum_dtheta{ 0. };
            for (int j = 0; j < nc; j++)
            {
                sum_dtheta += dthetadxj[m*nc*nc + j*nc + k];
            }
            ddmuHdxj[k] -= vm[m] / (1.-sum_theta) * sum_dtheta;
        }
    }
    return ddmuHdxj;
}

std::vector<double> VdWP::xH() {
    std::vector<double> xH(nc, 0.);

    double denominator{ 1. }; // denominator of eq. 3.50
    for (int m = 0; m < n_cages; m++) 
    {
        for (int i = 0; i < nc; i++) 
        {
            denominator += vm[m] * theta_km[nc*m + i];
        }
    }
    
    xH[water_index] = 1.;
    for (int i = 0; i < nc; i++) 
    {
        if (i != water_index)
        {
            double numerator{ 0. }; // numerator of eq. 3.50
            for (int m = 0; m < n_cages; m++) 
            { 
                numerator += vm[m] * theta_km[nc*m + i]; 
            }
            xH[i] = numerator / denominator;
            xH[water_index] -= xH[i];
        }
    }
    return xH;
}

double VdWP::lnphii(int i) {
    return std::log(f[i]/(x[i]*p));
}

std::vector<double> VdWP::dlnphi_dP() {
    // Calculate derivative of lnphi's w.r.t. P

    // Calculate derivatives of guest fugacities
    std::vector<double> dfidP = this->dfi_dP();
    for (int i = 0; i < nc; i++)
    {
        if (i != water_index)
        {
            // Derivative of lnphii w.r.t. P for guest molecules
            // dlnphi/dP = 1/f[i] * df[i]/dP - 1/p
            dlnphidP[i] = 1./f[i] * dfidP[i] - 1./p;
        }
        else
        {
            dlnphidP[i] = 1./f[i] * this->dfw_dP(dfidP) - 1./p;
        }
    }

    return dlnphidP;
}

std::vector<double> VdWP::dlnphi_dT() {
    // Calculate derivative of lnphi's w.r.t. T

    // Calculate derivative of guest fugacities
    std::vector<double> dfidT = this->dfi_dT();
    for (int i = 0; i < nc; i++)
    {
        if (i != water_index)
        {
            // Derivative of lnphii w.r.t. T for guest molecules
            // dlnphi/dP = 1/f[i] * df[i]/dT
            dlnphidT[i] = 1./f[i] * dfidT[i];
        }
        else
        {
            dlnphidT[i] = 1./f[i] * this->dfw_dT(dfidT);
        }
    }
    return dlnphidT;
}

double VdWP::dlnphii_dnj(int i, int k) {
	// Evaluate derivative of fugacity coefficient of component i w.r.t. n_k
    std::vector<double> dlnphiidxj(nc);

    // Calculate derivative of guest fugacities w.r.t. x_j
    std::vector<double> dfidxj = this->dfi_dxj();
	if (i == water_index)
	{
		// Calculate derivative of fwH w.r.t. x_j
        std::vector<double> dfwdxj = this->dfw_dxj(dfidxj);
        for (int j = 0; j < nc; j++)
        {
            dlnphiidxj[i*nc + j] = 1./f[i] * dfwdxj[i*nc + j];
        }
        dlnphiidxj[i*nc + i] -= 1./x[i];
	}
	else
	{
		for (int j = 0; j < nc; j++)
        {
            // Derivative of lnphii w.r.t. x_j for guest molecules
            // dlnphi/dxj = 1/f[i] * df[i]/dxj - dij/xj
            dlnphiidxj[j] = 1./f[i] * dfidxj[i*nc + j];
        }
        dlnphiidxj[i] -= 1./x[i];
	}

	// Translate from dxj to dnj
	return this->dxj_to_dnk(dlnphiidxj, this->n_iterator, k);
}

double VdWP::dlnphii_dxj(int i, int j)
{
    // Evaluate derivative of fugacity coefficient of component i w.r.t. x_j
    double dlnphiidxj = 0.;
    
    // Calculate derivative of guest fugacities w.r.t. x_j
    std::vector<double> dfidxj = this->dfi_dxj();
	if (i == water_index)
	{
		// Calculate derivative of fwH w.r.t. x_j
        std::vector<double> dfwdxj = this->dfw_dxj(dfidxj);
        dlnphiidxj = 1./f[i] * dfwdxj[i*nc + j];
        if (i == j)
        {
            dlnphiidxj -= 1./x[i];
        }
	}
	else
	{
        // Derivative of lnphii w.r.t. x_j for guest molecules
        // dlnphi/dxj = 1/f[i] * df[i]/dxj - dij/xj
        dlnphiidxj = 1./f[i] * dfidxj[i*nc + j];
        if (i == j)
        {
            dlnphiidxj -= 1./x[i];
        }
	}
    return dlnphiidxj;
}

int VdWP::dP_test(double p_, double T_, std::vector<double>& x_, double tol) {
    // Test analytical derivatives of eta, theta and dmuH with respect to P
    int error_output = 0;
    double dp = 1e-5;

    // Calculate eta, theta, dmuH at p, T
    this->init_PT(p_, T_);
    this->solve_PT(x_.begin(), false);
    std::vector<double> f0 = f;
    
    double eta0 = this->calc_eta();
    double dmu_H0 = this->calc_dmuH();
    std::vector<double> theta0 = this->calc_theta();
    std::vector<double> Ckm0 = C_km;

    std::vector<double> dfidP = this->dfi_dP();
    double deta = this->deta_dP();
    std::vector<double> dtheta = this->dtheta_dP(dfidP);
    double ddmuH = this->ddmuH_dP(dfidP);
    double dfwH = this->dfw_dP(dfidP);

    // Calculate eta, theta, dmuH at p1, T
    this->init_PT(p_+dp, T_);
    this->solve_PT(x_.begin(), false);
    std::vector<double> f1 = f;

    double eta1 = this->calc_eta();
    double dmu_H1 = this->calc_dmuH();
    std::vector<double> theta1 = this->calc_theta();
    std::vector<double> Ckm1 = C_km;
    
    double deta_num = (eta1-eta0)/dp;
    double d = (deta_num-deta)/deta;
    if (std::fabs(d) > tol)
    {
        print("deta/dP", std::vector<double>{deta, deta_num, d});
        error_output++;
    }
    double ddmuH_num = (dmu_H1-dmu_H0)/dp;
    d = (ddmuH_num-ddmuH)/ddmuH;
    if (std::fabs(d) > tol)
    {
        print("ddmuH/dP", std::vector<double>{ddmuH, ddmuH_num, d});
        error_output++;
    }

    for (int m = 0; m < n_cages; m++)
    {
        for (int i = 0; i < nc; i++)
        {
            double dtheta_num = (theta1[m*nc + i] - theta0[m*nc + i])/dp;
            d = (dtheta_num-dtheta[m*nc + i])/dtheta[m*nc+i];
            if (std::fabs(d) > tol)
            {
                print("dtheta/dP", std::vector<double>{dtheta[m*nc+i], dtheta_num, d});
                error_output++;
            }
            double dCkm_num = (Ckm1[m*nc + i] - Ckm0[m*nc + i])/dp;
            d = (dCkm_num-dCkmdP[m*nc + i])/dCkmdP[m*nc+i];
            if (std::fabs(d) > tol)
            {
                print("dCkm/dP", std::vector<double>{dCkmdP[m*nc+i], dCkm_num, d});
                error_output++;
            }
        }
    }

    for (int i = 0; i < nc; i++)
    {
        double df_num = (f1[i]-f0[i])/dp;
        if (i == water_index)
        {
            d = (df_num-dfwH)/dfwH;
            if (std::fabs(d) > tol)
            {
                print("dfwH/dP", std::vector<double>{dfwH, df_num, d});
                error_output++;
            }
        }
        else
        {
            d = (df_num-dfidP[i])/dfidP[i];
            if (std::fabs(d) > tol)
            {
                print("dfi/dP", std::vector<double>{dfidP[i], df_num, d});
                error_output++;
            }
        }
    }

    return error_output;
}

int VdWP::dT_test(double p_, double T_, std::vector<double>& x_, double tol) {
    // Test analytical derivatives of eta, theta and dmuH with respect to T
    int error_output = 0;
    double dT = 1e-5;

    // Calculate eta, theta, dmuH at p, T
    this->init_PT(p_, T_);
    this->solve_PT(x_.begin(), false);
    std::vector<double> f0 = f;

    double eta0 = this->calc_eta();
    double dmu_H0 = this->calc_dmuH();
    std::vector<double> theta0 = this->calc_theta();
    std::vector<double> Ckm0 = C_km;

    std::vector<double> dfidT = this->dfi_dT();
    double deta = this->deta_dT();
    std::vector<double> dtheta = this->dtheta_dT(dfidT);
    double ddmuH = this->ddmuH_dT(dfidT);
    double dfwH = this->dfw_dT(dfidT);

    // Calculate eta, theta, dmuH at p1, T
    this->init_PT(p_, T_+dT);
    this->solve_PT(x_.begin(), false);
    std::vector<double> f1 = f;

    double eta1 = this->calc_eta();
    double dmu_H1 = this->calc_dmuH();
    std::vector<double> theta1 = this->calc_theta();
    std::vector<double> Ckm1 = C_km;
    
    double deta_num = (eta1-eta0)/dT;
    double d = (deta_num-deta)/deta;
    if (std::fabs(d) > tol)
    {
        print("deta/dT", std::vector<double>{deta, deta_num, d});
        error_output++;
    }
    double ddmuH_num = (dmu_H1-dmu_H0)/dT;
    d = (ddmuH_num-ddmuH)/ddmuH;
    if (std::fabs(d) > tol)
    {
        print("ddmuH/dT", std::vector<double>{ddmuH, ddmuH_num, d});
        error_output++;
    }

    for (int m = 0; m < n_cages; m++)
    {
        for (int i = 0; i < nc; i++)
        {
            double dtheta_num = (theta1[m*nc + i] - theta0[m*nc + i])/dT;
            d = (dtheta_num-dtheta[m*nc + i])/dtheta[m*nc+i];
            if (std::fabs(d) > tol)
            {
                print("dtheta/dT", std::vector<double>{dtheta[m*nc+i], dtheta_num, d});
                error_output++;
            }
            
            double dCkm_num = (Ckm1[m*nc + i] - Ckm0[m*nc + i])/dT;
            d = (dCkm_num-dCkmdT[m*nc + i])/dCkmdT[m*nc+i];
            if (std::fabs(d) > tol)
            {
                print("dCkm/dT", std::vector<double>{dCkmdT[m*nc+i], dCkm_num, d});
                error_output++;
            }
        }
    }

    for (int i = 0; i < nc; i++)
    {
        double df_num = (f1[i]-f0[i])/dT;
        if (i == water_index)
        {
            d = (df_num-dfwH)/dfwH;
            if (std::fabs(d) > tol)
            {
                print("dfwH/dT", std::vector<double>{dfwH, df_num, d});
                error_output++;
            }
        }
        else
        {
            d = (df_num-dfidT[i])/dfidT[i];
            if (std::fabs(d) > tol)
            {
                print("dfi/dT", std::vector<double>{dfidT[i], df_num, d});
                error_output++;
            }
        }
    }

    return error_output;
}
