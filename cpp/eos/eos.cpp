#include <iostream>
#include <cmath>
#include <vector>
#include <numeric>
#include <algorithm>
#include <cassert>

#include "dartsflash/global/global.hpp"
#include "dartsflash/global/units.hpp"
#include "dartsflash/eos/eos.hpp"
#include <Eigen/Dense>

EoS::EoS(CompData& comp_data) 
{
    this->compdata = comp_data;
    this->nc = comp_data.nc;
    this->ni = comp_data.ni;
    this->ns = nc + ni;
    this->units = comp_data.units;
	
    this->dlnphidP.resize(ns);
    this->dlnphidT.resize(ns);
    this->dlnphidn.resize(ns*ns);
    this->gpure.resize(ns);
}

void EoS::set_eos_range(int i, const std::vector<double>& range)
{
    // Define range of applicability for specific EoS
    this->eos_range[i] = range;
    return;
}

bool EoS::eos_in_range(std::vector<double>::iterator n_it)
{
    // Check if EoS is applicable for specific state according to specified ranges
    for (auto& it: this->eos_range) {
        double N_ = std::accumulate(n_it, n_it + this->ns, 0.);
        double xi = *(n_it + it.first) / N_;
		if (xi < it.second[0] || xi > it.second[1])
        {
            return false;
        }
	}
    return true;
}

void EoS::solve_PT(double p_, double T_, std::vector<double>& n_, int start_idx, bool second_order)
{
    // Calculate composition-independent and composition-dependent EoS-parameters
    this->init_PT(p_, T_);
    this->solve_PT(n_.begin() + start_idx, second_order);
    return;
}

std::vector<double> EoS::lnphi()
{
    // Return vector of lnphi for each component
    std::vector<double> ln_phi(ns);
    for (int i = 0; i < ns; i++)
    {
        ln_phi[i] = this->lnphii(i);
    }
    return ln_phi;
}

std::vector<double> EoS::dlnphi_dn() {
    for (int i = 0; i < ns; i++)
    {
        for (int j = 0; j < ns; j++)
        {
            dlnphidn[i * ns + j] = this->dlnphii_dnj(i, j);
        }
    }
    return dlnphidn;
}

std::vector<double> EoS::dlnphi_dP() {
    for (int i = 0; i < ns; i++)
    {
        dlnphidP[i] = this->dlnphii_dP(i);
    }
    return dlnphidP;
}

std::vector<double> EoS::dlnphi_dT() {
    for (int i = 0; i < ns; i++)
    {
        dlnphidT[i] = this->dlnphii_dT(i);
    }
    return dlnphidT;
}

std::vector<double> EoS::fugacity(double p_, double T_, std::vector<double>& x_)
{
    // Calculate component fugacities in mixture
    if (this->eos_in_range(x_.begin()))
    {
	    // Calculate fugacity coefficients and fugacity
        std::vector<double> fi(nc);
        this->solve_PT(p_, T_, x_, 0, false);
        for (int i = 0; i < nc; i++)
        {
            fi[i] = std::exp(this->lnphii(i)) * x_[i] * p;
        }
        return fi;
    }
    else
    {
        return std::vector<double>(nc, NAN);
    }
}

Eigen::MatrixXd EoS::calc_hessian(std::vector<double>::iterator n_it)
{
    // Calculate dlnphii/dnj
    this->solve_PT(n_it);
    this->dlnphi_dn();

    // Construct Hessian matrix of Gibbs energy surface
    Eigen::MatrixXd H = Eigen::MatrixXd(nc, nc);
    for (int j = 0; j < nc; j++)
    {
        for (int i = j; i < nc; i++)
        {
            H(i, j) = dlnphidn[i*nc + j];  // PHI contribution
            H(j, i) = dlnphidn[i*nc + j];  // PHI contribution
        }
        H(j, j) += 1. / *(n_it + j);  // U contribution
    }

    return H;
}

bool EoS::is_convex(std::vector<double>::iterator n_it)
{
    // Check if GE/TPD surface at composition n is convex
    Eigen::MatrixXd H = this->calc_hessian(n_it);

    // Perform Cholesky decomposition; only possible with positive definite matrix
    // Positive definite Hessian matrix corresponds to local minimum
    Eigen::LLT<Eigen::MatrixXd> lltOfA(H);
    if(lltOfA.info() == Eigen::NumericalIssue)
    {
        return false;
    }
    else
    {
        return true;
    }
}

bool EoS::is_convex(double p_, double T_, std::vector<double>& n_, int start_idx)
{
    // Check if GE/TPD surface at P, T and composition n is convex
    this->init_PT(p_, T_);
    return this->is_convex(n_.begin() + start_idx);
}

double EoS::calc_condition_number(double p_, double T_, std::vector<double>& n_, int start_idx)
{
    if (this->eos_in_range(n_.begin() + start_idx))
    {
        // Calculate condition number of Hessian to evaluate curvature of GE/TPD surface at P, T and composition n
        this->init_PT(p_, T_);
        Eigen::MatrixXd H = this->calc_hessian(n_.begin() + start_idx);

        // Get the condition number for stability if Newton was used
        Eigen::VectorXd eigen = H.eigenvalues().real();
        double min_eigen = *std::min_element(eigen.begin(), eigen.end());
        double max_eigen = *std::max_element(eigen.begin(), eigen.end());
        return (min_eigen > 0.) ? max_eigen/min_eigen : NAN;
    }
    else
    {
        return NAN;
    }
}

double EoS::cpi(double T_, int i)
{
    // Cpi/R: Ideal gas heat capacity at constant pressure
    return this->compdata.cpi[i][0]
         + this->compdata.cpi[i][1] * T_
         + this->compdata.cpi[i][2] * std::pow(T_, 2)
         + this->compdata.cpi[i][3] * std::pow(T_, 3);
}

double EoS::hi(double T_, int i)
{
    // Hi/R: Ideal gas enthalpy
    return this->compdata.cpi[i][0] * (T_-this->compdata.T_0)
         + 1. / 2 * this->compdata.cpi[i][1] * (std::pow(T_, 2)-std::pow(this->compdata.T_0, 2))
         + 1. / 3 * this->compdata.cpi[i][2] * (std::pow(T_, 3)-std::pow(this->compdata.T_0, 3))
         + 1. / 4 * this->compdata.cpi[i][3] * (std::pow(T_, 4)-std::pow(this->compdata.T_0, 4));
}

double EoS::si_PT(double p_, double T_, int i)
{
    // Si/R: ideal gas entropy at constant pressure
    return this->compdata.cpi[i][0] * std::log(T_ / this->compdata.T_0)
         + this->compdata.cpi[i][1] * (T_ - this->compdata.T_0)
         + 1. / 2 * this->compdata.cpi[i][2] * (std::pow(T_, 2)-std::pow(this->compdata.T_0, 2))
         + 1. / 3 * this->compdata.cpi[i][3] * (std::pow(T_, 3)-std::pow(this->compdata.T_0, 3))
         - std::log(p_ / this->compdata.P_0);
}

double EoS::si_VT(double V_, double T_, int i)
{
    // Si/R: ideal gas entropy at constant volume
    return this->compdata.cpi[i][0] * std::log(T_ / this->compdata.T_0)
         + this->compdata.cpi[i][1] * (T_ - this->compdata.T_0)
         + 1. / 2 * this->compdata.cpi[i][2] * (std::pow(T_, 2)-std::pow(this->compdata.T_0, 2))
         + 1. / 3 * this->compdata.cpi[i][3] * (std::pow(T_, 3)-std::pow(this->compdata.T_0, 3))
         - std::log(T_ / this->compdata.T_0) - std::log(V_ / this->compdata.V_0);
}

double EoS::G_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Ideal contribution to Gibbs free energy
    double Gi = 0.;
    for (int i = 0; i < ns; i++)
    {
        double xi = x_[start_idx+i];
        if (xi > 0.)
        {
            Gi += xi * std::log(xi);
        }
    }

    // Ideal + residual Gibbs free energy
    double Gr = this->Gr_PT(p_, T_, x_, start_idx) / (this->units.R * T_);
    return Gi + Gr;
}

double EoS::Gr_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate Gibbs energy of mixture at P, T, x
    std::vector<double> GriTP = this->Gri_PT(p_, T_, x_, start_idx);
    
    double G = 0.;
    bool nans = false;
    for (int i = 0; i < ns; i++)
    {
        if (!std::isnan(GriTP[i]))
        {
            G += x_[start_idx + i] * GriTP[i];
        }
        else
        {
            nans = true;
        }
    }
    // If all NANs, G will be equal to zero, so return NAN; else return G
    return (nans && G == 0.) ? NAN : G;
}

std::vector<double> EoS::Gri_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate partial molar Gibbs energy at P,T,x
    if (this->eos_in_range(x_.begin() + start_idx))
    {
        std::vector<double> GriTP(ns);

        this->solve_PT(p_, T_, x_, start_idx, false);
        for (int i = 0; i < ns; i++)
        {
            if (x_[start_idx + i] > 0.)
            {
                GriTP[i] = this->lnphii(i) * this->units.R * T;
            }
            else
            {
                GriTP[i] = NAN;
            }
        }
        return GriTP;
    }
    else
    {
        return std::vector<double>(ns, NAN);
    }
}

double EoS::H_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Ideal contribution to enthalpy
    double Hi = 0.;
    for (int i = 0; i < ns; i++)
    {
        double xi = x_[start_idx+i];
        Hi += xi * this->hi(T_, i);
    }

    // Ideal + residual enthalpy
    double Hr = this->Hr_PT(p_, T_, x_, start_idx) / this->units.R;
    return Hi + Hr;  // H/R
}

double EoS::Hr_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate residual enthalpy of mixture at P, T, x
    std::vector<double> HriTP = this->Hri_PT(p_, T_, x_, start_idx);

    double H = 0.;
    bool nans = false;
    for (int i = 0; i < ns; i++)
    {
        if (!std::isnan(HriTP[i]))
        {
            H += x_[start_idx + i] * HriTP[i];
        }
        else
        {
            nans = true;
        }
    }
    // If all NANs, H will be equal to zero, so return NAN; else return H
    return (nans && H == 0.) ? NAN : H;
}

std::vector<double> EoS::Hri_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate partial molar enthalpy at P,T,x
    std::vector<double> HriTP(ns);

    this->solve_PT(p_, T_, x_, start_idx, true);
    dlnphidT = this->dlnphi_dT();
    double RT2 = this->units.R * std::pow(T, 2);
    for (int i = 0; i < ns; i++)
    {
        HriTP[i] = -RT2 * dlnphidT[i];
    }
    return HriTP;
}

double EoS::S_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Ideal contribution to entropy
    double Si = 0.;
    for (int i = 0; i < ns; i++)
    {
        double xi = x_[start_idx+i];
        Si += xi * this->si_PT(p_, T_, i) - xi * std::log(xi);
    }

    // Ideal + residual entropy
    double Sr = this->Sr_PT(p_, T_, x_, start_idx) / this->units.R;
    return Si + Sr;
}

double EoS::Sr_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate residual entropy of mixture at P, T, x
    std::vector<double> SriTP = this->Sri_PT(p_, T_, x_, start_idx);

    double S = 0.;
    bool nans = false;
    for (int i = 0; i < ns; i++)
    {
        if (!std::isnan(SriTP[i]))
        {
            S += x_[start_idx + i] * SriTP[i];
        }
        else
        {
            nans = true;
        }
    }
    // If all NANs, S will be equal to zero, so return NAN; else return S
    return (nans && S == 0.) ? NAN : S;
}

std::vector<double> EoS::Sri_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate partial molar entropy of mixture at P, T, x
    std::vector<double> SriTP(ns);
    std::vector<double> HriTP = this->Hri_PT(p_, T_, x_, start_idx);
    std::vector<double> GriTP = this->Gri_PT(p_, T_, x_, start_idx);

    double T_inv = 1./T;
    for (int i = 0; i < ns; i++)
    {
        SriTP[i] = (HriTP[i] - GriTP[i]) * T_inv;
    }
    return SriTP;
}

/*
double EoS::A_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Ideal + residual Helmholtz free energy
    double Ai = this->ideal.A(T_, x_.begin() + start_idx);
    double Ar = this->Ar_PT(p_, T_, x_, start_idx);
    return Ai + Ar;
}

double EoS::Ar_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate residual Helmholtz free energy of mixture at P, T, x
}

std::vector<double> EoS::Ari_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate partial molar Helmholtz free energy of mixture at P, T, x
}

double EoS::U_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Ideal + residual internal energy
    double Ui = this->ideal.U(T_, x_.begin() + start_idx);
    double Ur = this->Ur_PT(p_, T_, x_, start_idx);
    return Ui + Ur;
}

double EoS::Ur_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate residual internal energy of mixture at P, T, x
}

std::vector<double> EoS::Uri_PT(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate partial molar internal energy of mixture at P, T, x
}
*/

std::vector<double> EoS::G_PT_pure()
{
    // Calculate pure component Gibbs energy at P, T
    // if (p_ != p || T_ != T_)
    {
        // this->init_PT(p_, T_);
        gpure = std::vector<double>(nc, NAN);

        // Loop over components
        for (int i = 0; i < nc; i++)
        {
            std::vector<double> n_(nc, 0.);
            n_[i] = 1.;

            if (this->eos_in_range(n_.begin()))
            {
                this->solve_PT(n_.begin(), false);

                if (this->select_root(n_.begin()))
                {
                    gpure[i] = this->lnphii(i);
                }
                else
                {
                    gpure[i] = NAN;
                }
            }
        }
    }
    return gpure;
}

double EoS::property_of_mixing(std::vector<double>& x_, std::vector<double>& mixture_prop, std::vector<double>& pure_prop)
{
    // Calculate Gibbs energy of mixing of mixture at P, T, x
    if (this->eos_in_range(x_.begin()))
    {
        double prop_of_mixing = 0.;
        for (int i = 0; i < ns; i++)
        {
            if (x_[i] > 0.)
            {
                prop_of_mixing += x_[i] * (mixture_prop[i] - pure_prop[i]);
            }
        }
        return prop_of_mixing;
    }
    else
    {
        return NAN;
    }
}

std::vector<double> EoS::mix_min(std::vector<double>& x_, std::vector<double>& gpure_, double& gmix)
{
    // Find local minimum of Gibbs energy of mixing
    // Last component is dependent variable s.t. Σi ni - 1 = 0
    std::vector<double> xx = x_;

    double obj = 0.;
    Eigen::VectorXd g(ns-1), X(ns-1);
    Eigen::MatrixXd H(ns-1, ns-1);

    // Initialize solution vector
    for (int i = 0; i < ns-1; i++)
    {
        X(i) = xx[i];
    }

    // Solve fugacity coefficient and derivatives
    this->solve_PT(xx.begin(), false);
    double sumX = std::accumulate(X.data(), X.data() + X.size(), 0.);
    double ln_1x = std::log(1.-sumX);
    obj = (1.-sumX) * (this->lnphii(ns-1) + ln_1x - gpure_[ns-1]);
    for (int i = 0; i < ns-1; i++)
    {
        obj += X[i] * (this->lnphii(i) + std::log(X[i]) - gpure_[i]);
    }
    double obj_init = obj;

    // Start Newton loop
    int iter = 1;
    while (iter < 10)
    {
        // Entries for xj
        this->solve_PT(xx.begin(), true);
        for (int j = 0; j < ns-1; j++)
        {
            // Gradient vector for xj entries dG/dxj
            g(j) = this->lnphii(j) + std::log(X[j]) - gpure_[j] - this->lnphii(ns-1) - ln_1x + gpure_[ns-1];

            for (int k = j; k < ns-1; k++)
            {
                // Hessian for d2G/dxjdxk
                double Hjk = this->dlnphii_dxj(j, k) - this->dlnphii_dxj(ns-1, k);
                H(j, k) = Hjk;
                H(k, j) = Hjk;
            }
            H(j, j) += 1./xx[j] + 1./(1.-sumX);
        }

        if (g.squaredNorm() < 1e-3)
        {
            gmix = obj;
            return xx;
        }

        // Solve Newton step
        Eigen::LLT<Eigen::MatrixXd> lltOfH(H);
        Eigen::VectorXd dX;
        if (lltOfH.info() == Eigen::NumericalIssue )
        {
            // print("LLT of H not possible", 1);
            gmix = (obj < obj_init) ? obj : obj_init;
            return (obj < obj_init) ? xx : x_;
        }
        else
        {
            dX = lltOfH.solve(g);
        }

        bool reduced = false;
        double lamb = 0.5;
        double obj_old = obj;
        Eigen::VectorXd X_old = X;
        int iter2 = 1;
        while (true)
        {
            X = X_old - lamb*dX;
            xx = std::vector<double>(X.data(), X.data() + X.size());
            sumX = std::accumulate(X.data(), X.data() + X.size(), 0.);
            ln_1x = std::log(1.-sumX);
            xx.push_back(1.-sumX);

            if (!std::count_if(xx.begin(), xx.end(), [](double xi) { return (xi < 0.); }))
            {
                // Calculate objective function
                this->solve_PT(xx.begin(), false);
                obj = (1.-sumX) * (this->lnphii(ns-1) + ln_1x - gpure_[ns-1]);
                for (int i = 0; i < ns-1; i++)
                {
                    obj += X[i] * (this->lnphii(i) + std::log(X[i]) - gpure_[i]);
                }

                if (obj < obj_old)
                {
                    reduced = true;
                }
            }

            if (iter2 == 10)
            {
                return (obj < obj_init) ? xx : x_;
            }
            else if (!reduced)
            {
                lamb *= 0.5;
                iter2++;
            }
            else
            {
                break;
            }
        }

        iter++;
    }
    gmix = obj;
    return xx;
}

std::vector<double> EoS::dlnphi_dn_num(double p_, double T_, std::vector<double>& n_, double dn) {
    // Numerical derivative of fugacity coefficient w.r.t. composition
    this->solve_PT(p_, T_, n_, 0, false);
    std::vector<double> ln_phi = this->lnphi();
    for (int j = 0; j < ns; j++)
    {
        n_[j] += dn;
        this->solve_PT(p_, T_, n_, 0, false);
        std::vector<double> ln_phi1 = this->lnphi();
        n_[j] -= dn;
        for (int i = 0; i < ns; i++)
        {
            this->dlnphidn[j*ns + i] = (ln_phi1[i]-ln_phi[i])/dn;
        }
    }
    return dlnphidn;
}

std::vector<double> EoS::dlnphi_dT_num(double p_, double T_, std::vector<double>& n_, double dT) {
    // Numerical derivative of fugacity coefficient w.r.t. temperature
    std::vector<double> ln_phi = lnphi();

    T_ += dT;
    this->solve_PT(p_, T_, n_, 0, false);
    std::vector<double> ln_phi1 = this->lnphi();
    T_ -= dT;
    for (int i = 0; i < ns; i++)
    {
        this->dlnphidT[i] = (ln_phi1[i]-ln_phi[i])/dT;
    }
    return this->dlnphidT;
}

std::vector<double> EoS::dlnphi_dP_num(double p_, double T_, std::vector<double>& n_, double dP) {
    // Numerical derivative of fugacity coefficient w.r.t. pressure
    std::vector<double> ln_phi = lnphi();

    p_ += dP;
    this->solve_PT(p_, T_, n_, 0, false);
    std::vector<double> ln_phi1 = this->lnphi();
    p_ -= dP;
    for (int i = 0; i < ns; i++)
    {
        this->dlnphidP[i] = (ln_phi1[i]-ln_phi[i])/dP;
    }
    return this->dlnphidP;
}

double EoS::dxj_to_dnk(std::vector<double>& dlnphiidxj, std::vector<double>::iterator n_it, int k) {
    // Translate from dlnphii/dxj to dlnphii/dnk
	// dlnphii/dnk = 1/V * [dlnphii/dxk - sum_j xj dlnphii/dxj]
    double nT_inv = 1./std::accumulate(n_it, n_it + this->ns, 0.);
	double dlnphiidnk = dlnphiidxj[k];
	for (int j = 0; j < ns; j++)
	{
        double nj = *(n_it + j);
		dlnphiidnk -= nj * nT_inv * dlnphiidxj[j];
	}
	dlnphiidnk *= nT_inv;
    return dlnphiidnk;
}

std::vector<double> EoS::dxj_to_dnk(std::vector<double>& dlnphiidxj, std::vector<double>::iterator n_it)
{
    // Translate from dlnphii/dxj to dlnphii/dnk
	// dlnphii/dnk = 1/V * [dlnphii/dxk - sum_j xj dlnphii/dxj]
    double nT_inv = 1./std::accumulate(n_it, n_it + this->ns, 0.);
    std::vector<double> dlnphiidnk(ns*ns);

    for (int i = 0; i < ns; i++)
    {
        double sum_xj = 0.;
        for (int j = 0; j < ns; j++)
        {
            double nj = *(n_it + j);
            sum_xj += nj * nT_inv * dlnphiidxj[i*ns + j];
        }
        for (int k = 0; k < ns; k++)
        {
            dlnphiidnk[i*ns + k] = nT_inv * (dlnphiidxj[i*ns + k] - sum_xj);
        }
    }
    return dlnphiidnk;
}
