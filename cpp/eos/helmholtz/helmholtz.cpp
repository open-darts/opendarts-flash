#include <iostream>
#include <cmath>
#include <vector>
#include <numeric>

#include "dartsflash/eos/helmholtz/helmholtz.hpp"
#include <Eigen/Dense>

HelmholtzEoS::HelmholtzEoS(CompData& comp_data) : EoS(comp_data)
{
    this->n.resize(ns);
}

EoS::RootSelect HelmholtzEoS::select_root(std::vector<double>::iterator n_it)
{
    for (auto it: this->preferred_roots)
    {
        // it.first: i
        // it.second: {x, root_flag}
        if (*(n_it + it.first) / N >= it.second.first)
        {
            if (this->root == it.second.second && this->is_preferred_root >= RootSelect::ACCEPT)
            {
                // If particular root is the preferred root, return PREFER flag
                return RootSelect::PREFER;
            }
            else
            {
                return RootSelect::REJECT;
            }
        }
    }
    return this->is_stable() ? RootSelect::ACCEPT : RootSelect::REJECT;
}

// Evaluation of EoS at (T, V, n)
void HelmholtzEoS::solve_VT(std::vector<double>::iterator n_it, bool second_order)
{
    // Calculate zero'th order parameters for (V, T, n) specification
    std::copy(n_it, n_it + ns, this->n.begin());
    N = std::accumulate(n.begin(), n.begin() + this->nc, 0.);

    // Set root flag if in preferred root composition range
    this->root_type = EoS::RootFlag::NONE;
    for (auto it: this->preferred_roots)
    {
        if (n[it.first] / N > it.second.first)
        {
            this->set_root_flag(it.second.second);
        }
        else
        {
            this->set_root_flag(EoS::RootFlag::STABLE);
        }
    }

    return this->solve_VT(this->v, second_order);
    // // Calculate first/second order parameters and elements of reduced Helmholtz function F
    // this->zeroth_order(n_it, this->v);
    // (second_order) ? this->second_order(n_it) : this->first_order(n_it);

    // // Calculate pressure
    // this->p = P();
    // return;
}
void HelmholtzEoS::solve_VT(double V_, bool second_order)
{
    // Calculate zero'th/first/second order parameters for (V, T, n) specification
    this->zeroth_order(n.begin(), V_);
    (second_order) ? this->second_order(n.begin()) : this->first_order(n.begin());

    // Calculate pressure
    this->p = P();
    return;
}
void HelmholtzEoS::solve_VT(double V_, double T_, std::vector<double>& n_, int start_idx, bool second_order)
{
    this->init_VT(V_, T_);
    this->solve_VT(n_.begin() + start_idx, second_order);
    return;
}
void HelmholtzEoS::solve_PT(std::vector<double>::iterator n_it, bool second_order)
{
    // Calculate zero'th order parameters for (P, T, n) specification. This includes calculation of V(P, T, n)
    std::copy(n_it, n_it + ns, this->n.begin());
    N = std::accumulate(n.begin(), n.begin() + this->nc, 0.);

    // Set root flag if in preferred root composition range
    this->root_type = EoS::RootFlag::NONE;
    for (auto it: this->preferred_roots)
    {
        if (n[it.first] / N > it.second.first)
        {
            this->set_root_flag(it.second.second);
        }
        else
        {
            this->set_root_flag(EoS::RootFlag::STABLE);
        }
    }

    // Calculate first/second order parameters and elements of reduced Helmholtz function F
    this->zeroth_order(n.begin());
    (second_order) ? this->second_order(n.begin()) : this->first_order(n.begin());
    
    // Calculate V
    this->v = this->V();
    return;
}

// Overloaded pressure P(T, V, n) and volume V(P, T, n) functions
double HelmholtzEoS::P(double V_, double T_, std::vector<double>& n_) 
{
    this->solve_VT(V_, T_, n_, 0, false);
	return this->P();
}
double HelmholtzEoS::V(double p_, double T_, std::vector<double>& n_) 
{
    this->solve_PT(p_, T_, n_, 0, false);
	return this->V();
}
double HelmholtzEoS::Zp(double p_, double T_, std::vector<double>& n_)
{
    this->solve_PT(p_, T_, n_, 0, false);
    return p * v / (N * this->units.R * T);
}
double HelmholtzEoS::Zv(double V_, double T_, std::vector<double>& n_)
{
    this->solve_VT(V_, T_, n_, 0, false);
    return p * v / (N * this->units.R * T);
}
double HelmholtzEoS::rho(double p_, double T_, std::vector<double>& n_)
{
    return this->compdata.get_molar_weight(n_) * 1e-3 / this->V(p_, T_, n_);
}

// Pressure function and derivatives
double HelmholtzEoS::P() 
{
    return -this->units.R * T * this->dF_dV() + N * this->units.R * T / v;
}
double HelmholtzEoS::dP_dV() 
{
    return - this->units.R * T * this->d2F_dV2() - N * this->units.R * T / std::pow(v, 2);
}
double HelmholtzEoS::dP_dT() 
{
    return - this->units.R * T * this->d2F_dTdV() + p / T;
}
double HelmholtzEoS::dP_dni(int i) 
{
    return - this->units.R * T * this->d2F_dVdni(i) + this->units.R * T / v;
}
double HelmholtzEoS::dV_dni(int i) 
{
    return - this->dP_dni(i) / this->dP_dV();
}
double HelmholtzEoS::dV_dT() 
{
    return - this->dP_dT() / this->dP_dV();
}
double HelmholtzEoS::dT_dni(int i) 
{
    return - this->dP_dni(i) / this->dP_dT();
}
double HelmholtzEoS::d2P_dV2()
{
    return - this->units.R * T * this->d3F_dV3() + 2. * N * this->units.R * T / std::pow(v, 3);
}

// Fugacity coefficient and derivatives
double HelmholtzEoS::lnphii(int i) 
{
    return this->dF_dni(i) - std::log(z);
}
double HelmholtzEoS::dlnphii_dnj(int i, int j) 
{
    return this->d2F_dnidnj(i, j) + 1./N + 1./(this->units.R * T) * this->dP_dni(j) * this->dP_dni(i) / this->dP_dV();
}
double HelmholtzEoS::dlnphii_dT(int i) 
{
    return this->d2F_dTdni(i) + 1./T - this->dV_dni(i) / (this->units.R * T) * this->dP_dT();
}
double HelmholtzEoS::dlnphii_dP(int i) 
{
    return this->dV_dni(i) / (this->units.R * T) - 1./p;
}

// Residual bulk properties
double HelmholtzEoS::Ar_VT(double V_, double T_, std::vector<double>& n_, int start_idx) 
{
    // Ar_VT = F * RT
    this->solve_VT(V_, T_, n_, start_idx, false);
    return this->F() * this->units.R * T;
}
double HelmholtzEoS::Sr_VT(double V_, double T_, std::vector<double>& n_, int start_idx) 
{
    // Sr_VT = -R (T dF/dT + F)
    this->solve_VT(V_, T_, n_, start_idx, true);
    return -this->units.R * (T * this->dF_dT() + this->F());
}
double HelmholtzEoS::Ur_VT(double V_, double T_, std::vector<double>& n_, int start_idx) 
{
    // Ur_VT = Ar_VT + T * Sr_VT
    this->solve_VT(V_, T_, n_, start_idx, true);

    // Ar_VT = F * RT
    // Sr_VT = -R (T dF/dT + F)
    double ArTV = this->F() * this->units.R * T;
    double SrTV = -this->units.R * (T * this->dF_dT() + this->F());
    
    return ArTV + T * SrTV;
}
double HelmholtzEoS::Hr_VT(double V_, double T_, std::vector<double>& n_, int start_idx) 
{
    // Hr_VT = Ur_VT + PV - nRT
    double UrTV = this->Ur_VT(V_, T_, n_, start_idx);

    return UrTV + p * v - N * this->units.R * T;
}
double HelmholtzEoS::Gr_VT(double V_, double T_, std::vector<double>& n_, int start_idx) 
{
    // Gr_VT = Ar_VT + PV - nRT
    // Ar_VT = F * RT
    double ArTV = this->Ar_VT(V_, T_, n_, start_idx);

    return ArTV + p * v - N * this->units.R * T;
}
double HelmholtzEoS::Ar_PT(double p_, double T_, std::vector<double>& n_, int start_idx) 
{
    // Ar_PT = Ar_VT - nRT ln Z
    this->solve_PT(p_, T_, n_, start_idx, false);

    // Ar_VT = F * RT
    double ArTV = this->F() * this->units.R * T;

    return ArTV - N * this->units.R * T * std::log(z);
}
double HelmholtzEoS::Sr_PT(double p_, double T_, std::vector<double>& n_, int start_idx) 
{
    // Sr_PT = Sr_VT + nR ln Z
    this->solve_PT(p_, T_, n_, start_idx, true);

    // Sr_VT = -R (T dF/dT + F)
    double SrTV = -this->units.R * (T * this->dF_dT() + this->F());

    return SrTV + N * this->units.R * std::log(z);
}
double HelmholtzEoS::Ur_PT(double p_, double T_, std::vector<double>& n_, int start_idx) 
{
    // Ur_PT = Ur_VT
    this->solve_PT(p_, T_, n_, start_idx, true);

    // Ur_VT = Ar_VT + T * Sr_VT
    // Ar_VT = F * RT
    // Sr_VT = -R (T dF/dT + F)
    double ArTV = this->F() * this->units.R * T;
    double SrTV = -this->units.R * (T * this->dF_dT() + this->F());
    
    return ArTV + T * SrTV;
}
double HelmholtzEoS::Hr_PT(double p_, double T_, std::vector<double>& n_, int start_idx)
{
    // Hr_PT = Hr_VT = Ur_VT + PV - nRT
    // Ur_VT = Ur_PT
    double UrTV = this->Ur_PT(p_, T_, n_, start_idx);

    return UrTV + p * v - N * this->units.R * T;
}
double HelmholtzEoS::Gr_PT(double p_, double T_, std::vector<double>& n_, int start_idx)
{
    // Gr_PT = Gr_VT - nRT ln Z
    this->solve_PT(p_, T_, n_, start_idx, false);

    // Gr_VT = Ar_VT + PV - nRT
    // Ar_VT = F * RT
    double ArTV = this->F() * this->units.R * T;
    double GrTV = ArTV + p * v - N * this->units.R * T;

    return GrTV - N * this->units.R * T * std::log(z);
}

double HelmholtzEoS::Cvr(double p_, double T_, std::vector<double>& n_, int start_idx) 
{
    // Cvr/R = - T^2 d2F/dT2 - 2T dF/dT
    this->solve_PT(p_, T_, n_, start_idx, true);
    return -std::pow(T, 2) * this->d2F_dT2() - 2. * T * this->dF_dT();
}
double HelmholtzEoS::Cv(double p_, double T_, std::vector<double>& n_, int start_idx)
{
    // Total heat capacity at constant volume Cv/R
    double cvr = this->Cvr(p_, T_, n_, start_idx);
    double cvi = 0.;
    for (int i = 0; i < ns; i++)
    {
        cvi += n_[i] * this->cvi(T, i);
    }
    return cvi + cvr;
}
double HelmholtzEoS::Cpr(double p_, double T_, std::vector<double>& n_, int start_idx) 
{
    // Cpr/R = Cvr/R - T/R (dP/dT)^2/ (dP/dV) - n
    double cvr = this->Cvr(p_, T_, n_, start_idx);
    double cpr = cvr - T/this->units.R * std::pow(this->dP_dT(), 2) / this->dP_dV() - N;
    return cpr;
}
double HelmholtzEoS::Cp(double p_, double T_, std::vector<double>& n_, int start_idx)
{
    // Total heat capacity at constant pressure Cp/R
    double cpr = this->Cpr(p_, T_, n_, start_idx);
    double cpi = 0.;
    for (int i = 0; i < ns; i++)
    {
        cpi += n_[i] * this->cpi(T, i);
    }
    return cpi + cpr;
}

double HelmholtzEoS::vs(double p_, double T_, std::vector<double>& n_, int start_idx) 
{
    // Sound speed vs^2 = v / (β Mw)
    // β = -1/v Cvr/Cpr / (dP/dV)
    double Mw = this->compdata.get_molar_weight(n_);
    double cv = this->Cv(p_, T_, n_, start_idx);
    double cp = this->Cp(p_, T_, n_, start_idx);
    double beta = -1./v * cv/cp / this->dP_dV();
    double VS2 = v / (beta * Mw * 1e-3) * M_R / this->units.R;
    return std::sqrt(VS2);
}
double HelmholtzEoS::JT(double p_, double T_, std::vector<double>& n_, int start_idx) 
{
    // Joule-Thomson coefficient μ_JT = -1/Cp (v + T (dP/dT)/(dP/dV))
    double cp = this->Cp(p_, T_, n_, start_idx) * this->units.R;
    return -1. / cp * (v - T * this->dV_dT());
}

// EoS CONSISTENCY TESTS
int HelmholtzEoS::derivatives_test(double p_, double T_, std::vector<double>& n_, double tol)
{
    // Test derivatives of Helmholtz function F(T,V,n) = Ar(T,V,n)/RT
    int error_output = 0;
    double T0 = T_;
    double V0 = this->V(p_, T_, n_);
    std::vector<double> n0 = n_;
    double dX = 1e-5;

    // Analytical derivatives
    this->solve_VT(V0, T0, n0, 0, true);
    double F0 = this->F();
    double dV = this->dF_dV();
    double dT = this->dF_dT();
    double dTdV = this->d2F_dTdV();
    double dV2 = this->d2F_dV2();
    double dT2 = this->d2F_dT2();
    double dV3 = this->d3F_dV3();

    std::vector<double> dni(nc), dTdni(nc), dVdni(nc), dnidnj(nc*nc);
    for (int i = 0; i < nc; i++)
    {
        dni[i] = this->dF_dni(i);
        dTdni[i] = this->d2F_dTdni(i);
        dVdni[i] = this->d2F_dVdni(i);
        for (int j = 0; j < nc; j++)
        {
            dnidnj[i*nc + j] = this->d2F_dnidnj(i, j);
        }
    }

    // Numerical derivatives
    double d;

    // dF/dV
    this->solve_VT(V0 + V0*dX, T0, n_, 0, true);
    double dV_num = (this->F() - F0) / (V0*dX);
    d = std::log(std::fabs(dV + 1e-15)) - std::log(std::fabs(dV_num + 1e-15));
    if (std::fabs(d) > tol) { print("dF/dV != dF/dV", std::vector<double>{dV, dV_num, d}); error_output++; }

    // dF/dT
    this->solve_VT(V0, T0 + T0*dX, n_, 0, true);
    double dT_num = (this->F() - F0) / (T0*dX);
    d = std::log(std::fabs(dT + 1e-15)) - std::log(std::fabs(dT_num + 1e-15));
    if (std::fabs(d) > tol) { print("dF/dT != dF/dT", std::vector<double>{dT, dT_num, d}); error_output++; }

    // dF/dni
    for (int i = 0; i < nc; i++)
    {
        n_ = n0;
        n_[i] += dX*n0[i];
        this->solve_VT(V0, T0, n_, 0, true);
        double dni_num = (this->F() - F0) / (n0[i]*dX);
        d = std::log(std::fabs(dni[i] + 1e-15)) - std::log(std::fabs(dni_num + 1e-15));
        if (std::fabs(d) > tol) { print("i", i); print("dF/dni != dF/dni", std::vector<double>{dni[i], dni_num, d}); error_output++; }

        // d2F/dnidnj
        double dni1, dni_1;
        for (int j = 0; j < nc; j++)
        {
            n_[j] += dX*n0[j];
            this->solve_VT(V0, T0, n_, 0, true);
            dni1 = this->dF_dni(i);
            n_[j] -= dX*n0[j];

            n_[j] -= dX*n0[j];
            this->solve_VT(V0, T0, n_, 0, true);
            dni_1 = this->dF_dni(i);
            n_[j] += dX*n0[j];

            double dnidnj_num = (dni1 - dni_1) / (2*n0[j]*dX);
            d = std::log(std::fabs(dnidnj[i*nc + j] + 1e-15)) - std::log(std::fabs(dnidnj_num + 1e-15));
            if (std::fabs(d) > tol) 
            { 
                print("i, j", std::vector<int>{i, j}); print("d2F/dnidnj != d2F/dnidnj", std::vector<double>{dnidnj[i*nc + j], dnidnj_num, d}); 
                error_output++; 
            }
        }

        // d2F/dTdni
        this->solve_VT(V0, T0 + T0*dX, n_, 0, true);
        dni1 = this->dF_dni(i);
        this->solve_VT(V0, T0 - T0*dX, n_, 0, true);
        dni_1 = this->dF_dni(i);
        double dTdni_num = (dni1 - dni_1) / (2*T0*dX);
        d = std::log(std::fabs(dTdni[i] + 1e-15)) - std::log(std::fabs(dTdni_num + 1e-15));
        if (std::fabs(d) > tol) { print("i", i); print("d2F/dTdni != d2F/dTdni", std::vector<double>{dTdni[i], dTdni_num, d}); error_output++; }

        // d2F/dVdni
        this->solve_VT(V0 + V0*dX, T0, n_, 0, true);
        dni1 = this->dF_dni(i);
        this->solve_VT(V0 - V0*dX, T0, n_, 0, true);
        dni_1 = this->dF_dni(i);
        double dVdni_num = (dni1 - dni_1) / (2*V0*dX);
        d = std::log(std::fabs(dVdni[i] + 1e-15)) - std::log(std::fabs(dVdni_num + 1e-15));
        if (std::fabs(d) > tol) { print("i", i); print("d2F/dVdni != d2F/dVdni", std::vector<double>{dVdni[i], dVdni_num, d}); error_output++; }

        n_[i] -= dX*n0[i];
    }

    // d2F/dVdT and d2F/dTdV
    this->solve_VT(V0, T0 - dX*T0, n_, 0, true);
    double dV_1 = this->dF_dV();
    this->solve_VT(V0, T0 + dX*T0, n_, 0, true);
    double dV1 = this->dF_dV();
    double dVdT_num = (dV1 - dV_1) / (2*dX*T0);
    d = std::log(std::fabs(dTdV + 1e-15)) - std::log(std::fabs(dVdT_num + 1e-15));
    if (std::fabs(d) > tol) { print("d2F/dVdT != d2F/dVdT", std::vector<double>{dTdV, dVdT_num}); error_output++; }

    this->solve_VT(V0 - dX*V0, T0, n_, 0, true);
    double dT_1 = this->dF_dT();
    this->solve_VT(V0 + dX*V0, T0, n_, 0, true);
    double dT1 = this->dF_dT();
    double dTdV_num = (dT1 - dT_1) / (2*dX*V0);
    d = std::log(std::fabs(dTdV + 1e-15)) - std::log(std::fabs(dTdV_num + 1e-15));
    if (std::fabs(d) > tol) { print("d2F/dTdV != d2F/dTdVdT", std::vector<double>{dTdV, dTdV_num}); error_output++; }

    // d2F/dV2
    this->solve_VT(V0 - V0*dX, T0, n_, 0, true);
    dV_1 = this->dF_dV();
    this->solve_VT(V0 + V0*dX, T0, n_, 0, true);
    dV1 = this->dF_dV();
    double dV2_num = (dV1 - dV_1) / (2*dX*V0);
    d = std::log(std::fabs(dV2 + 1e-15)) - std::log(std::fabs(dV2_num + 1e-15));
    if (std::fabs(d) > tol) { print("d2F/dV2 != d2F/dV2", std::vector<double>{dV2, dV2_num, d}); error_output++; }

    // d2F/dT2
    this->solve_VT(V0, T0 - T0*dX, n_, 0, true);
    dT_1 = this->dF_dT();
    this->solve_VT(V0, T0 + T0*dX, n_, 0, true);
    dT1 = this->dF_dT();
    double dT2_num = (dT1 - dT_1) / (2*dX*T0);
    d = std::log(std::fabs(dT2 + 1e-15)) - std::log(std::fabs(dT2_num + 1e-15));
    if (std::fabs(d) > tol) { print("d2F/dT2 != d2F/dT2", std::vector<double>{dT2, dT2_num, d}); error_output++; }

    // d3F/dV3
    this->solve_VT(V0 - V0*dX, T0, n_, 0, true);
    dV_1 = this->d2F_dV2();
    this->solve_VT(V0 + V0*dX, T0, n_, 0, true);
    dV1 = this->d2F_dV2();
    double dV3_num = (dV1 - dV_1) / (2*dX*V0);
    d = std::log(std::fabs(dV3 + 1e-15)) - std::log(std::fabs(dV3_num + 1e-15));
    if (std::fabs(d) > tol) { print("d3F/dV3 != d3F/dV3", std::vector<double>{dV3, dV3_num, d}); error_output++; }

    // dZ/dP and d2Z/dP2
    this->solve_PT(p_, T0, n_, 0, true);
    double dZdP = this->dZ_dP();
    double d2ZdP2 = this->d2Z_dP2();

    this->solve_PT(p_ - p_*dX, T0, n_, 0, true);
    double Z_1 = this->z;
    double dP_1 = this->dZ_dP();
    this->solve_PT(p_ + p_*dX, T0, n_, 0, true);
    double Z1 = this->z;
    double dP1 = this->dZ_dP();
    double dP_num = (Z1 - Z_1) / (2*dX*p_);
    double d2P_num = (dP1 - dP_1) / (2*dX*p_);
    
    d = std::log(std::fabs(dZdP + 1e-15)) - std::log(std::fabs(dP_num + 1e-15));
    if (std::fabs(d) > tol) { print("dZ/dP != dZ/dP", std::vector<double>{dZdP, dP_num, d}); error_output++; }
    d = std::log(std::fabs(d2ZdP2 + 1e-15)) - std::log(std::fabs(d2P_num + 1e-15));
    if (std::fabs(d) > tol) { print("d2Z/dP2 != d2Z/dP2", std::vector<double>{d2ZdP2, d2P_num, d}); error_output++; }

    return error_output;
}

int HelmholtzEoS::lnphi_test(double p_, double T_, std::vector<double>& n_, double tol) 
{
    // d/dnj (sum_i n_i lnphii) = G_rj/RT = lnphij
    int error_output = 0;

    // Calculate d(Gri/RT)/dnj numerically
    this->solve_PT(p_, T_, n_, 0, false);
    std::vector<double> lnphi0 = this->lnphi();

    // at p, T, n
    double Gres0 = 0.;
    for (int i = 0; i < nc; i++) {
        Gres0 += n_[i] * lnphi0[i];
    }

    // add dn
    double dn = 1e-8;
    std::vector<double> dGres(nc, 0.);
    for (int j = 0; j < nc; j++)
    {
        n_[j] += dn;
        this->solve_PT(p_, T_, n_, 0, false);
        std::vector<double> lnphi1 = this->lnphi();

        double Gres1 = 0.;
        for (int i = 0; i < nc; i++)
        {
            Gres1 += n_[i] * lnphi1[i];
        }
        dGres[j] = (Gres1-Gres0)/dn;
        n_[j] -= dn;
    }

    // compare lnphi's
    for (int j = 0; j < nc; j++)
    {
        double d = std::log(std::fabs(dGres[j] + 1e-15)) - std::log(std::fabs(lnphi0[j] + 1e-15));
        if (std::fabs(d) > tol)
        {
            print("comp", j);
            print("lnphi consistency test", std::vector<double>{dGres[j], lnphi0[j], d});
            error_output++;
        }
    }
    return error_output;
}

int HelmholtzEoS::pressure_test(double p_, double T_, std::vector<double>& n_, double tol) 
{
    // Pressure consistency test
    // d/dP sum_i dlnphii/dP = (Z-1)N/P
    int error_output = 0;

    // Calculate dlnphi/dP from EoS
    this->solve_PT(p_, T_, n_, 0, true);

    dlnphidP = this->dlnphi_dP();
    double ndlnphi_dp = std::inner_product(n_.begin(), n_.end(), dlnphidP.begin(), 0.);

    // compare dlnphi_dP with (Z-1)/P
    if (std::fabs(ndlnphi_dp - (this->z-1.)*N/p) > tol)
    {
        print("Pressure consistency test", std::vector<double>{ndlnphi_dp, (this->z-1.)/p});
        error_output++;
    }
    return error_output;
}

int HelmholtzEoS::temperature_test(double p_, double T_, std::vector<double>& n_, double tol) 
{
    (void) p_;
    (void) T_;
    (void) n_;
    (void) tol;
    return 0;
}

int HelmholtzEoS::composition_test(double p_, double T_, std::vector<double>& n_, double tol) 
{
    // Consistency of composition derivatives: symmetry of dlnphii/dnj and Gibbs-Duhem relation
    int error_output = 0;

    // Calculate all dlnphii/dnj
    this->solve_PT(p_, T_, n_, 0, true);
    dlnphidn = this->dlnphi_dn();

    // Symmetry of dlnphii_dnj: dlnphii/dnj == dlnphij/dni
    for (int i = 0; i < nc; i++)
    {
        for (int j = 0; j < nc; j++)
        {
            double d = (this->dlnphidn[i*nc+j]-this->dlnphidn[j*nc+i])/this->dlnphidn[i*nc+j];
            if (std::fabs(d) > tol)
            {
                print("symmetry", std::vector<double>{dlnphidn[i*nc+j], dlnphidn[j*nc+i], d});
                error_output++;
            }
        }
    }

    // Gibbs-Duhem relation
    // sum_i n_i dlnphii/dnj = 0
    for (int j = 0; j < nc; j++)
    {
        double G_D = 0;
        for (int i = 0; i < nc; i++)
        {
            G_D += n_[i] * this->dlnphidn[i*nc+ j];
        }
        if (std::fabs(G_D) > tol)
        {
            print("Gibbs-Duhem", G_D);
            error_output++;
        }
    }
    return error_output;
}

int HelmholtzEoS::pvt_test(double p_, double T_, std::vector<double>& n_, double tol)
{
    // Consistency of EoS PVT: Calculate volume roots at P, T, n and find P at roots (T, V, n)
    int error_output = 0;

    // Evaluate properties at P, T, n
    this->solve_PT(p_, T_, n_, 0, true);
    
    for (std::complex<double> Z: this->Z_roots)
    {
        if (Z.imag() == 0.)
        {
            // Calculate total volume
            double V = Z.real() * N * this->units.R * T / p;

            // Evaluate P(T,V,n)
            double pp = this->P(V, T, n_);
            if (std::fabs(pp - p) > tol)
            {
                print("P(T, V, n) != p", std::vector<double>{pp, p, std::fabs(pp-p)});
                error_output++;
            }
        }
    }

    // Calculate critical point and check if dP/dV = d2P/dV2 = 0
    // std::vector<double> nn(nc, 0.);
    // nn[0] = 1.;
    HelmholtzEoS::CriticalPoint cp = this->critical_point(n_);
    this->solve_PT(cp.Pc, cp.Tc, n_, 0, true);
    double dPdV = this->dP_dV();
    double d2PdV2 = this->d2P_dV2();

    this->solve_VT(cp.Vc-1e-8, cp.Tc, n_, 0, true);
    double d2P_dV2_min = this->d2P_dV2();

    this->solve_VT(cp.Vc+1e-8, cp.Tc, n_, 0, true);
    double d2P_dV2_plus = this->d2P_dV2();

    if (std::fabs(dPdV) > 1e-1 || std::fabs(d2PdV2) > 1e8 || !(d2P_dV2_plus <= 0. && d2P_dV2_min >= 0.))
    {
        print("Criticality conditions not satisfied, dP/dV", dPdV);
        print("d2P/dV2 min, plus", std::vector<double>{d2P_dV2_min, d2P_dV2_plus});
        cp.print_point();
        error_output++;
    }

    return error_output;
}

int HelmholtzEoS::properties_test(double p_, double T_, std::vector<double>& n_, double tol)
{
    // Consistency of EoS residual properties at (P, T, n)
    int error_output = 0;
    double p0 = p_;
    double T0 = T_;
    double V0 = this->V(p0, T0, n_);
    double Z0 = this->z;
    double dP = 1e-5;
    double d;

    // Derivatives of compressibility factor dZ/dP and d2Z/dP2
    double dZdP = this->dZ_dP();
    double d2ZdP2 = this->d2Z_dP2();
    
    this->solve_PT(p0 + p0*dP, T0, n_, 0, true);
    double dZ1 = this->dZ_dP();
    double dZdP_num = (this->z - Z0) / (p0*dP);

    this->solve_PT(p0 - p0*dP, T0, n_, 0, true);
    double dZ_1 = this->dZ_dP();
    double d2ZdP2_num = (dZ1 - dZ_1) / (2*p0*dP);

    d = std::log(std::fabs(dZdP + 1e-15)) - std::log(std::fabs(dZdP_num + 1e-15));
    if (std::fabs(d) > tol) { print("dZ/dP != dZ/dP", std::vector<double>{dZdP, dZdP_num, d}); error_output++; }
    d = std::log(std::fabs(d2ZdP2 + 1e-15)) - std::log(std::fabs(d2ZdP2_num + 1e-15));
    if (std::fabs(d) > tol) { print("d2Z/dP2 != d2Z/dP2", std::vector<double>{d2ZdP2, d2ZdP2_num, d}); error_output++; }

    // Gibbs free energy Gr_PT(P, T, n)
    double Gr = EoS::Gr_PT(p0, T0, n_);
    double Grhh = this->Gr_PT(p0, T0, n_);
    if (std::fabs(Gr - Grhh) > tol) { print("Gr != Gr", std::vector<double>{Gr, Grhh}); error_output++; }

    // Enthalpy Hr_PT(P, T, n)
    double Hr = EoS::Hr_PT(p0, T0, n_);
    double Hrhh = this->Hr_PT(p0, T0, n_);
    if (std::fabs(Hr - Hrhh) > tol) { print("Hr != Hr", std::vector<double>{Hr, Hrhh}); error_output++; }

    // Entropy Sr_PT(P, T, n)
    double Sr = EoS::Sr_PT(p0, T0, n_);
    double Srhh = this->Sr_PT(p0, T0, n_);
    if (std::fabs(Sr - Srhh) > tol) { print("Sr != Sr", std::vector<double>{Sr, Srhh}); error_output++; }

    // Helmholtz free energy Ar_PT(P, T, n)
    // double Ar = EoS::Ar_PT(p0, T0, n_);
    // double Arhh = this->Ar_PT(p0, T0, n_);
    // if (std::fabs(Ar - Arhh) > tol) { print("Ar != Ar", std::vector<double>{Ar, Arhh}); error_output++; }

    // Internal energy Ur_PT(P, T, n)
    // double Ur = EoS::Ur_PT(p0, T0, n_);
    // double Urhh = this->Ur_PT(p0, T0, n_);
    // if (std::fabs(Ur - Urhh) > tol) { print("Ur != Ur", std::vector<double>{Ur, Urhh}); error_output++; }

    // Thermal properties tests
    double dT = 1e-3;

    // Residual heat capacity at constant volume Cvr
    double CVr = this->Cvr(p0, T0, n_, 0) * this->units.R;
    double Ur0 = this->Ur_VT(V0, T0, n_);
    double Ur1 = this->Ur_VT(V0, T0 + dT, n_);
    double CVr_num = (Ur1-Ur0)/dT;
    if (std::fabs(CVr - CVr_num) > tol) { print("Cvr != Cvr", std::vector<double>{CVr, CVr_num}); error_output++; }

    // // Heat capacity at constant volume Cv
    // double CV = this->Cv(p, T, n_, 0) * this->units.R;
    // double U0 = this->U_VT(V0, T0, n_);
    // double U1 = this->U_VT(V0, T0+dT, n_);
    // double CV_num = (U1-U0)/dT;
    // if (std::fabs(CV - CV_num) > tol) { print("Cv != Cv", std::vector<double>{CV, CV_num}); error_output++; }

    // Residual heat capacity at constant pressure Cpr
    double CPr = this->Cpr(p0, T0, n_, 0) * this->units.R;
    double Hr1 = this->Hr_PT(p0, T0+dT, n_);
    double CPr_num = (Hr1-Hrhh)/dT;
    if (std::fabs(CPr - CPr_num) > tol) { print("Cpr != Cpr", std::vector<double>{CPr, CPr_num}); error_output++; }

    // Heat capacity at constant pressure Cp
    double CP = this->Cp(p0, T0, n_, 0);
    double H0 = this->H_PT(p0, T0, n_);
    double H1 = this->H_PT(p0, T0+dT, n_);
    double CP_num = (H1-H0)/dT;
    if (std::fabs(CP - CP_num) > tol) { print("Cp != Cp", std::vector<double>{CP, CP_num}); error_output++; }

    return error_output;
}
