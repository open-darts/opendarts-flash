#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include <numeric>

#include "dartsflash/global/global.hpp"
#include "dartsflash/maths/maths.hpp"
#include "dartsflash/eos/aq/jager.hpp"

namespace jager {
    // Reference conditions
    double R = 8.3145; double T_0 = 298.15; double P_0 = 1.0; double ms_0 = 1.;
    
    // gibbs energy of ideal gas at p0, T0
    std::unordered_map<std::string, double> gi0 = {
        {"H2O", -228700}, {"CO2", -394600}, {"N2", 0}, {"H2S", -33100}, 
        {"C1", -50830}, {"C2", -32900}, {"C3", -23500}, {"iC4", -20900}, {"nC4", -17200}, {"iC5", -15229}, {"nC5", -8370}, 
        {"nC6", -290}, {"nC7", 8120}, {"nC8", 16500}, {"nC9", 24811}, {"nC10", 33220},
        {"Ethene", 68170}, {"Propene", 62760}, {"Benzene", 129700}, {"Toluene", 122100}, {"MeOH", -162600}, {"EOH", -168400}, {"MEG", -304464},
        {"NaCl", -384138.00}, {"CaCl2", -748100}, {"KCl", -409140} 
    };
    // molar enthalpy of ideal gas at p0, T0
    std::unordered_map<std::string, double> hi0 = {
        {"H2O", -242000}, {"CO2", -393800}, {"N2", 0}, {"H2S", -20200}, 
        {"C1", -74900}, {"C2", -84720}, {"C3", -103900}, {"iC4", -134600}, {"nC4", -126200}, {"iC5", -165976}, {"nC5", -146500}, 
        {"nC6", -167300}, {"nC7", -187900}, {"nC8", -208600}, {"nC9", -229028}, {"nC10", -249655},
        {"Ethene", 52320}, {"Propene", 20400}, {"Benzene", 82980}, {"Toluene", 50030}, {"MeOH", -201300}, {"EOH", -235000}, {"MEG", -389314},
        {"NaCl", -411153.00}, {"CaCl2", -795800}, {"KCl", -436747}
    };

    // gibbs energy of pure H2O or 1 molal solution at p0, T0
    std::unordered_map<std::string, double> gi_0 =  {
        {"H2O", -237129}, {"CO2", -385974}, {"N2", 18188}, {"H2S", -27920}, 
        {"C1", -34451}, {"C2", -17000}, {"C3", -7550}, {"iC4", -2900}, {"nC4", -940}, {"iC5", 1451}, {"nC5", 9160}, 
        {"nC6", 18200}, {"nC7", 27500}, {"nC8", 36700}, {"nC9", 45200}, {"nC10", 54140},
        {"Ethene", 81379}, {"Propene", 74935}, {"Benzene", 133888}, {"Toluene", 126608}, {"MeOH", -175937}, {"EOH", -181293}, {"MEG", -325000},
        {"Na+", -261881}, {"Ca2+", -552790}, {"K+", -282462}, {"Cl-", -131039}
    };
    // molar enthalpy of pure H2O or 1 molal solution at p0, T0
    std::unordered_map<std::string, double> hi_0 =  {
        {"H2O", -285830.}, {"CO2", -413798.}, {"N2", -10439.}, {"H2S", -37660.}, 
        {"C1", -87906.}, {"C2", -103136.}, {"C3", -131000.}, {"iC4", -156000.}, {"nC4", -152000.}, {"iC5", -193476.}, {"nC5", -173887.}, 
        {"nC6", -199200.}, {"nC7", -225000.}, {"nC8", -250500.}, {"nC9", -273150.}, {"nC10", -297330.},
        {"Ethene", 35857.}, {"Propene", -1213.}, {"Benzene", 51170.}, {"Toluene", 13724.}, {"MeOH", -246312.}, {"EOH", -287232.}, {"MEG", -320000.},
        {"Na+", -240300.}, {"Ca2+", -543083.}, {"K+", -252170.}, {"Cl-", -167080.} 
    };
	std::vector<double> hi_a = {8.712 * R, 0.125E-2 * R, -0.018E-5 * R};
	// molar volume of pure H2O
	std::vector<std::vector<double>> vi_a = {{31.1251, -2.46176E-2, 8.69425E-6, -6.03348E-10}, // K^0
											 {-1.14154E-1, 2.15663E-4, -7.96939E-8, 5.57791E-12}, // K^-1
											 {3.10034E-4, -6.48160E-7, 2.45391E-10, -1.72577E-14}, // K^-2
											 {-2.48318E-7, 6.47521E-10, -2.51773E-13, 1.77978E-17}}; // K^-3
    
    // Born constants of solutes [eq. 3.6-3.7]
    std::unordered_map<std::string, double> omega = {
        {"CO2", -8368.}, {"N2", -145101.}, {"H2S", -41840.}, 
        {"C1", -133009.}, {"C2", -169870.}, {"C3", -211418.}, {"iC4", -253592.}, {"nC4", -253592.}, {"iC5", -301000.}, {"nC5", -300955.}, 
        {"nC6", -335180.}, {"nC7", -380158.}, {"nC8", -404258.}, {"nC9", -453600.}, {"nC10", -494000.},
        {"Ethene", -167360.}, {"Propene", -232547.}, {"Benzene", -82676.}, {"Toluene", -135896.}, {"MeOH", -61756.}, {"EOH", -85228.}, {"MEG", 406762.},
        {"Na+", 138323.}, {"Ca2+", 517142}, {"K+", 80626}, {"Cl-", 609190.} 
    };
    // Partial molar heat capacity terms [eq. 3.6]
    std::unordered_map<std::string, double> cp1 = {
        {"CO2", 167.50}, {"N2", 149.75}, {"H2S", 135.14}, 
        {"C1", 176.12}, {"C2", 226.67}, {"C3", 277.52}, {"iC4", 330.77}, {"nC4", 330.77}, {"iC5", 373.00}, {"nC5", 373.24}, 
        {"nC6", 424.53}, {"nC7", 472.37}, {"nC8", 522.13}, {"nC9", 571.90}, {"nC10", 621.10},
        {"Ethene", 163.59}, {"Propene", 209.62}, {"Benzene", 338.33}, {"Toluene", 392.98}, {"MeOH", 165.21}, {"EOH", 251.11}, {"MEG", -2.55},
        {"Na+", 76.065}, {"Ca2+", 37.656}, {"K+", 30.962}, {"Cl-", -18.410} 
    };
    std::unordered_map<std::string, double> cp2 = {
        {"CO2", 5304066.}, {"N2", 5046230.}, {"H2S", 2850801.}, 
        {"C1", 6310762.}, {"C2", 9011737.}, {"C3", 11749531.}, {"iC4", 14610096.}, {"nC4", 14610096.}, {"iC5", 16997948}, {"nC5", 16955051.}, 
        {"nC6", 19680558.}, {"nC7", 22283347.}, {"nC8", 24886075.}, {"nC9", 27607264.}, {"nC10", 30256344.},
        {"Ethene", 5846257.}, {"Propene", 8447000.}, {"Benzene", 1072758.}, {"Toluene", 1745012}, {"MeOH", -903211.}, {"EOH", 90828.}, {"MEG", 5711758.},
        {"Na+", -593488.}, {"Ca2+", -1520020}, {"K+", -1079442}, {"Cl-", -1176604.}
    };
    // Partial molar volume terms [eq. 3.7]
    std::unordered_map<std::string, std::vector<double>> vp = {
        {"CO2", {2.614, 3125.9, 11.7721, -129198.}},
        {"N2", {2.596, 3083.0, 11.9407, -129018.}},
        {"H2S", {2.724, 2833.6, 24.9559, -127989.}},
        {"C1", {2.829, 3651.8, 9.7119, -131365.}},
        {"C2", {3.612, 5565.2, 2.1778, -139277.}},
        {"C3", {4.503, 7738.2, -6.3316, -148260.}},
        {"iC4", {5.500, 11014.4, -14.9298, -157256.}}, 
        {"nC4", {5.500, 11014.4, -14.9298, -157256.}},
        {"iC5", {6.300, 12100.0, -23.4000, -166000.}}, 
        {"nC5", {6.282, 12082.2, -23.4091, -166218.}},
        {"nC6", {7.175, 14264.4, -32.0202, -175238.}},
        {"nC7", {8.064, 16435.2, -40.5342, -184213.}},
        {"nC8", {8.961, 18624.1, -49.1356, -193263.}}, 
        {"nC9", {9.830, 20730.0, -57.4000, -201950.}}, 
        {"nC10", {10.710, 22890.0, -65.9000, -210850.}},
        {"Ethene", {3.287, 5288.2, -7.8396, -138131.}}, 
        {"Propene", {4.170, 6925.3, -3.1648, -144900.}},
        {"Benzene", {5.491, 7579.0, 49.4653, -147599.}}, 
        {"Toluene", {6.287, 9169.4, 50.7724, -154176.}},
        {"MeOH", {2.903, 2307.3, 47.7051, -125.809}},
        {"EOH", {3.863, 4166.5, 50.8126, -133495.}},
        {"MEG", {4.000, 0., 0., 0.}},
        {"Na+", {0.7694, -956.04, 13.6231, -114056.}},
        {"Ca2+", {-0.0815, -3034.24, 22.1610, -103721.}}, 
        {"K+", {1.4891, -616.30, 22.7400, -113470.}}, 
        {"Cl-", {1.6870, 2008.74, 23.2756, -119118.}}
    };
    // Dielectric constant of water coefficients [eq. 3.9]
    std::vector<double> eps = {243.9576, 0.039037, -1.01261E-5, // n=0
                            -0.7520846, -2.12309E-4, 6.04961E-8,  // n=1
                            6.60648E-4, 3.18021E-7, -9.33341E-11}; // n=2

    // Solute interaction parameters eq. 3.18-3.20
    std::unordered_map<std::string, std::unordered_map<std::string, std::vector<double>>> Bca = {
        {"Na+", {{"Cl-", {-0.554860699, 4.2795E-3, -6.529E-6}}}},
        {"K+", {{"Cl-", {0.178544751, -9.55043E-4, 1.8208E-6}}}},
        {"Ca2+", {{"Cl-", {0.549244833, -1.870735E-3, 3.3604E-6}}}},
		{"Cl-", {{"Na+", {-0.554860699, 4.2795E-3, -6.529E-6}},
				{"K+", {0.178544751, -9.55043E-4, 1.8208E-6}},
				{"Ca2+", {0.549244833, -1.870735E-3, 3.3604E-6}}}}
    };
    std::unordered_map<std::string, std::unordered_map<std::string, std::vector<double>>> Cca = {
        {"Na+", {{"Cl-", {-0.016131327, -1.25089E-5, 5.89E-8}}}},
        {"K+", {{"Cl-", {-5.546927E-3, 4.22294E-5, -9.038E-8}}}},
        {"Ca2+", {{"Cl-", {-0.011031685, 7.49491E-5, -1.639E-7}}}},
		{"Cl-", {{"Na+", {-0.016131327, -1.25089E-5, 5.89E-8}},
				{"K+", {-5.546927E-3, 4.22294E-5, -9.038E-8}},
				{"Ca2+", {-0.011031685, 7.49491E-5, -1.639E-7}}}}
    };
    std::unordered_map<std::string, std::unordered_map<std::string, std::vector<double>>> Dca = {
        {"Na+", {{"Cl-", {-1.12161E-3, 2.49474E-5, -4.603E-8}}}},
        {"K+", {{"Cl-", {7.12650E-5, -6.04659E-7, 1.327E-9}}}},
        {"Ca2+", {{"Cl-", {1.08383E-4, -1.03524E-6, 2.3878E-9}}}},
		{"Cl-", {{"Na+", {-1.12161E-3, 2.49474E-5, -4.603E-8}},
				{"K+", {7.12650E-5, -6.04659E-7, 1.327E-9}},
				{"Ca2+", {1.08383E-4, -1.03524E-6, 2.3878E-9}}}}
    };
    std::unordered_map<std::string, std::unordered_map<std::string, std::vector<double>>> B = {
        {"H2S", {{"H2S", {0.1, -4.7e-4, 0., 0.}}}},
        {"CO2", {{"CO2", {0.107, -4.5e-4, 0., 0.}}}},
        {"C1", {{"Na+", {0.025, 0., 0., -5e-3}}, {"Cl-", {0.025, 0., 0., -5e-3}}}},
        {"C3", {{"Na+", {-0.09809, 4.19e-4, -6.2e-6, 0.}}, {"Cl-", {-0.09809, 4.19e-4, -6.2e-6, 0.}}}},
    };

    double e = 1.60218E-19; double eps0 = 8.85419E-12;
	double Theta = 228.; double Psi = 2600.;

    double Integral::simpson(double x0, double x1, int steps) {
        // integrals solved numerically with simpson's rule
        double s = 0.;
        double h = (x1-x0)/steps;  // interval
    
	    // hi
        for (int i = 0; i < steps; i++) 
        {
            double hix = this->f(x0 + i*h);
            double hixh2 = this->f(x0 + (i+0.5)*h);
            double hixh = this->f(x0 + (i+1)*h);
            s += h*((hix + 4*hixh2 + hixh) / 6);
        }
	    return s;
    }

    IG::IG(std::string component_) : Integral(component_)
    {
        this->gi0 = jager::gi0[component_];
        this->hi0 = jager::hi0[component_];
        this->cpi = comp_data::cpi[component_];
    }
    double IG::H(double T)
    {
        // Integral of H(T)/RT^2 dT from T_0 to T
        return (-(this->hi0 / M_R
                - this->cpi[0] * jager::T_0 
                - 1. / 2 * this->cpi[1] * std::pow(jager::T_0, 2) 
                - 1. / 3 * this->cpi[2] * std::pow(jager::T_0, 3)
                - 1. / 4 * this->cpi[3] * std::pow(jager::T_0, 4)) * (1./T - 1./jager::T_0)
                + (this->cpi[0] * (std::log(T) - std::log(jager::T_0))
                + 1. / 2 * this->cpi[1] * (T - jager::T_0)
                + 1. / 6 * this->cpi[2] * (std::pow(T, 2) - std::pow(jager::T_0, 2))
                + 1. / 12 * this->cpi[3] * (std::pow(T, 3) - std::pow(jager::T_0, 3))));
    }
    double IG::dHdT(double T)
    {
        // Derivative of integral w.r.t. temperature
        return (this->hi0 / M_R + 
                this->cpi[0] * (T-jager::T_0) 
                + 1. / 2 * this->cpi[1] * (std::pow(T, 2)-std::pow(jager::T_0, 2)) 
                + 1. / 3 * this->cpi[2] * (std::pow(T, 3)-std::pow(jager::T_0, 3))
                + 1. / 4 * this->cpi[3] * (std::pow(T, 4)-std::pow(jager::T_0, 4))) / std::pow(T, 2);
    }

	double H::f(double T) {
		// f(x) = h(T)/RT^2
		if (component == "H2O")  // pure water enthalpy
    	{
        	return (hi_0["H2O"] + hi_a[0] * (T - T_0) 
            		+ 1. / 2 * hi_a[1] * (std::pow(T, 2) - std::pow(T_0, 2)) 
					+ 1. / 3 * hi_a[2] * (std::pow(T, 3) - std::pow(T_0, 3))) / (R*pow(T, 2));
    	}
	    else  // solute molar enthalpy
	    {
			double c1 = cp1[component];
			double c2 = cp2[component];
			double om = omega[component];
			double a = 1.1048e-4;
			double b = -6.881e-7;

			return (hi_0[component] + c1*(T - T_0) - c2*(1./T - 1./T_0) 
					+ om * (a * (T - T_0) + 0.5 * b * (pow(T, 2) - pow(T_0, 2))))
					/ (R * pow(T, 2));
	    }
	}
	double H::F(double T) {
		// Integral of h(T)/RT^2 dT from T_0 to T
		if (component == "H2O")
		{
			return (-(hi_0["H2O"] - hi_a[0]*T_0 - 1. / 2 * hi_a[1] * pow(T_0, 2) - 1. / 3 * hi_a[2] * pow(T_0, 3)) * (1./T - 1./T_0) 
					+ hi_a[0] * (std::log(T) - std::log(T_0)) 
					+ 1. / 2 * hi_a[1] * (T - T_0) 
					+ 1. / 6 * hi_a[2] * (pow(T, 2) - pow(T_0, 2))) / R;
		}
		else
		{
			double c1 = cp1[component];
			double c2 = cp2[component];
			double om = omega[component];
			double a = 1.1048e-4;
			double b = -6.881e-7;

			return (-(hi_0[component] - c1*T_0 + c2/T_0 - om*(a*T_0 + 0.5 * b * pow(T_0, 2))) * (1./T - 1./T_0)
					+ c1 * (std::log(T) - std::log(T_0))
					+ 0.5 * c2 * (std::pow(T, -2) - std::pow(T_0, -2))
					+ om * (a * (std::log(T) - std::log(T_0)) + 0.5 * b * (T - T_0))) / R;
		}
	}

    double H::dFdT(double T) {
        return this->f(T);
    }

	double V::f(double p) {
		if (component == "H2O") 
    	{
			double vw = (vi_a[0][0] + vi_a[0][1] * p + vi_a[0][2] * std::pow(p, 2) + vi_a[0][3] * std::pow(p, 3))
						+ (vi_a[1][0] + vi_a[1][1] * p + vi_a[1][2] * std::pow(p, 2) + vi_a[1][3] * std::pow(p, 3)) * TT
						+ (vi_a[2][0] + vi_a[2][1] * p + vi_a[2][2] * std::pow(p, 2) + vi_a[2][3] * std::pow(p, 3)) * std::pow(TT, 2)
						+ (vi_a[3][0] + vi_a[3][1] * p + vi_a[3][2] * std::pow(p, 2) + vi_a[3][3] * std::pow(p, 3)) * std::pow(TT, 3);
			return vw * 1e-6 / (R*1e-5*TT);  // m3/mol / (m3.bar/K.mol). K
	    }
	    else 
    	{
			double tau = (5. / 6 * TT - Theta) / (1 + std::exp((TT-273.15) / 5));

			PX px;
    	    return (vp[component][0] 
					+ vp[component][1] / (Psi + p) 
					+ (vp[component][2] + vp[component][3] / (Psi + p)) / (TT - Theta - tau) 
					+ px.f(p, TT)) / (R*TT);  // J/mol.bar / (J/mol.K).K
    	}
	}

    double V::f(double p, double T)
    {
        this->TT = T;
        return this->f(p);
    }

	double V::F(double p, double T) {
		// Integral of v(p)/RT dp from P_0 to P
		if (component == "H2O")
		{
			return ((vi_a[0][0]/T + vi_a[1][0] + vi_a[2][0]*T + vi_a[3][0]*std::pow(T, 2)) * (p - P_0) 
					+ 1. / 2 * (vi_a[0][1]/T + vi_a[1][1] + vi_a[2][1]*T + vi_a[3][1]*std::pow(T, 2)) * (std::pow(p, 2) - std::pow(P_0, 2)) 
					+ 1. / 3 * (vi_a[0][2]/T + vi_a[1][2] + vi_a[2][2]*T + vi_a[3][2]*std::pow(T, 2)) * (std::pow(p, 3) - std::pow(P_0, 3)) 
					+ 1. / 4 * (vi_a[0][3]/T + vi_a[1][3] + vi_a[2][3]*T + vi_a[3][3]*std::pow(T, 2)) * (std::pow(p, 4) - std::pow(P_0, 4))) / (R*1e-5) * 1e-6;
		}
		else
		{
			this->TT = T;
			return this->simpson(P_0, p, 100);
		}
	}

	double V::dFdP(double p, double T) {
        return this->f(p, T);
    }

    double V::dFdT(double p, double T) {
		if (component == "H2O")
		{
			return ((-vi_a[0][0]/pow(T, 2) + vi_a[2][0] + 2*vi_a[3][0]*T) * (p - P_0) 
					+ 1. / 2 * (-vi_a[0][1]/pow(T, 2) + vi_a[2][1] + 2*vi_a[3][1]*T) * (std::pow(p, 2) - std::pow(P_0, 2)) 
					+ 1. / 3 * (-vi_a[0][2]/pow(T, 2) + vi_a[2][2] + 2*vi_a[3][2]*T) * (std::pow(p, 3) - std::pow(P_0, 3)) 
					+ 1. / 4 * (-vi_a[0][3]/pow(T, 2) + vi_a[2][3] + 2*vi_a[3][3]*T) * (std::pow(p, 4) - std::pow(P_0, 4))) / (R*1e-5) * 1e-6;
		}
		else
		{
			double dT = 1e-5;
			double F1 = this->F(p, T+dT);
			double F0 = this->F(p, T);
			return (F1-F0)/dT;

			// double tau = (5. / 6 * T - Theta) / (1 + std::exp((T-273.15) / 5));
			// double dtau_dT = ((1 + std::exp((T-273.15)/5.)) * 5. / 6 - (5. / 6 * T - Theta) / 5 * std::exp((T-273.15)/5.)) / pow(1 + std::exp((T-273.15)/5.), 2);

			// // PX *px{};
			// return (- vp[component][0] / pow(T, 2) * (p - P_0) 
			// 		- vp[component][1] / pow(T, 2) * (std::log(Psi + p) - std::log(Psi + P_0))
			// 		- (vp[component][2] * (p - P_0) + vp[component][3] * (std::log(Psi + p) - std::log(Psi + P_0))) 
			// 		/ pow(T*(T - Theta - tau), 2) * (T - Theta - tau + T * (1-dtau_dT))) * R_inv
			// 		// + px->F(p, T) * R_inv / T
			// 		;
		}
    }

    double PX::f(double p)
    {
        // Term in vi(p)
    	std::vector<double> b(3);
    	for (int i = 0; i < 3; i++)
        {   
    	    b[i] = eps[0 + i] + eps[3 + i] * TT + eps[6 + i] * std::pow(TT, 2);
    	}
		double epsilon = b[0] + b[1] * p + b[2] * std::pow(p, 2);
	    double dedp = b[1] + 2*p*b[2];
		return dedp / std::pow(epsilon, 2);
    }

	double PX::f(double p, double T) 
    {
		this->TT = T;
        return this->f(p);
	}

	double PX::F(double p, double T) {
		// Integral of term in vi(p)
		this->TT = T;
		return simpson(P_0, p, 20);
	}
}

Jager2003::Jager2003(CompData& comp_data) : AQBase(comp_data) {
    gi0.resize(ns);
    gi.resize(ns);
	hi.resize(ns);
	vi.resize(ns);
    lna.resize(ns);
    dlnadxj.resize(ns*ns);
}

void Jager2003::init_PT(double p_, double T_, AQEoS::CompType comp_type) {
    this->p = p_;
    this->T = T_;
    (void) comp_type;

    // H2O and molecular solutes
    for (int i = 0; i < nc; i++)
    {
        // ideal gas Gibbs energy
        jager::IG ig = jager::IG(species[i]);
        double gio = ig.G();
        double hio = ig.H(T);  // eq. 3.3
        gi0[i] = gio - hio;  // eq. 3.2

        // Gibbs energy of aqueous phase
        jager::H h = jager::H(species[i]);
        jager::V v = jager::V(species[i]);
        gi[i] = jager::gi_0[species[i]]/(jager::R*jager::T_0);
        hi[i] = h.F(T);
        vi[i] = v.F(p, T); // - jager::omega[comp] * PX;
    }
    // Ionic solutes
    for (int i = 0; i < ni; i++)
    {
        // ideal gas Gibbs energy of salt
        jager::IG ig = jager::IG(this->compdata.salt);
        double gio = ig.G();
        double hio = ig.H(T);  // eq. 3.3
        gi0[nc + i] = gio - hio;  // eq. 3.2

        // Gibbs energy of ions in aqueous phase
        jager::H h = jager::H(species[i+nc]);
        jager::V v = jager::V(species[i+nc]);
        gi[nc + i] = jager::gi_0[species[i+nc]]/(jager::R*jager::T_0);
        hi[nc + i] = h.F(T);
        vi[nc + i] = v.F(p, T); // - jager::omega[comp] * PX;
    }

    // Initialize vectors
    B0 = std::vector<double>(ns*ns, 0.);
    dB0dP = std::vector<double>(ns*ns, 0.);
    dB0dT = std::vector<double>(ns*ns, 0.);
    B1 = std::vector<double>(ns*ns, 0.);
    for (int i = 0; i < ns; i++)
    {
        if (jager::B.find(species[i]) != jager::B.end())
        {
            std::unordered_map<std::string, std::vector<double>> Bi = jager::B[species[i]];
            for (int j = 0; j < ns; j++)
            {
                if (Bi.find(species[j]) != Bi.end())
                {
                    std::vector<double> b = Bi[species[j]];
                    B0[i*ns + j] = b[0] + b[1] * T + b[2] * p;  // B0
                    B0[j*ns + i] = b[0] + b[1] * T + b[2] * p;  // B0
                    dB0dT[i*ns + j] = b[1];
                    dB0dT[j*ns + i] = b[1];
                    dB0dP[i*ns + j] = b[2];
                    dB0dP[j*ns + i] = b[2];
                    B1[i*ns + j] = b[3];
                    B1[j*ns + i] = b[3];
                }
            }
        }
    }

    // salt related numbers
    B_ca = std::vector<double>(ni*ni, 0.);
    dBcadT = std::vector<double>(ni*ni, 0.);
    C_ca = std::vector<double>(ni*ni, 0.);
    dCcadT = std::vector<double>(ni*ni, 0.);
    D_ca = std::vector<double>(ni*ni, 0.);
    dDcadT = std::vector<double>(ni*ni, 0.);
    for (int j = 0; j < ni; j++)
    {
        int cj = this->charge[j];
        if (cj > 0)  // only cations (+ charge)
        {
            for (int k = 0; k < ni; k++)
            {
                int ck = this->charge[k];
                if (ck < 0)  // only anions (- charge)
                {
                    std::vector<double> B_jk = jager::Bca[species[j+nc]][species[k+nc]];
                    B_ca[j*ni + k] = B_jk[0] + B_jk[1] * T + B_jk[2] * std::pow(T, 2);
                    dBcadT[j*ni + k] = B_jk[1] + 2 * B_jk[2] * T;
                    std::vector<double> C_jk = jager::Cca[species[j+nc]][species[k+nc]];
                    C_ca[j*ni + k] = C_jk[0] + C_jk[1] * T + C_jk[2] * std::pow(T, 2);
                    dCcadT[j*ni + k] = C_jk[1] + 2 * C_jk[2] * T;
                    std::vector<double> D_jk = jager::Dca[species[j+nc]][species[k+nc]];
                    D_ca[j*ni + k] = D_jk[0] + D_jk[1] * T + D_jk[2] * std::pow(T, 2);
                    dDcadT[j*ni + k] = D_jk[1] + 2 * D_jk[2] * T;
                }
            }
        }
    }

    // Debye Huckel parameters
    eps = (jager::eps[0] + jager::eps[1] * p + jager::eps[2] * std::pow(p, 2))
        + (jager::eps[3] + jager::eps[4] * p + jager::eps[5] * std::pow(p, 2)) * T
        + (jager::eps[6] + jager::eps[7] * p + jager::eps[8] * std::pow(p, 2)) * std::pow(T, 2);
    depsdP = (jager::eps[1] + 2 * jager::eps[2] * p)
            + (jager::eps[4] + 2 * jager::eps[5] * p) * T
            + (jager::eps[7] + 2 * jager::eps[8] * p) * pow(T, 2);
    depsdT = jager::eps[3] + jager::eps[4] * p + jager::eps[5] * std::pow(p, 2)
            + 2 * (jager::eps[6] + jager::eps[7] * p + jager::eps[8] * std::pow(p, 2)) * T;
    
    double rho_s = 1000.;
    A_DH = std::pow(std::pow(jager::e, 2)/(jager::eps0*eps*jager::R*T), 1.5) * std::pow(M_NA, 2)/(8*M_PI) * std::sqrt(2*rho_s); // kg^0.5/mol^0.5
    
    dA_DHdP = -1.5 * std::pow(1./eps, 2.5) * depsdP * std::pow(std::pow(jager::e, 2)/(jager::eps0 * jager::R * T), 1.5) 
            * std::pow(M_NA, 2)/(8*M_PI) * std::sqrt(2*rho_s);
    dA_DHdT = (-1.5 * std::pow(1./eps, 2.5) * depsdT * std::pow(std::pow(jager::e, 2)/(jager::eps0 * jager::R * T), 1.5)
            - 1.5 * std::pow(1./T, 2.5) * std::pow(std::pow(jager::e, 2)/(jager::eps0 * jager::R * eps), 1.5))
            * std::pow(M_NA, 2)/(8*M_PI) * std::sqrt(2*rho_s);

	return;
}

void Jager2003::solve_PT(std::vector<double>& x_, bool second_order, AQEoS::CompType comp_type) {
    // Calculate molality of molecular and ionic species
    this->x = x_;  // Water mole fraction to approximate activity

    // Ionic strength: I and dI/dxj (eq. 17)
    I = 0.;
    if (ni > 0)
    {
        dIdxj = std::vector<double>(ns, 0.);
        for (int i = 0; i < ni; i++)
        {
            I += 0.5 * std::pow(this->charge[i], 2) * m_s[nc + i];
            dIdxj[nc + i] += 0.5 * std::pow(this->charge[i], 2) * this->dmi_dxi();
            dIdxj[water_index] += 0.5 * std::pow(this->charge[i], 2) * this->dmi_dxw(nc+i);
        }
    }

    // Calculate activities and derivatives
	if (comp_type == AQEoS::CompType::water)
	{
        this->lnaw(second_order);
	}
	else if (comp_type == AQEoS::CompType::solute)
	{
        this->lnam(second_order);
	}
	else if (comp_type == AQEoS::CompType::ion)
	{
        this->lnai(second_order);
	}
	else
	{
		print("Invalid CompType for Jager2003 correlation specified", comp_type);
		exit(1);
	}

    return;
}

void Jager2003::lnaw(bool second_order)
{
    // For lna_w (eq. 24)
    lna[water_index] = 0.;
    for (int j = 0; j < ns; j++)
    {
        dlnadxj[water_index*ns + j] = 0.;
    }
    double sqrtI = std::sqrt(I);

    // /*
    // 1st term: ionic contribution j_Ica
    if (ni > 0 && I > 0.)
    {
        // eq. 26
        double j_DH = -2.*A_DH * (1. + sqrtI - 2. * std::log(1. + sqrtI) - 1./(1.+sqrtI));
        double djDHdI = -A_DH / sqrtI * (1. - 2./(1.+sqrtI) + 1. / std::pow(1.+sqrtI, 2));
        // double j_DH = 2*A_DH * ((1.-std::pow(1.+sqrtI, 2))/(1.+sqrtI) + 2*std::log(1.+sqrtI));
        // double djDHdI = -2*A_DH * (1 + sqrtI - 1./(I + sqrtI) + 0.5 / (std::pow(1+sqrtI, 2) * sqrtI));

        double sum_mc{ 0. }, sum_ma{ 0. };
        for (int i = 0; i < ni; i++)
        {
            // sum_mc & sum_ma
            if (this->charge[i] > 0)
            {
                sum_mc += std::pow(this->charge[i], 2) * m_s[i+nc];
            }
            else
            {
                sum_ma += std::pow(this->charge[i], 2) * m_s[i+nc];
            }
        }

        for (int i = 0; i < ni; i++)
        {
            // sum_mcma_jIca
            int ci = this->charge[i];
            if (ci > 0)  // i are only cations (+ charge)
            {
                for (int j = 0; j < ni; j++)
                {
                    int cj = this->charge[j];
                    if (cj < 0)  // j only anions (- charge)
                    {
                        // eq. 27
                        double cc_ca = -ci*cj;  // |charge_j * charge_k|
                        double cc_ca_inv = 1./cc_ca;
                        
                        double a = (0.13816 + 0.6*B_ca[i*ni + j])*cc_ca/1.5;
                        double jBca1 = (1.+3.*I*cc_ca_inv)/std::pow(1.+1.5*I*cc_ca_inv, 2);
                        double jBca2 = std::log(1.+1.5*I*cc_ca_inv)/(1.5*I*cc_ca_inv);
                        double jBca3 = 0.5*B_ca[i*ni + j]*std::pow(I, 2) + 2./3.*C_ca[i*ni + j]*std::pow(I, 3) + 0.75*D_ca[i*ni + j]*std::pow(I, 4);
                        double j_Bca = 2 * a * I * (jBca1 - jBca2) + 2.*cc_ca_inv * jBca3;

                        // eq. 25
                        double j_Ica = -comp_data::Mw["H2O"]*1e-3 * (2.*I*cc_ca_inv + j_DH + j_Bca);
                        lna[water_index] += m_s[nc + i] * m_s[nc + j] * std::pow(cc_ca, 2) * j_Ica / (sum_mc * sum_ma);

                        if (second_order)
                        {
                            // eq. 27
                            double djBca1 = 3.*cc_ca_inv / std::pow(1.+1.5*I*cc_ca_inv, 2) - 2.*(1.+3.*I*cc_ca_inv)/std::pow(1.+1.5*I*cc_ca_inv, 3) * 1.5*cc_ca_inv;
                            double djBca2 = 1. / (1. + 1.5*I*cc_ca_inv) / I - std::log(1.+1.5*I*cc_ca_inv)/std::pow(1.5*I*cc_ca_inv, 2) * 1.5*cc_ca_inv;
                            double djBca3 = B_ca[i*ni + j]*I + 2.*C_ca[i*ni + j]*std::pow(I, 2) + 3.*D_ca[i*ni + j]*std::pow(I, 3);                            
                            double djBcadI = 2 * a * (jBca1 - jBca2) + 2*a*I*(djBca1 - djBca2) + 2.*cc_ca_inv * djBca3;
                            double djIcadI = -comp_data::Mw["H2O"]*1e-3 * (2.*cc_ca_inv + djDHdI + djBcadI);

                            // for i == + and j == -
                            double d_denom_dxi = this->dmi_dxi() * std::pow(this->charge[i], 2) * sum_ma;
                            dlnadxj[water_index*ns + nc + i] += (this->dmi_dxi() * m_s[nc + j] * j_Ica / (sum_ma*sum_mc)
                                                                + m_s[nc + i] * m_s[nc + j] * djIcadI * dIdxj[i+nc] / (sum_ma*sum_mc)
                                                                - m_s[nc + i] * m_s[nc + j] * j_Ica / std::pow(sum_ma*sum_mc, 2) * d_denom_dxi) * std::pow(cc_ca, 2);
                            double d_denom_dxj = this->dmi_dxi() * std::pow(this->charge[j], 2) * sum_mc;
                            dlnadxj[water_index*ns + nc + j] += (m_s[nc + i] * this->dmi_dxi() * j_Ica / (sum_ma*sum_mc)
                                                                + m_s[nc + i] * m_s[nc + j] * djIcadI * dIdxj[j+nc] / (sum_ma*sum_mc)
                                                                - m_s[nc + i] * m_s[nc + j] * j_Ica / std::pow(sum_ma*sum_mc, 2) * d_denom_dxj) * std::pow(cc_ca, 2);

                            // for j == water
                            // THIS MIGHT NOT BE CORRECT FOR MULTIPLE IONS
                            double d_denom_dxw = std::pow(this->charge[i], 2) * dmi_dxw(i+nc) * sum_ma + std::pow(this->charge[j], 2) * dmi_dxw(j+nc) * sum_mc;
                            dlnadxj[water_index*ns + water_index] += (this->dmi_dxw(nc+i) * m_s[nc + j] * j_Ica / (sum_ma*sum_mc)
                                                                    + m_s[nc + i] * this->dmi_dxw(nc+j) * j_Ica / (sum_ma*sum_mc)
                                                                    + m_s[nc + i] * m_s[nc + j] * djIcadI * dIdxj[water_index] / (sum_ma*sum_mc)
                                                                    - m_s[nc + i] * m_s[nc + j] * j_Ica / std::pow(sum_ma*sum_mc, 2) * d_denom_dxw) * std::pow(cc_ca, 2);
                        }
                    }
                }
            }
        }
    }

    // 2nd term: ionic-molecular contribution
    double e = std::exp(-2. * sqrtI);
    double dedI = -e / sqrtI;
    for (int i = 0; i < ns; i++)
    {
        if (i != water_index)
        {
            // Pitzer
            for (int j = 0; j < ns; j++)
            {
                if (j != water_index)
                {
                    double B0B1 = B0[i*ns + j] + B1[i*ns + j] * e;  // B0 + B1 exp(-2 sqrt(I))
                    lna[water_index] -= m_s[i] * m_s[j] * B0B1 * comp_data::Mw["H2O"]*1e-3;

                    if (second_order)
                    {
                        dlnadxj[water_index*ns + i] -= this->dmi_dxi() * m_s[j] * B0B1 * comp_data::Mw["H2O"]*1e-3;
                        dlnadxj[water_index*ns + j] -= this->dmi_dxi() * m_s[i] * B0B1 * comp_data::Mw["H2O"]*1e-3;
                        dlnadxj[water_index*ns + water_index] -= -2 * m_s[i] * m_s[j] / x[water_index] * B0B1 * comp_data::Mw["H2O"]*1e-3;
                        if (ni > 0 && I > 0.)
                        {
                            double mmB1dedI = m_s[i] * m_s[j] * B1[i*ns + j] * dedI * comp_data::Mw["H2O"]*1e-3;
                            dlnadxj[water_index*ns + i] -= mmB1dedI * dIdxj[i];
                            dlnadxj[water_index*ns + j] -= mmB1dedI * dIdxj[j];
                            dlnadxj[water_index*ns + water_index] -= mmB1dedI * dIdxj[water_index];
                        }
                    }
                }
            }
        }
    }

    // 3rd term: molecular contribution
    for (int i = 0; i < nc; i++)
    {
        if (i != water_index)
        {
            lna[water_index] -= m_s[i] * comp_data::Mw["H2O"]*1e-3;
            if (second_order)
            {
                dlnadxj[water_index*ns + i] -= this->dmi_dxi() * comp_data::Mw["H2O"]*1e-3;
                dlnadxj[water_index*ns + water_index] -= this->dmi_dxw(i) * comp_data::Mw["H2O"]*1e-3;
            }
        }
    }
    
    return;
}

double Jager2003::dlnaw_dP()
{
    // Calculate derivative of lnaw with respect to P
    // dlna_w/dP: eq 24
    double dlnawdP = 0.;
    if (ni > 0)  // 1st term
    {
        // Eq 26
        double sqrtI = std::sqrt(I);
        double djDHdP = 2*dA_DHdP * ((1.-std::pow(1.+sqrtI, 2))/(1.+sqrtI) + 2*std::log(1.+sqrtI));

        double sum_mc{ 0. }, sum_ma{ 0. }, d_num{ 0. };
        for (int ii = 0; ii < ni; ii++)
        {
            // sum_mc & sum_ma
            if (charge[ii] > 0)
            {
                sum_mc += std::pow(charge[ii], 2) * m_s[ii+nc];
            }
            else
            {
                sum_ma += std::pow(charge[ii], 2) * m_s[ii+nc];
            }

            if (charge[ii] > 0)
            {
                for (int jj = 0; jj < ni; jj++)
                {
                    if (charge[jj] < 0)
                    {
                        double mcma = m_s[ii+nc] * std::pow(charge[ii], 2) * m_s[jj+nc] * std::pow(charge[jj], 2);

                        // Eq 25
                        double djIdP = -djDHdP;
                        d_num += mcma * djIdP;
                    }
                }
            }
        }
        dlnawdP += comp_data::Mw["H2O"]*1e-3 * d_num / (sum_mc * sum_ma);
    }
    for (int ii = 0; ii < ns; ii++)  // 2nd term
    {
        for (int jj = 0; jj < ns; jj++)
        {
            // eq 21
            dlnawdP -= comp_data::Mw["H2O"]*1e-3 * m_s[ii] * m_s[jj] * dB0dP[ii*ns+jj];
        }
    }
    return dlnawdP;
}

double Jager2003::dlnaw_dT()
{
    // Calculate derivative of lnai with respect to T
    // dlna_w/dT: eq 24
    double dlnawdT = 0.;
    if (ni > 0)  // 1st term
    {
        // Eq 26
        double sqrtI = std::sqrt(I);
        double djDHdT = 2*dA_DHdT * ((1.-std::pow(1.+sqrtI, 2))/(1.+sqrtI) + 2*std::log(1.+sqrtI));

        double sum_mc{ 0. }, sum_ma{ 0. }, d_num{ 0. };
        for (int ii = 0; ii < ni; ii++)
        {
            // sum_mc & sum_ma
            if (charge[ii] > 0)
            {
                sum_mc += std::pow(charge[ii], 2) * m_s[ii+nc];
            }
            else
            {
                sum_ma += std::pow(charge[ii], 2) * m_s[ii+nc];
            }

            if (charge[ii] > 0)
            {
                for (int jj = 0; jj < ni; jj++)
                {
                    if (charge[jj] < 0)
                    {
                        double mcma = m_s[ii+nc] * std::pow(charge[ii], 2) * m_s[jj+nc] * std::pow(charge[jj], 2);

                        // Eq 27
                        int idx = ii*ni + jj;
                        double cc_ca = -charge[ii]*charge[jj];  // |charge_j * charge_k|
                        double djBcadT = 0.6*dBcadT[idx] * I * cc_ca/1.5;
                        djBcadT *= (1.+3*I/cc_ca)/std::pow(1.+3*I/(2*cc_ca), 2) - std::log(1.+3*I/(2*cc_ca))/(3*I/(2*cc_ca));
                        djBcadT += 2./cc_ca * (0.5*dBcadT[idx]*std::pow(I, 2) + 2./3*dCcadT[idx]*std::pow(I, 3) + 3./4*dDcadT[idx]*std::pow(I, 4));

                        // Eq 25
                        double djIdT = -(djDHdT + djBcadT);
                        d_num += mcma * djIdT;
                    }
                }
            }
        }
        dlnawdT += comp_data::Mw["H2O"]*1e-3 * d_num / (sum_mc * sum_ma);
    }
    for (int ii = 0; ii < ns; ii++)  // 2nd term
    {
        for (int jj = 0; jj < ns; jj++)
        {
            // eq 21
            dlnawdT -= comp_data::Mw["H2O"]*1e-3 * m_s[ii] * m_s[jj] * dB0dT[ii*ns+jj];
        }
    }
    return dlnawdT;
}

void Jager2003::lnam(bool second_order)
{
    // For lna_i molecular (eq. 15)
    for (int i = 0; i < nc; i++)
    {
        if (i != water_index)
        {
            lna[i] = 0.;
            for (int j = 0; j < ns; j++)
            {
                dlnadxj[i*ns + j] = 0.;
            }
        }
    }
    // For molecular/ionic interactions: Pitzer j_P1
    double sqrtI = std::sqrt(I);
    double I_inv = 1./I;
    double e = std::exp(-2.*sqrtI);
    double dedI = -e / sqrtI;
    (void) second_order;
    (void) dedI;
    (void) I_inv;
    for (int i = 0; i < nc; i++)
    {
        if (i != water_index)
        {
            for (int j = 0; j < ns; j++)
            {
                if (j != water_index)
                {
                    double j_P1ij = B0[i*ns + j];
                    if (ni > 0 && I > 0.)
                    {
                        // Pitzer contribution of ions - eq 21 and 22
                        j_P1ij += 0.5 * B1[i*ns + j] * I_inv * (1. - (1. + 2.*sqrtI) * e);
                    }
                    // eq 23
                    lna[i] += 2. * m_s[j] * j_P1ij;

                    if (second_order)
                    {
                        if (ni > 0 && I > 0.)
                        {
                            double dj_P1ij = -0.5 * B1[i*ns + j] * (std::pow(I_inv, 2) * (1. - (1. + 2.*sqrtI) * e) + 
                                                                    I_inv * (std::sqrt(I_inv) * e + (1. + 2.*sqrtI) * dedI));
                            dlnadxj[i*ns + j] += 2. * m_s[j] * dj_P1ij * dIdxj[j];
                            dlnadxj[i*ns + water_index] += 2. * m_s[j] * dj_P1ij * dIdxj[water_index];
                        }
                        
                        dlnadxj[i*ns + j] += 2. * this->dmi_dxi() * j_P1ij;
                        dlnadxj[i*ns + water_index] += 2. * this->dmi_dxw(j) * j_P1ij;
                    }
                }
            }

            lna[i] += std::log(m_s[i]);

            dlnadxj[i*ns + i] += 1./m_s[i] * this->dmi_dxi();
            dlnadxj[i*ns + water_index] += 1./m_s[i] * this->dmi_dxw(i);
        }
    }
    
    return;
}

double Jager2003::dlnam_dP(int i)
{
    // dlnai/dP for molecular: eq 23
    double dlnamdP = 0.;
    for (int jj = 0; jj < ns; jj++)
    {
        // eq 21
        double djP1dP = dB0dP[i*ns + jj];
        dlnamdP += 2 * m_s[jj] * djP1dP;
    }
    return dlnamdP;
}

double Jager2003::dlnam_dT(int i)
{
    // dlnai/dT for molecular: eq 23
    double dlnamdT = 0.;
    for (int jj = 0; jj < ns; jj++)
    {
        // eq 21
        double djP1dT = dB0dT[i*ns + jj];
        dlnamdT += 2 * m_s[jj] * djP1dT;
    }
    return dlnamdT;
}

void Jager2003::lnai(bool second_order)
{
    // For lna_i ions (eq. 16)
    for (int i = 0; i < ni; i++)
    {
        lna[nc + i] = 0.;
        for (int j = 0; j < ns; j++)
        {
            dlnadxj[(nc+i)*ns + j] = 0.;
        }
    }
    double sqrtI = std::sqrt(I);
    double I_inv = 1./I;
    double e = std::exp(-2.*sqrtI);
    double dedI = -e / sqrtI;
    (void) I_inv;
    (void) e;
    (void) dedI;

    // 1st term: j_LR - eq 18
    double j_LR = -A_DH * sqrtI / (1.+sqrtI);
    double djLRdI = -0.5 * A_DH / (sqrtI * std::pow(1.+sqrtI, 2));
    for (int i = 0; i < ni; i++)
    {
        lna[i+nc] += std::pow(this->charge[i], 2) * j_LR;
    
        if (second_order)
        {
            for (int j = 0; j < ni; j++)
            {
                dlnadxj[(i + nc) * ns + j + nc] += std::pow(this->charge[i], 2) * djLRdI * dIdxj[j+nc];
            }
            dlnadxj[(i + nc) * ns + water_index] += std::pow(this->charge[i], 2) * djLRdI * dIdxj[water_index];
        }
    }

    // 2nd term: j_SR - eq 20
    for (int i = 0; i < ni; i++)
    {
        for (int j = 0; j < ni; j++)
        {
            int ci = this->charge[i];
            int cj = this->charge[j];
            if (ci * cj < 0)
            {
                int idx = ci > 0 ? i*ni+j : j*ni + i;
                int zizj = -ci*cj;
                double cicj2 = std::pow(0.5*(std::abs(ci) + std::abs(cj)), 2);
                double j_SR = (0.13816 + 0.6 * B_ca[idx]) * zizj / std::pow(1.+1.5*I/zizj, 2)
                                + B_ca[idx] + C_ca[idx] * I + D_ca[idx] * std::pow(I, 2);
                lna[i+nc] += m_s[nc+j] * j_SR * cicj2;
                
                if (second_order)
                {
                    double djSRdI = -2 * (0.13816 + 0.6 * B_ca[idx]) * zizj / std::pow(1.+1.5*I/zizj, 3) * 1.5/zizj
                                    + C_ca[idx] + 2. * D_ca[idx] * I;
                    dlnadxj[(i + nc) * ns + i + nc] += m_s[nc+j] * djSRdI * dIdxj[nc+i] * cicj2;
                    dlnadxj[(i + nc) * ns + j + nc] += this->dmi_dxi() * j_SR * cicj2 + m_s[nc+j] * djSRdI * dIdxj[nc+j] * cicj2;
                    dlnadxj[(i + nc) * ns + water_index] += this->dmi_dxw(nc+j) * j_SR * cicj2 + m_s[nc+j] * djSRdI * dIdxj[water_index] * cicj2;
                }
            }
        }
    }

    // 3rd term: j_P1 - eq 21
    for (int i = 0; i < ni; i++)
    {
        for (int j = 0; j < ns; j++)
        {
            // Pitzer contribution of ions - eq 21 and 22
            double j_P1ij = B0[(i+nc)*ns + j] + 0.5 * B1[(i+nc)*ns + j] * I_inv * (1. - (1. + 2.*sqrtI) * e);
            lna[i+nc] += 2. * m_s[j] * j_P1ij;
            
            if (second_order && j != water_index)
            {
                double dj_P1ij = -0.5 * B1[i*ns + j] * (std::pow(I_inv, 2) * (1. - (1. + 2.*sqrtI) * e) + 
                                                        I_inv * (std::sqrt(I_inv) * e + (1. + 2.*sqrtI) * dedI));
                dlnadxj[(i+nc)*ns + j] += 2. * this->dmi_dxi() * j_P1ij + 2. * m_s[j] * dj_P1ij * dIdxj[j];
                dlnadxj[(i+nc)*ns + water_index] += 2. * this->dmi_dxw(j) * j_P1ij + 2. * m_s[j] * dj_P1ij * dIdxj[water_index];
            }
        }
    }

    // 4th term: j_P2 - eq 22
    double sum_mimj_jP2 = 0.;
    std::vector<double> dsum_mimj_jP2(ns, 0.);
    for (int j = 0; j < ns; j++)
    {
        if (j != water_index)
        {
            for (int k = 0; k < ns; k++)
            {
                if (k != water_index)
                {
                    // Pitzer contribution of ions - eq 21 and 22
                    double j_P2jk = B1[j*ns + k] * (1. - (1. + 2.*sqrtI + 2.*I) * e);
                    sum_mimj_jP2 += m_s[j] * m_s[k] * j_P2jk;

                    if (second_order)
                    {
                        double djP2jkdI = B1[j*ns + k] * ((1. - (1. + 2.*sqrtI + 2.*I)) * dedI - (std::sqrt(I_inv) + 2.) * e);
                
                        for (int i = 0; i < ns; i++)
                        {
                            dsum_mimj_jP2[i] += m_s[j] * m_s[k] * djP2jkdI * dIdxj[i];
                        }
                        dsum_mimj_jP2[j] += this->dmi_dxi() * m_s[k] * j_P2jk;
                        dsum_mimj_jP2[k] += m_s[j] * this->dmi_dxi() * j_P2jk;
                        dsum_mimj_jP2[water_index] += (this->dmi_dxw(j) * m_s[k] + m_s[j] * this->dmi_dxw(k)) * j_P2jk;
                    }
                }
            }
        }
    }
    for (int i = 0; i < ni; i++)
    {
        lna[i+nc] += 0.25 * std::pow(this->charge[i], 2) * std::pow(I_inv, 2) * sum_mimj_jP2;
        
        if (second_order)
        {
            for (int j = 0; j < ns; j++)
            {
                dlnadxj[(i+nc) * ns + j] += -0.5 * std::pow(this->charge[i], 2) * std::pow(I_inv, 3) * dIdxj[j] * sum_mimj_jP2
                                          + 0.25 * std::pow(this->charge[i], 2) * std::pow(I_inv, 2) * dsum_mimj_jP2[j];
            }
        }
    }

    // Eq 15: ai = mi/mi0 * j_i -> lnai = ln(m_i) + ln(j_i)
    for (int i = 0; i < ni; i++)
    {
        lna[i+nc] += std::log(m_s[i+nc]);
        dlnadxj[(i+nc)*ns + i+nc] += 1./m_s[i+nc] * this->dmi_dxi();
        dlnadxj[(i+nc)*ns + water_index] += 1./m_s[i+nc] * this->dmi_dxw(i+nc);
    }
    return;
}

double Jager2003::dlnai_dP(int i)
{
    // dlnai/dP for ions: eq 16
    double dlnaidP = 0.;

    // 1st term: dj_LR/dP: eq 18
    double sqrtI = std::sqrt(I);
    double dj_LRdP = -dA_DHdP * sqrtI / (1.+sqrtI);
    dlnaidP += std::pow(charge[i], 2) * dj_LRdP;

    // 2nd term: dj_SR/dP = 0 - eq 20

    // 3rd term: dj_P1/dP - eq 21
    for (int jj = 0; jj < nc; jj++)
    {
        double dj_P1dP = dB0dP[(i+nc)*ns + jj];
        dlnaidP += 2 * m_s[jj] * dj_P1dP;
    }

    // 4th term: dj_P2/dP = 0 - eq 22

    return dlnaidP;
}

double Jager2003::dlnai_dT(int i)
{
      // dlnai/dT for ions: eq 16
    double dlnaidT = 0.;

    // 1st term: dj_LR/dT - eq 18
    double sqrtI = std::sqrt(I);
    double dj_LRdT = -dA_DHdT * sqrtI / (1.+sqrtI);
    dlnaidT += std::pow(charge[i], 2) * dj_LRdT;

    // 2nd term: dj_SR/dT - eq 20
    for (int jj = 0; jj < ni; jj++)
    {
        if (charge[i] * charge[jj] < 0)
        {
            int idx = charge[i] > 0 ? i*ni+jj : jj*ni + i;
            int zizj = std::abs(charge[i]*charge[jj]);
            double dj_SRdT = 0.6 * dBcadT[idx] * zizj / std::pow(1.+1.5*I/zizj, 2)
                            + dBcadT[idx] + dCcadT[idx] * I + dDcadT[idx] * pow(I, 2);
            dlnaidT += m_s[nc+jj] * dj_SRdT * std::pow(0.5 * (std::abs(charge[i]) + std::abs(charge[jj])), 2);
        }
    }

    // 3rd term: dj_P1/dT - eq 21
    for (int jj = 0; jj < nc; jj++)
    {
        double dj_P1dT = dB0dT[(i+nc)*ns + jj];
        dlnaidT += 2 * m_s[jj] * dj_P1dT;
    }

    // 4th term: dj_P2/dT = 0 - eq 22

    return dlnaidT;
}

double Jager2003::lnphii(int i) 
{
    // Calculate fugacity coefficient
    double mu = gi[i] - hi[i] + vi[i] + lna[i];  // eq. 3.5/3.10
    return mu - gi0[i] - std::log(x[i]) - std::log(p);
}

double Jager2003::dlnphii_dP(int i) 
{
    jager::V v = jager::V(species[i]);
    double dlnaidP;
    if (i == water_index)
    {
        dlnaidP = this->dlnaw_dP();
    }
    else if (i < nc)
    {
        dlnaidP = this->dlnam_dP(i);
    }
    else
    {
        dlnaidP = this->dlnai_dP(i-nc);
    }
    double dmuidP = v.dFdP(p, T) + dlnaidP;
    return dmuidP - 1./p;
}

double Jager2003::dlnphii_dT(int i) 
{
    double dgi0dT, dlnaidT;
    if (i == water_index)
    {
        jager::IG ig = jager::IG(species[i]);
        dgi0dT = -ig.dHdT(T);
        dlnaidT = this->dlnaw_dT();
    }
    else if (i < nc)
    {
        jager::IG ig = jager::IG(species[i]);
        dgi0dT = -ig.dHdT(T);
        dlnaidT = this->dlnam_dT(i);
    }
    else
    {
        // ideal_gas::IdealGas ideal = ideal_gas::IdealGas("NaCl");
        jager::IG ig = jager::IG(this->compdata.salt);
        dgi0dT = -ig.dHdT(T);
        dlnaidT = this->dlnai_dT(i-nc);
    }
    jager::H h = jager::H(species[i]);
    jager::V v = jager::V(species[i]);
    double dmuidT = -h.dFdT(T) + v.dFdT(p, T) + dlnaidT;
    
    return dmuidT - dgi0dT;
}

double Jager2003::dlnphii_dxj(int i, int j) 
{
    double dmuidxj = dlnadxj[i*ns + j];
    if (i == j)
    {
        return dmuidxj - 1./x[i];
    }
    else
    {
        return dmuidxj;
    }
}

double Jager2003::G_PT_pure()
{
    // Calculate pure water Gibbs energy
    double mu = gi[water_index] - hi[water_index] + vi[water_index];  // eq. 3.5/3.10
    return mu - gi0[water_index] - std::log(p);
}

int Jager2003::dP_test(double p_, double T_, std::vector<double>& x_, double tol) 
{
    // Test analytical derivatives of h, v and lna with respect to P
    int error_output = 0;

    this->init_PT(p_, T_, AQEoS::CompType::water);
    this->solve_PT(x_, false, AQEoS::CompType::water);
    this->solve_PT(x_, false, AQEoS::CompType::solute);
    if (ni > 0) { this->solve_PT(x_, false, AQEoS::CompType::ion); }
    std::vector<double> vi0(ns), lnai0(ns);
    for (int i = 0; i < ns; i++)
    {
        jager::V v = jager::V(species[i]);
        vi0[i] = v.F(p_, T_);
        lnai0[i] = this->lna[i];
    }

    this->init_PT(p_+1e-5, T_, AQEoS::CompType::water);
    this->solve_PT(x_, false, AQEoS::CompType::water);
    this->solve_PT(x_, false, AQEoS::CompType::solute);
    if (ni > 0) { this->solve_PT(x_, false, AQEoS::CompType::ion); }
    std::vector<double> vi1(ns), lnai1(ns);
    for (int i = 0; i < ns; i++)
    {
        jager::V v = jager::V(species[i]);
        vi1[i] = v.F(p_+1e-5, T_);
        lnai1[i] = this->lna[i];
    }
    
    this->init_PT(p_, T_, AQEoS::CompType::water);
    this->solve_PT(x_, false, AQEoS::CompType::water);
    this->solve_PT(x_, false, AQEoS::CompType::solute);
    if (ni > 0) { this->solve_PT(x_, false, AQEoS::CompType::ion); }
    for (int i = 0; i < ns; i++)
    {
        jager::V v = jager::V(species[i]);
        double dvnum = (vi1[i]-vi0[i])/1e-5;
        double dvi = v.dFdP(p, T);
        double d = std::fabs(dvnum-dvi);
        if (d > tol)
        {
            print("comp", i);
            print("dvi/dP", std::vector<double>{dvi, dvnum, d});
            error_output++;
        }
        double dlnanum = (lnai1[i]-lnai0[i])/1e-5;

        double dlnai;
        if (i == water_index)
        {
            dlnai = this->dlnaw_dP();
        }
        else if (i < nc)
        {
            dlnai = this->dlnam_dP(i);
        }
        else
        {
            dlnai = this->dlnai_dP(i-nc);
        }

        d = std::fabs(dlnanum-dlnai);
        if (d > tol)
        {
            print("comp", i);
            print("dlnai/dP", std::vector<double>{dlnai, dlnanum, d});
            error_output++;
        }
    }

    return error_output;
}

int Jager2003::dT_test(double p_, double T_, std::vector<double>& x_, double tol) 
{
    // Test analytical derivatives of h, v, lna and gi0 with respect to T
    int error_output = 0;

    this->init_PT(p_, T_, AQEoS::CompType::water);
    this->solve_PT(x_, false, AQEoS::CompType::water);
    this->solve_PT(x_, false, AQEoS::CompType::solute);
    if (ni > 0) { this->solve_PT(x_, false, AQEoS::CompType::ion); }
    std::vector<double> hi0(ns), vi0(ns), lnai0(ns), gi00(ns);
    for (int i = 0; i < nc; i++)
    {
        jager::H h = jager::H(species[i]);
        jager::V v = jager::V(species[i]);
        hi0[i] = h.F(T_);
        vi0[i] = v.F(p_, T_);
        lnai0[i] = this->lna[i];

        jager::IG ig = jager::IG(species[i]);
        gi00[i] = ig.H(T_);
    }
    for (int i = 0; i < ni; i++)
    {
        // ideal gas Gibbs energy of salt
        jager::H h = jager::H(species[i+nc]);
        jager::V v = jager::V(species[i+nc]);
        hi0[nc + i] = h.F(T_);
        vi0[nc + i] = v.F(p_, T_);
        lnai0[nc + i] = this->lna[nc + i];

        jager::IG ig = jager::IG(this->compdata.salt);
        gi00[nc+i] = ig.H(T_);
    }

    this->init_PT(p_, T_+1e-5, AQEoS::CompType::water);
    this->solve_PT(x_, false, AQEoS::CompType::water);
    this->solve_PT(x_, false, AQEoS::CompType::solute);
    if (ni > 0) { this->solve_PT(x_, false, AQEoS::CompType::ion); }
    std::vector<double> hi1(ns), vi1(ns), lnai1(ns), gi01(ns);
    for (int i = 0; i < nc; i++)
    {
        jager::H h = jager::H(species[i]);
        jager::V v = jager::V(species[i]);
        hi1[i] = h.F(T_+1e-5);
        vi1[i] = v.F(p_, T_+1e-5);
        lnai1[i] = this->lna[i];

        jager::IG ig = jager::IG(species[i]);
        gi01[i] = ig.H(T_+1e-5);
    }
    for (int i = 0; i < ni; i++)
    {
        // ideal gas Gibbs energy of salt
        jager::H h = jager::H(species[i+nc]);
        jager::V v = jager::V(species[i+nc]);
        hi1[nc + i] = h.F(T_+1e-5);
        vi1[nc + i] = v.F(p_, T_+1e-5);
        lnai1[nc + i] = this->lna[nc + i];

        jager::IG ig = jager::IG(this->compdata.salt);
        gi01[nc + i] = ig.H(T_+1e-5);  // eq. 3.3
    }
    
    this->init_PT(p_, T_, AQEoS::CompType::water);
    this->solve_PT(x_, false, AQEoS::CompType::water);
    this->solve_PT(x_, false, AQEoS::CompType::solute);
    if (ni > 0) { this->solve_PT(x_, false, AQEoS::CompType::ion); }

    // Water and molecular solutes
    for (int i = 0; i < nc; i++)
    {
        jager::H h = jager::H(species[i]);
        double dhnum = (hi1[i]-hi0[i])/1e-5;
        double dhi = h.dFdT(T_);
        double d = std::fabs(dhnum-dhi);
        if (d > tol)
        {
            print("comp", i);
            print("dhi/dT", std::vector<double>{dhi, dhnum, d});
            error_output++;
        }
        jager::V v = jager::V(species[i]);
        double dvnum = (vi1[i]-vi0[i])/1e-5;
        double dvi = v.dFdT(p_, T_);
        d = std::fabs(dvnum-dvi);
        if (d > tol)
        {
            print("comp", i);
            print("dvi/dT", std::vector<double>{dvi, dvnum, d});
            error_output++;
        }
        double dlnanum = (lnai1[i]-lnai0[i])/1e-5;
        
        double dlnai;
        if (i == water_index)
        {
            dlnai = this->dlnaw_dT();
        }
        else
        {
            dlnai = this->dlnam_dT(i);
        }

        d = std::fabs(dlnanum-dlnai);
        if (d > tol)
        {
            print("comp", i);
            print("dlnai/dT", std::vector<double>{dlnai, dlnanum, d});
            error_output++;
        }

        jager::IG ig = jager::IG(species[i]);
        double dgi0num = (gi01[i]-gi00[i])/1e-5;
        double dgi0dT = ig.dHdT(T_);
        d = std::fabs(dgi0num-dgi0dT);
        if (d > tol)
        {
            print("comp", i);
            print("dgi0/dT", std::vector<double>{dgi0dT, dgi0num, d});
            error_output++;
        }
    }

    // Ionic solutes
    for (int i = 0; i < ni; i++)
    {
        jager::H h = jager::H(species[i+nc]);
        double dhnum = (hi1[nc+i]-hi0[nc+i])/1e-5;
        double dhi = h.dFdT(T_);
        double d = std::fabs(dhnum-dhi);
        if (d > tol)
        {
            print("comp", nc+i);
            print("dhi/dT", std::vector<double>{dhi, dhnum, d});
            error_output++;
        }
        jager::V v = jager::V(species[i+nc]);
        double dvnum = (vi1[nc+i]-vi0[nc+i])/1e-5;
        double dvi = v.dFdT(p_, T_);
        d = std::fabs(dvnum-dvi);
        if (d > tol)
        {
            print("comp", nc+i);
            print("dvi/dT", std::vector<double>{dvi, dvnum, d});
            error_output++;
        }
        
        double dlnanum = (lnai1[nc+i]-lnai0[nc+i])/1e-5;
        double dlnai = this->dlnai_dT(i);
        d = std::fabs(dlnanum-dlnai);
        if (d > tol)
        {
            print("comp", nc+i);
            print("dlnai/dT", std::vector<double>{dlnai, dlnanum, d});
            error_output++;
        }

        jager::IG ig = jager::IG(this->compdata.salt);
        double dgi0num = (gi01[nc+i]-gi00[nc+i])/1e-5;
        double dgi0dT = ig.dHdT(T_);
        d = std::fabs(dgi0num-dgi0dT);
        if (d > tol)
        {
            print("comp", nc+i);
            print("dgi0/dT", std::vector<double>{dgi0dT, dgi0num, d});
            error_output++;
        }
    }

    return error_output;
}
