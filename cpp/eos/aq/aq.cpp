#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <map>

#include "dartsflash/eos/aq/aq.hpp"
#include "dartsflash/eos/aq/ziabakhsh.hpp"
#include "dartsflash/eos/aq/jager.hpp"

AQEoS::AQEoS(CompData& comp_data) : EoS(comp_data) 
{
	this->evaluators = {};
	this->evaluator_map = {};

	this->m_s.resize(comp_data.ns);
	this->constant_salinity = false;
	this->multiple_minima = false;
}

AQEoS::AQEoS(CompData& comp_data, AQEoS::Model model) : AQEoS(comp_data)
{
	// Set evaluator map for each CompType to Model
	this->evaluator_map = {
		{AQEoS::CompType::water, model},
		{AQEoS::CompType::solute, model},
	};
	// If ions present, add ion to evaluator_map
	if (ni > 0)	{ this->evaluator_map.insert({AQEoS::CompType::ion, model}); }

	// Add instance of AQBase to map of evaluators
	switch (model)
	{
		case AQEoS::Model::Ziabakhsh2012:
		{
			this->evaluators.insert({model, new Ziabakhsh2012(comp_data)});
			break;
		}
		case AQEoS::Model::Jager2003:
		{
			this->evaluators.insert({model, new Jager2003(comp_data)});
			break;
		}
		default:
        {
        	std::cout << "Invalid AQBase model specified\n";
	        exit(1);
        }
	}
}

AQEoS::AQEoS(CompData& comp_data, std::map<CompType, Model>& evaluator_map_)
	: AQEoS(comp_data)
{
	// Set evaluator_map with keys (water, solute, ion) that point to a model
	this->evaluator_map = evaluator_map_;

	// Add AQBase objects to evaluators
	for (auto& it: evaluator_map) 
	{
		if (evaluators.count(it.second) == 0)
		{
			switch (it.second)
			{
				case Model::Ziabakhsh2012:
				{
					evaluators[it.second] = new Ziabakhsh2012(comp_data);
					break;
				}
				case Model::Jager2003:
				{
					evaluators[it.second] = new Jager2003(comp_data);
					break;
				}
				default:
            	{
                	std::cout << "Invalid AQBase model specified\n";
	                exit(1);
    	        }
			}
		}
	}
}

AQEoS::AQEoS(CompData& comp_data, std::map<CompType, Model>& evaluator_map_, std::map<Model, AQBase*>& evaluators_)
	: AQEoS(comp_data)
{
	// Define evaluator_map with keys (water, solute, ion) that point to a model
	this->evaluator_map = evaluator_map_;

	// Add copies of AQBase objects to evaluators
	for (auto& it: evaluators_) 
	{
		evaluators[it.first] = it.second->getCopy();
	}
}

void AQEoS::init_PT(double p_, double T_, bool calc_gpure)
{
	// Evaluate water, solute and ion evaluator parameters
	if (p_ != p || T_ != T)
	{
		this->p = p_;
		this->T = T_;

		this->evaluators[this->evaluator_map[CompType::water]]->init_PT(p_, T_, CompType::water);
		this->evaluators[this->evaluator_map[CompType::solute]]->init_PT(p_, T_, CompType::solute);
		if (ni > 0)	{ this->evaluators[this->evaluator_map[CompType::ion]]->init_PT(p_, T_, CompType::ion); }

		if (calc_gpure)
		{
			this->gpure = this->G_PT_pure();
		}
	}
}

void AQEoS::solve_PT(std::vector<double>::iterator n_it, bool second_order)
{
	// Calculate mole fractions and molality of molecular and ionic species
	std::vector<double> x(ns);
	this->n_iterator = n_it;
	double nT_inv = 1./std::accumulate(n_it, n_it + ns, 0.);
    std::transform(n_it, n_it + ns, x.begin(), [&nT_inv](double element) { return element *= nT_inv; });
	
	this->species_molality(x);
	this->evaluators[this->evaluator_map[CompType::water]]->solve_PT(x, second_order, CompType::water);
	this->evaluators[this->evaluator_map[CompType::solute]]->solve_PT(x, second_order, CompType::solute);
	if (ni > 0)	{ this->evaluators[this->evaluator_map[CompType::ion]]->solve_PT(x, second_order, CompType::ion); }
	return;
}

double AQEoS::lnphii(int i)
{
	// Evaluate fugacity coefficient of component i
	Model model;
	if (i == this->compdata.water_index)
	{
		model = this->evaluator_map[CompType::water];
	}
	else if (i < nc)
	{
		model = this->evaluator_map[CompType::solute];
	}
	else
	{
		model = this->evaluator_map[CompType::ion];
	}
	return this->evaluators[model]->lnphii(i);
}

double AQEoS::dlnphii_dP(int i)
{
	// Evaluate derivative of fugacity coefficient w.r.t. pressure of component i
	Model model;
	if (i == this->compdata.water_index)
	{
		model = this->evaluator_map[CompType::water];
	}
	else if (i < nc)
	{
		model = this->evaluator_map[CompType::solute];
	}
	else
	{
		model = this->evaluator_map[CompType::ion];
	}
	return this->evaluators[model]->dlnphii_dP(i);
}

double AQEoS::dlnphii_dT(int i)
{
	// Evaluate derivative of fugacity coefficient w.r.t. temperature of component i
	Model model;
	if (i == this->compdata.water_index)
	{
		model = this->evaluator_map[CompType::water];
	}
	else if (i < nc)
	{
		model = this->evaluator_map[CompType::solute];
	}
	else
	{
		model = this->evaluator_map[CompType::ion];
	}
	return this->evaluators[model]->dlnphii_dT(i);
}

double AQEoS::dlnphii_dnj(int i, int k) {
	// Evaluate derivative of fugacity coefficient w.r.t. n_k of component i
	Model model;
	if (i == this->compdata.water_index)
	{
		model = this->evaluator_map[CompType::water];
	}
	else if (i < nc)
	{
		model = this->evaluator_map[CompType::solute];
	}
	else
	{
		model = this->evaluator_map[CompType::ion];
	}

	std::vector<double> dlnphiidxj(ns);
	for (int j = 0; j < ns; j++)
	{
		dlnphiidxj[j] = this->evaluators[model]->dlnphii_dxj(i, j);
	}

	// Translate from dxj to dnj
	return this->dxj_to_dnk(dlnphiidxj, this->n_iterator, k);
}

double AQEoS::dlnphii_dxj(int i, int j)
{
	Model model;
	if (i == this->compdata.water_index)
	{
		model = this->evaluator_map[CompType::water];
	}
	else if (i < nc)
	{
		model = this->evaluator_map[CompType::solute];
	}
	else
	{
		model = this->evaluator_map[CompType::ion];
	}

	return this->evaluators[model]->dlnphii_dxj(i, j);
}

void AQEoS::species_molality(std::vector<double>& x) {
	// molality of dissolved species
	for (int ii = 0; ii < ns; ii++)
	{
		if (ii == this->compdata.water_index)
		{
			m_s[ii] = 0.;
		}
		else
		{
			m_s[ii] = 55.509 * x[ii] / x[this->compdata.water_index];
			if (ii >= nc)
			{
				if (m_s[ii] > 6.)
				{
					m_s[ii] = 6.;
				}
			}
		}
	}

	// Molality of ions
	if (this->compdata.constant_salinity)
	{
		for (int ii = 0; ii < ni; ii++)
		{
			m_s[nc + ii] = this->compdata.m_i[ii];
		}
	}

	// Set
	this->evaluators[this->evaluator_map[CompType::water]]->set_species_molality(m_s);
	this->evaluators[this->evaluator_map[CompType::solute]]->set_species_molality(m_s);
	if (ni > 0)	{ this->evaluators[this->evaluator_map[CompType::ion]]->set_species_molality(m_s); }

	return;
}

std::vector<double> AQEoS::G_PT_pure()
{
    // Calculate pure component Gibbs energy at P, T
    std::vector<double> G_pure(nc, NAN);

	// Only H2O component is not NAN
	Model H2O_model = this->evaluator_map[CompType::water];
	G_pure[this->compdata.water_index] = this->evaluators[H2O_model]->G_PT_pure();

	return G_pure;
}

AQBase::AQBase(CompData& comp_data)
{
	this->compdata = comp_data;
	this->nc = comp_data.nc;
	this->ni = comp_data.ni;
	this->ns = comp_data.ns;

	this->x.resize(ns);
	this->m_s.resize(ns);

	this->species.resize(ns);
	std::copy(comp_data.components.begin(), comp_data.components.end(), this->species.begin());
	std::copy(comp_data.ions.begin(), comp_data.ions.end(), this->species.begin()+nc);
	
	this->charge = comp_data.charge;

	this->water_index = comp_data.water_index;
}

double AQBase::dmi_dxi() {
	// Derivative of mi(xi) w.r.t. xi
	return 55.509 / x[water_index];
}

double AQBase::dmi_dxw(int i) {
	// Derivative of mi(xi) w.r.t. xw
	return -55.509 * x[i] / std::pow(x[water_index], 2);
}
