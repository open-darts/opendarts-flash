#include <algorithm>
#include "dartsflash/eos/eos.hpp"
#include "dartsflash/eos/ideal.hpp"
#include "dartsflash/eos/aq/aq.hpp"
#include "dartsflash/eos/aq/jager.hpp"
#include "dartsflash/eos/aq/ziabakhsh.hpp"
#include "dartsflash/eos/helmholtz/cubic.hpp"
// #include "dartsflash/eos/helmholtz/gerg.hpp"
#include "dartsflash/eos/solid/solid.hpp"
#include "dartsflash/eos/vdwp/ballard.hpp"
#include "dartsflash/eos/vdwp/munck.hpp"
#include "dartsflash/flash/flash_params.hpp"

int test_ideal();
int test_cubic();
int test_aq();
// int test_gerg();
int test_solid();
int test_vdwp();
int test_gmix();

int dlnphi_test(EoS* eos, double p, double T, std::vector<double>& n, double tol) 
{
    // Analytical derivatives w.r.t. composition, pressure and temperature
	int error_output = 0;
	int ns = static_cast<int>(n.size());

	eos->solve_PT(p, T, n, 0, true);
	std::vector<double> lnphi = eos->lnphi();
    std::vector<double> dlnphidn = eos->dlnphi_dn();
	std::vector<double> dlnphidP = eos->dlnphi_dP();
	std::vector<double> dlnphidT = eos->dlnphi_dT();

	// Calculate numerical derivatives w.r.t. composition
	double dn = 1e-6;
	std::vector<double> lnphi1;
    for (int j = 0; j < ns; j++)
	{
        // Numerical derivative of lnphi w.r.t. nj
	    n[j] += dn;
		eos->solve_PT(p, T, n, 0, false);
	    lnphi1 = eos->lnphi();
		n[j] -= dn;
        for (int i = 0; i < ns; i++)
	    {
	        double dlnphidn_num = (lnphi1[i] - lnphi[i])/dn;
			double d = std::log(std::fabs(dlnphidn_num + 1e-15)) - std::log(std::fabs(dlnphidn[i*ns + j] + 1e-15));
        	if (std::fabs(d) > tol && !(std::fabs(dlnphidn[i*ns+j] + 1e-15) < 1e-14))
	        {
	            print("comp", std::vector<int>{i, j});
        	    print("dlnphi/dn", std::vector<double>{dlnphidn[i*ns+j], dlnphidn_num, d});
                error_output++;
	        }
        }
	}

	// Calculate numerical derivatives w.r.t. pressure
	double dp = 1e-6;
	p += dp;
	eos->solve_PT(p, T, n, 0, false);
	lnphi1 = eos->lnphi();
	p -= dp;

	// Compare analytical and numerical
	for (int i = 0; i < ns; i++)
	{
        double dlnphidP_num = (lnphi1[i] - lnphi[i])/dp;
		// Use logarithmic scale to compare
		double d = std::log(std::fabs(dlnphidP_num + 1e-15)) - std::log(std::fabs(dlnphidP[i] + 1e-15));
	    if (std::fabs(d) > tol && (std::fabs(dlnphidP[i]) > 1e-8 && std::fabs(dlnphidP_num) > 1e-8))
        {
        	print("comp", i);
            print("dlnphi/dP", std::vector<double>{dlnphidP[i], dlnphidP_num, d});
    	    error_output++;
	    }
    }

	// Calculate numerical derivatives w.r.t. temperature
	double dT = 1e-6;
	T += dT;
	eos->solve_PT(p, T, n, 0, false);
	lnphi1 = eos->lnphi();
	T -= dT;

	// Compare analytical and numerical
	for (int i = 0; i < ns; i++)
	{
		double dlnphidT_num = (lnphi1[i] - lnphi[i])/dT;
		double d = std::log(std::fabs(dlnphidT_num + 1e-15)) - std::log(std::fabs(dlnphidT[i] + 1e-15));
		if (std::fabs(d) > tol && (std::fabs(dlnphidT[i]) > 1e-8 && std::fabs(dlnphidT_num) > 1e-8))
		{
			print("comp", i);
			print("dlnphi/dT", std::vector<double>{dlnphidT[i], dlnphidT_num, d});
			error_output++;
		}
	}

	// Calculate lnphi and derivatives for -n
	std::vector<double> n_n = n;
	std::transform(n.begin(), n.end(), n_n.begin(), [](double element) { return element *= -1; });
	eos->solve_PT(p, T, n_n, 0, true);
	std::vector<double> lnphi_n = eos->lnphi();
	std::vector<double> dlnphidP_n = eos->dlnphi_dP();
	std::vector<double> dlnphidT_n = eos->dlnphi_dT();
	std::vector<double> dlnphidn_n = eos->dlnphi_dn();

	// Compare positive and negative
	for (int i = 0; i < ns; i++)
	{
		double d = lnphi[i] - lnphi_n[i];
		if (std::fabs(d) > tol)
		{
			print("comp", i);
			print("lnphi negative", std::vector<double>{lnphi[i], lnphi_n[i], d});
			error_output++;
		}
		d = dlnphidP[i] - dlnphidP_n[i];
		if (std::fabs(d) > tol)
		{
			print("comp", i);
			print("dlnphi/dP negative", std::vector<double>{dlnphidP[i], dlnphidP_n[i], d});
			error_output++;
		}
		d = dlnphidT[i] - dlnphidT_n[i];
		if (std::fabs(d) > tol)
		{
			print("comp", i);
			print("dlnphi/dT negative", std::vector<double>{dlnphidT[i], dlnphidT_n[i], d});
			error_output++;
		}
		for (int j = 0; j < ns; j++)
		{
			d = dlnphidn[i*ns + j] + dlnphidn_n[i*ns + j];
			if (std::fabs(d) > tol)
			{
				print("comp", std::vector<int>{i, j});
				print("dlnphi/dn negative", std::vector<double>{dlnphidn[i*ns + j], dlnphidn_n[i*ns + j], d});
				error_output++;
			}
		}
	}

    return error_output;
}

int gmix_test(EoS* eos, double p, double T, std::vector<double>& x, std::vector<double>& gpure, bool verbose = false)
{
	// Test for finding local minimum of Gibbs energy of mixing surface
	double gmix = 0.;
	eos->init_PT(p, T);
	std::vector<double> xmin = eos->mix_min(x, gpure, gmix);

	if (std::isnan(xmin[0]))
	{
		std::cout << "Minimum of gmix not found\n";
		print("p, T", std::vector<double>{p, T});
		print("X", x);
		print("xmin", xmin);
		return 1;
	}
	else
	{
		if (verbose)
		{
			std::cout << "Local minimum of gmix\n";
			print("p, T", std::vector<double>{p, T});
			print("x", x);
			print("xmin", xmin);
		}
		return 0;
	}
}

int main() {

    int error_output = 0;

	error_output += test_ideal();
	error_output += test_cubic();
	error_output += test_aq();
	error_output += test_solid();
	error_output += test_vdwp();
	error_output += test_gmix();

    return error_output;
}

int test_ideal()
{
	// Test implementation of ideal gas enthalpy and derivative w.r.t. T
	int error_output = 0;
	double d;

	std::vector<std::string> comp = {"C1"};
	CompData comp_data(comp);
	comp_data.cpi = {comp_data::cpi["C1"]};

	IdealGas ig(comp_data);
	std::vector<double> x{ 1. };
	double T = 300.;
	double dT = 1e-5;
	double tol = 1e-5;

	// Enthalpy and heat capacity test
	double H0 = ig.hi(T, 0);
	double H1 = ig.hi(T + dT, 0);
	double CP = ig.cpi(T, 0);
	double CP_num = (H1-H0)/dT;
	d = std::log(std::fabs(CP + 1e-15)) - std::log(std::fabs(CP_num + 1e-15));
	if (std::fabs(d) > tol) { print("dH/dT != dH/dT", std::vector<double>{CP, CP_num, d}); error_output++; }

	if (error_output > 0)
	{
		print("Errors occurred in test_ideal()", error_output);
	}
	else
	{
		print("No errors occurred in test_ideal()", error_output);
	}
	return error_output;
}

int test_cubic()
{
	// Test Cubics (Peng-Robinson and Soave-Redlich-Kwong)
	int error_output = 0;
	double p{ 30. }, T{ 300. };

	// Pure CO2
	std::vector<std::string> comp = {"CO2"};
	CompData comp_data(comp);
	comp_data.Pc = {73.75};
    comp_data.Tc = {304.1};
    comp_data.ac = {0.239};
    comp_data.kij = {0.};
	std::vector<double> n = {1.};

	CubicEoS pr(comp_data, CubicEoS::PR);
    CubicEoS srk(comp_data, CubicEoS::SRK);

	// Test of analytical derivatives
	error_output += dlnphi_test(&pr, p, T, n, 1e-3);
	error_output += dlnphi_test(&srk, p, T, n, 1e-3);

    // // Consistency tests for cubics
	error_output += pr.derivatives_test(p, T, n, 2e-4);
	error_output += pr.lnphi_test(p, T, n, 2e-4);
	error_output += pr.pressure_test(p, T, n, 1e-5);
	// error_output += pr.temperature_test(p, T, n, 1e-5);
	error_output += pr.composition_test(p, T, n, 1e-5);
	error_output += pr.pvt_test(p, T, n, 1e-5);
	error_output += pr.properties_test(p, T, n, 1e-5);

	error_output += srk.derivatives_test(p, T, n, 2e-4);
	error_output += srk.lnphi_test(p, T, n, 3e-4);
	error_output += srk.pressure_test(p, T, n, 1e-5);
	// error_output += srk.temperature_test(p, T, n, 1e-5);
	error_output += srk.composition_test(p, T, n, 1e-5);
	error_output += srk.pvt_test(p, T, n, 1e-5);
	error_output += srk.properties_test(p, T, n, 1e-5);

	// // Test mixing rules (use PR parameters)
	error_output += pr.mix_dT_test(T, n, 1e-4);
	error_output += srk.mix_dT_test(T, n, 1e-4);

	// MY10 mixture
	//// Sour gas mixture, data from (Li, 2012)
	comp = std::vector<std::string>{"CO2", "N2", "H2S", "C1", "C2", "C3"};
    comp_data = CompData(comp);
	comp_data.Pc = {73.819, 33.9, 89.4, 45.992, 48.718, 42.462};
    comp_data.Tc = {304.211, 126.2, 373.2, 190.564, 305.322, 369.825};
    comp_data.ac = {0.225, 0.039, 0.081, 0.01141, 0.10574, 0.15813};
    comp_data.kij = std::vector<double>(6*6, 0.);
    comp_data.set_binary_coefficients(0, {0., -0.02, 0.12, 0.125, 0.135, 0.150});
    comp_data.set_binary_coefficients(1, {-0.02, 0., 0.2, 0.031, 0.042, 0.091});
    comp_data.set_binary_coefficients(2, {0.12, 0.2, 0., 0.1, 0.08, 0.08});
	n = {0.9, 0.03, 0.04, 0.06, 0.04, 0.03};

	pr = CubicEoS(comp_data, CubicEoS::PR);
    srk = CubicEoS(comp_data, CubicEoS::SRK);

	// Test of analytical derivatives
	error_output += dlnphi_test(&pr, p, T, n, 1e-3);
	error_output += dlnphi_test(&srk, p, T, n, 1e-3);
	
    // // Consistency tests for cubics
	error_output += pr.derivatives_test(p, T, n, 2e-4);
	error_output += pr.lnphi_test(p, T, n, 2e-4);
	error_output += pr.pressure_test(p, T, n, 1e-5);
	// error_output += pr.temperature_test(p, T, n, 1e-5);
	error_output += pr.composition_test(p, T, n, 1e-5);
	error_output += pr.pvt_test(p, T, n, 1e-5);
	error_output += pr.properties_test(p, T, n, 1e-5);

	error_output += srk.derivatives_test(p, T, n, 2e-4);
	error_output += srk.lnphi_test(p, T, n, 1e-3);
	error_output += srk.pressure_test(p, T, n, 1e-5);
	// error_output += srk.temperature_test(p, T, n, 1e-5);
	error_output += srk.composition_test(p, T, n, 1e-5);
	error_output += srk.pvt_test(p, T, n, 1e-5);
	error_output += srk.properties_test(p, T, n, 1e-5);

	// // Test mixing rules (use PR parameters)
	error_output += pr.mix_dT_test(T, n, 1e-4);
	error_output += srk.mix_dT_test(T, n, 1e-4);

	// H2O-CO2 mixture
	comp = {"H2O", "CO2"};
    comp_data = CompData(comp);
    comp_data.Pc = {220.50, 73.75};
    comp_data.Tc = {647.14, 304.10};
    comp_data.ac = {0.328, 0.239};
    comp_data.kij = std::vector<double>(2*2, 0.);
	comp_data.set_binary_coefficients(0, {0., 0.19014});
	n = {1., 0.};

	pr = CubicEoS(comp_data, CubicEoS::PR);
	srk = CubicEoS(comp_data, CubicEoS::SRK);
	error_output += pr.pvt_test(1., 360., n, 1e-5);
	error_output += srk.pvt_test(1., 360., n, 1e-5);

	if (error_output > 0)
	{
		print("Errors occurred in test_cubic()", error_output);
	}
	else
	{
		print("No errors occurred in test_cubic()", error_output);
	}
	return error_output;
}

int test_aq()
{
    int error_output = 0;

	// Without ions
    std::vector<std::string> comp{"H2O", "CO2", "C1"};
	CompData comp_data(comp);

	std::vector<double> n{0.95, 0.045, 0.005};
	double p{20.}, T{300.};

	Ziabakhsh2012 zia(comp_data);
	Jager2003 jag(comp_data);

	error_output += jag.dP_test(p, T, n, 1e-5);
	error_output += jag.dT_test(p, T, n, 1e-5);

	// Test AQEoS
	// Jager (2003) for water and solutes
	AQEoS aq(comp_data, AQEoS::Model::Jager2003);
	error_output += dlnphi_test(&aq, p, T, n, 1e-4);

	// Ziabakhsh (2012) for water and solutes
	aq = AQEoS(comp_data, AQEoS::Model::Ziabakhsh2012);
	error_output += dlnphi_test(&aq, p, T, n, 1e-4);

	// Test mixed AQEoS models: Jager (2003) for water and Ziabakhsh (2012) for solutes
	// Test passing evaluator_map with [CompType, Model]
	std::map<AQEoS::CompType, AQEoS::Model> evaluator_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
	aq = AQEoS(comp_data, evaluator_map);
	error_output += dlnphi_test(&aq, p, T, n, 1e-4);

	// Test passing map of evaluator pointers to AQEoS constructor
	std::map<AQEoS::Model, AQBase*> evaluators = {
		{AQEoS::Model::Jager2003, &jag},
		{AQEoS::Model::Ziabakhsh2012, &zia}
	};
	aq = AQEoS(comp_data, evaluator_map, evaluators);
	error_output += dlnphi_test(&aq, p, T, n, 1e-4);

	// With ions
	// Na+ and Cl-
	std::vector<std::string> ions = {"Na+", "Cl-"};
	comp_data = CompData(comp, ions);
	comp_data.charge = {1, -1};

	n = {0.9, 0.045, 0.005, 0.025, 0.025};

	zia = Ziabakhsh2012(comp_data);
	jag = Jager2003(comp_data);
	error_output += jag.dP_test(p, T, n, 1e-4);
	error_output += jag.dT_test(p, T, n, 1e-4);

	// Test Jager2003 with ions
	aq = AQEoS(comp_data, AQEoS::Model::Jager2003);
	error_output += dlnphi_test(&aq, p, T, n, 1.1e-2);

	// Test Ziabakhsh2012 with ions
	aq = AQEoS(comp_data, AQEoS::Model::Ziabakhsh2012);
	error_output += dlnphi_test(&aq, p, T, n, 1.1e-2);

	// Test mixed evaluators
	evaluators[AQEoS::Model::Jager2003] = &jag;
	evaluators[AQEoS::Model::Ziabakhsh2012] = &zia;

	aq = AQEoS(comp_data, evaluator_map, evaluators);
	error_output += dlnphi_test(&aq, p, T, n, 1.1e-2);

	// Ca2+ and Cl-
	ions = {"Ca2+", "Cl-"};
	comp_data = CompData(comp, ions);
	comp_data.charge = {2, -1};

	n = {0.9, 0.045, 0.005, 0.01667, 0.03333};

	zia = Ziabakhsh2012(comp_data);
	jag = Jager2003(comp_data);
	error_output += jag.dP_test(p, T, n, 1e-4);
	error_output += jag.dT_test(p, T, n, 1e-4);

	// Test Jager2003 with ions
	aq = AQEoS(comp_data, AQEoS::Model::Jager2003);
	error_output += dlnphi_test(&aq, p, T, n, 1.1e-1);

	// Test Ziabakhsh2012 with ions
	aq = AQEoS(comp_data, AQEoS::Model::Ziabakhsh2012);
	error_output += dlnphi_test(&aq, p, T, n, 1.1e-2);

	// Test mixed evaluators
	evaluators[AQEoS::Model::Jager2003] = &jag;
	evaluators[AQEoS::Model::Ziabakhsh2012] = &zia;

	aq = AQEoS(comp_data, evaluator_map, evaluators);
	error_output += dlnphi_test(&aq, p, T, n, 1.1e-2);


	if (error_output > 0)
	{
		print("Errors occurred in test_aq()", error_output);
	}
	else
	{
		print("No errors occurred in test_aq()", error_output);
	}
    return error_output;
}

int test_solid()
{
    int error_output = 0;
	double p{ 20.}, T{ 300. };
	std::vector<double> n{ 1. };

	// Test derivatives of H and V functions
	solid_par::H h = solid_par::H("NaCl");
    solid_par::V v = solid_par::V("NaCl");
	error_output += h.test_derivatives(T, 1e-5);
	error_output += v.test_derivatives(p, T, 1e-5);

	// Test for pure component only
	CompData comp_data({"NaCl"});
	PureSolid s(comp_data, "NaCl");
	error_output += dlnphi_test(&s, p, T, n, 1e-5);
	error_output += s.pvt_test(p, T, n, 1e-5);

	// Test Ice for vector of components
	comp_data = CompData({"H2O", "CO2", "C1"});
	s = PureSolid(comp_data, "Ice");
	error_output += dlnphi_test(&s, p, T, n, 1e-5);
	error_output += s.pvt_test(p, T, n, 1e-5);

	if (error_output > 0)
	{
		print("Errors occurred in test_solid()", error_output);
	}
	else
	{
		print("No errors occurred in test_solid()", error_output);
	}
	return error_output;
}

int test_vdwp()
{
	int error_output = 0;

	double p{ 30. }, T{ 280. };

	// Single-component sI hydrates
	std::vector<std::string> comp{"H2O", "CO2"};
	CompData comp_data(comp);
	std::vector<double> n{0.9, 0.1};

	Ballard ballard(comp_data, "sI");
	error_output += ballard.dP_test(p, T, n, 1e-5);
	error_output += ballard.dT_test(p, T, n, 1e-5);
	error_output += dlnphi_test(&ballard, p, T, n, 1e-4);

	Munck munck(comp_data, "sI");
	error_output += munck.dP_test(p, T, n, 1e-5);
	error_output += munck.dT_test(p, T, n, 1e-5);
	error_output += dlnphi_test(&munck, p, T, n, 1e-4);

	// Multi-component sI hydrates
	comp = {"H2O", "CO2", "C1"};
	comp_data = CompData(comp);
	n = {0.86, 0.1, 0.04};
	
	ballard = Ballard(comp_data, "sI");
	error_output += ballard.dP_test(p, T, n, 1e-5);
	error_output += ballard.dT_test(p, T, n, 1e-5);
	error_output += dlnphi_test(&ballard, p, T, n, 1e-4);

	munck = Munck(comp_data, "sI");
	error_output += munck.dP_test(p, T, n, 1e-5);
	error_output += munck.dT_test(p, T, n, 1e-5);
	error_output += dlnphi_test(&munck, p, T, n, 1e-4);

	// Single-component sII hydrates
	comp = {"H2O", "CO2"};
	comp_data = CompData(comp);
	n = {0.9, 0.1};

	ballard = Ballard(comp_data, "sII");
	error_output += ballard.dP_test(p, T, n, 1e-5);
	error_output += ballard.dT_test(p, T, n, 1e-5);
	error_output += dlnphi_test(&ballard, p, T, n, 1e-4);

	munck = Munck(comp_data, "sII");
	error_output += munck.dP_test(p, T, n, 1e-5);
	error_output += munck.dT_test(p, T, n, 1e-5);
	error_output += dlnphi_test(&munck, p, T, n, 1e-4);

	// Multi-component sII hydrates
	comp = {"H2O", "CO2", "C1"};
	comp_data = CompData(comp);
	n = {0.86, 0.1, 0.04};
	
	ballard = Ballard(comp_data, "sII");
	error_output += ballard.dP_test(p, T, n, 1e-5);
	error_output += ballard.dT_test(p, T, n, 1e-5);
	error_output += dlnphi_test(&ballard, p, T, n, 1e-4);

	munck = Munck(comp_data, "sII");
	error_output += munck.dP_test(p, T, n, 1e-5);
	error_output += munck.dT_test(p, T, n, 1e-5);
	error_output += dlnphi_test(&munck, p, T, n, 1e-4);
    
	if (error_output > 0)
	{
		print("Errors occurred in test_vdwp()", error_output);
	}
	else
	{
		print("No errors occurred in test_vdwp()", error_output);
	}
	return error_output;
}

int test_gmix()
{
	// Test implementation of Gibbs energy of mixing
	const bool verbose = false;
	int error_output = 0;

	// Binary mixture H2O-CO2
	std::vector<std::string> comp = {"H2O", "CO2"};
	CompData comp_data(comp);
	comp_data.Pc = {220.50, 73.75};
    comp_data.Tc = {647.14, 304.10};
    comp_data.ac = {0.328, 0.239};
    comp_data.kij = std::vector<double>(2*2, 0.);
	comp_data.set_binary_coefficients(0, {0., 0.19014});
	comp_data.cpi = {comp_data::cpi["H2O"], comp_data::cpi["CO2"]};

	IdealGas ig(comp_data);
	CubicEoS ceos(comp_data, CubicEoS::PR);
	ceos.set_preferred_roots(0, 0.6, EoS::RootFlag::MAX);

	std::map<AQEoS::CompType, AQEoS::Model> evaluator_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
	AQEoS aq(comp_data, evaluator_map);
	aq.set_eos_range(0, std::vector<double>{0.6, 1.});

	Ballard si(comp_data, "sI");
	Ballard sii(comp_data, "sII");

	PureSolid ice(comp_data, "Ice");

	FlashParams flashparams(comp_data);
	// flashparams.add_eos("IG", &ig);
	flashparams.add_eos("CEOS", &ceos);
	flashparams.add_eos("AQ", &aq);
	flashparams.add_eos("sI", &si);
	flashparams.add_eos("sII", &sii);
	// flashparams.add_eos("Ice", &ice);
	flashparams.verbose = verbose;

	std::vector<std::pair<std::vector<double>, std::vector<double>>> references = {
		{{1., 273.15}, {-5.037806881, -0.007120529782}},
		{{10., 273.15}, {-7.333254784, -0.07266156188}},
		{{100., 273.15}, {-9.564637647, -1.201389886}},
		{{1., 373.15}, {-0.008472870891, -0.002637647353}},
		{{10., 373.15}, {-2.260979589, -0.02638413411}},
		{{100., 373.15}, {-4.509159992, -0.2619219487}},
		{{1., 473.15}, {-0.004420302368, -0.001043064989}},
		{{10., 473.15}, {-0.04489612603, -0.01035300458}},
		{{100., 473.15}, {-1.87757965, -0.09494685447}},
	};

	std::vector<std::vector<double>> n = {
		{0.95, 0.05}, {0.05, 0.95}
	};

	for (auto ref: references)
	{
		double p = ref.first[0];
		double T = ref.first[1];
		std::vector<double> gpure = flashparams.G_PT_pure(p, T);

		for (size_t i = 0; i < gpure.size(); i++)
		{
			if (std::fabs(gpure[i]-ref.second[i]) > 1e-6)
			{
				print("Different values for Gpure", std::vector<double>{p, T, gpure[i]-ref.second[i]});
				print("result", gpure);
				print("ref", ref.second);
				error_output++;
			}
		}

		// Test for finding local minimum of Gibbs energy of mixing
		TrialPhase trial;

		trial = flashparams.find_ref_comp(p, T, n[0]);
		error_output += gmix_test(flashparams.eos_params[trial.eos_name].eos, p, T, n[0], gpure, verbose);

		trial = flashparams.find_ref_comp(p, T, n[1]);
		error_output += gmix_test(flashparams.eos_params[trial.eos_name].eos, p, T, n[1], gpure, verbose);
	}

	// Ternary mixture H2O-CO2-C1
	comp = {"H2O", "CO2", "C1"};
	comp_data = CompData(comp);
	comp_data.Pc = {220.50, 73.75, 46.04};
	comp_data.Tc = {647.14, 304.10, 190.58};
	comp_data.ac = {0.328, 0.239, 0.012};
	comp_data.kij = std::vector<double>(3*3, 0.);
    comp_data.set_binary_coefficients(0, {0., 0.19014, 0.47893});
	comp_data.set_binary_coefficients(1, {0.19014, 0., 0.0936});
	comp_data.Mw = {18.015, 44.01, 16.043};
	comp_data.cpi = {comp_data::cpi["H2O"], comp_data::cpi["CO2"], comp_data::cpi["C1"]};

	ig = IdealGas(comp_data);
	ceos = CubicEoS(comp_data, CubicEoS::PR);
	ceos.set_preferred_roots(0, 0.6, EoS::RootFlag::MAX);

	aq = AQEoS(comp_data, evaluator_map);
	aq.set_eos_range(0, std::vector<double>{0.6, 1.});

	si = Ballard(comp_data, "sI");
	sii = Ballard(comp_data, "sII");

	ice = PureSolid(comp_data, "Ice");

	flashparams = FlashParams(comp_data);
	// flashparams.add_eos("IG", &ig);
	flashparams.add_eos("CEOS", &ceos);
	flashparams.add_eos("AQ", &aq);
	flashparams.add_eos("sI", &si);
	flashparams.add_eos("sII", &sii);
	// flashparams.add_eos("Ice", &ice);
	flashparams.verbose = verbose;

	references = {
		{{1., 273.15}, {-5.037806881, -0.007120529782, -0.00293389646}},
		{{10., 273.15}, {-7.333254784, -0.07266156188, -0.02922065073}},
		{{100., 273.15}, {-9.564637647, -1.201389886, -0.2713925062}},
		{{1., 373.15}, {-0.008472870891, -0.002637647353, -0.0009766608968}},
		{{10., 373.15}, {-2.260979589, -0.02638413411, -0.009632032391}},
		{{100., 373.15}, {-4.509159992, -0.2619219487, -0.08209063185}},
		{{1., 473.15}, {-0.004420302368, -0.001043064989, -0.0002830630341}},
		{{10., 473.15}, {-0.04489612603, -0.01035300458, -0.002754486846}},
		{{100., 473.15}, {-1.87757965, -0.09494685447, -0.02013809187}},
	};

	n = {
		{0.95, 0.025, 0.025}, {0.025, 0.95, 0.025}, {0.025, 0.025, 0.95}
	};

	for (auto ref: references)
	{
		double p = ref.first[0];
		double T = ref.first[1];
		std::vector<double> gpure = flashparams.G_PT_pure(p, T);

		for (size_t i = 0; i < gpure.size(); i++)
		{
			if (std::fabs(gpure[i]-ref.second[i]) > 1e-6)
			{
				print("Different values for Gpure", std::vector<double>{p, T, gpure[i]-ref.second[i]});
				print("result", gpure);
				print("ref", ref.second);
				error_output++;
			}
		}

		// Test for finding local minimum of Gibbs energy of mixing
		TrialPhase trial;

		trial = flashparams.find_ref_comp(p, T, n[0]);
		error_output += gmix_test(flashparams.eos_params[trial.eos_name].eos, p, T, n[0], gpure, verbose);

		trial = flashparams.find_ref_comp(p, T, n[1]);
		error_output += gmix_test(flashparams.eos_params[trial.eos_name].eos, p, T, n[1], gpure, verbose);

		trial = flashparams.find_ref_comp(p, T, n[2]);
		error_output += gmix_test(flashparams.eos_params[trial.eos_name].eos, p, T, n[2], gpure, verbose);
	}

	if (error_output > 0)
	{
		print("Errors occurred in test_gpure()", error_output);
	}
	else
	{
		print("No errors occurred in test_gpure()", error_output);
	}
	return error_output;
}